// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	C++ class header boilerplate exported from UnrealHeaderTool.
	This is automatically generated by the tools.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectBase.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UPLANET_Interface_PlanetaryObject_generated_h
#error "Interface_PlanetaryObject.generated.h already included, missing '#pragma once' in Interface_PlanetaryObject.h"
#endif
#define UPLANET_Interface_PlanetaryObject_generated_h

#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_RPC_WRAPPERS
#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInterface_PlanetaryObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInterface_PlanetaryObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInterface_PlanetaryObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInterface_PlanetaryObject); \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API UInterface_PlanetaryObject(const UInterface_PlanetaryObject& InCopy); \
public:


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UInterface_PlanetaryObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private copy-constructor, should never be used */ \
	NO_API UInterface_PlanetaryObject(const UInterface_PlanetaryObject& InCopy); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UInterface_PlanetaryObject); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInterface_PlanetaryObject); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInterface_PlanetaryObject)


#undef GENERATED_UINTERFACE_BODY_COMMON
#define GENERATED_UINTERFACE_BODY_COMMON() \
	private: \
	static void StaticRegisterNativesUInterface_PlanetaryObject(); \
	friend UPLANET_API class UClass* Z_Construct_UClass_UInterface_PlanetaryObject(); \
public: \
	DECLARE_CLASS(UInterface_PlanetaryObject, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), 0, UPlanet, NO_API) \
	DECLARE_SERIALIZER(UInterface_PlanetaryObject) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	GENERATED_UINTERFACE_BODY_COMMON() \
	Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_STANDARD_CONSTRUCTORS \
	PRAGMA_POP


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	GENERATED_UINTERFACE_BODY_COMMON() \
	Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_POP


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInterface_PlanetaryObject() {} \
public: \
	typedef UInterface_PlanetaryObject UClassType; \
	virtual UObject* _getUObject() const = 0;


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_INCLASS_IINTERFACE \
protected: \
	virtual ~IInterface_PlanetaryObject() {} \
public: \
	typedef UInterface_PlanetaryObject UClassType; \
	virtual UObject* _getUObject() const = 0;


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_16_PROLOG
#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_RPC_WRAPPERS \
	Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_INCLASS_IINTERFACE \
public: \
PRAGMA_POP


#define Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h_19_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_POP


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Projects_4_8_Source_UPlanet_Interface_PlanetaryObject_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
