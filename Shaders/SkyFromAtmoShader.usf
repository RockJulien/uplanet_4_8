/******************************************************************************
 * SkyFromAtmoShader.usf
 *
 * NOTE : Used to render the planet sky watching it from the space.
 *
 ******************************************************************************/

#include "PlanetaryShaderCommon.usf"

/*************************************************************
 * Vertex Stage
 *************************************************************/
void SkyFromAtmoVertexStage( float3 pInPosition : ATTRIBUTE0,
							 out float4 pOutColor : COLOR0,
							 out float4 pOutSecondaryColor : COLOR1,
							 out float3 pOutDirection : DIRECTION,
							 out float4 pOutPosition : SV_POSITION )
{
	// Get the ray from the camera to the vertex and 
	// its length (which is the far point of the ray passing through the atmosphere)
	float3 lPosition = pInPosition;
	float3 lRay  = lPosition - SkyFromAtmoVVariables.CameraPos;
	float lFar   = length( lRay );
	lRay  /= lFar;

	// Calculate the ray's starting position, then calculate its scattering offset
	float3 lStart = SkyFromAtmoVVariables.CameraPos;
	float lHeight = length( lStart );
	float lDepth  = exp( SkyFromAtmoVConstants.ScaleOverScaleDepth * ( SkyFromAtmoVConstants.InnerRadius - SkyFromAtmoVVariables.CameraHeight ) );
	float lStartAngle  = dot( lRay, lStart ) / lHeight;
	float lStartOffset = lDepth * GetScale( SkyFromAtmoVConstants.ScaleDepth, lStartAngle );

	// Initialize the scattering loop variables
	float lSampleLength = lFar / SkyFromAtmoVConstants.fSamples;
	float lScaledLength = lSampleLength * SkyFromAtmoVConstants.Scale;
	float3 lSampleRay   = lRay * lSampleLength;
	float3 lSamplePoint = lStart + lSampleRay * 0.5;

	// Now loop through the sample rays
	float3 lPrimaryColor = float3( 0.0, 0.0, 0.0 );
	for
		( int i = 0; i < SkyFromAtmoVConstants.iSamples; i++ )
	{
		float lHeight = length( lSamplePoint );
		float lDepth  = exp( SkyFromAtmoVConstants.ScaleOverScaleDepth * ( SkyFromAtmoVConstants.InnerRadius - lHeight ) );
		float lLightAngle  = dot( SkyFromAtmoVVariables.LightPos, lSamplePoint ) / lHeight;
		float lCameraAngle = dot( lRay , lSamplePoint ) / lHeight;
		float lScatter     = ( lStartOffset + lDepth * ( GetScale( SkyFromAtmoVConstants.ScaleDepth, lLightAngle ) - GetScale( SkyFromAtmoVConstants.ScaleDepth, lCameraAngle ) ) );
		float3 lAttenuate  = exp( -lScatter * ( SkyFromAtmoVConstants.InvWavelength * SkyFromAtmoVConstants.Kr4PI + SkyFromAtmoVConstants.Km4PI ) );
		lPrimaryColor += lAttenuate * ( lDepth * lScaledLength );
		lSamplePoint  += lSampleRay;
	}

	// Finally, Scale the Mie and Rayleigh colors and set up the varying variables for the pixel shader
	pOutColor.rgb = lPrimaryColor * ( SkyFromAtmoVConstants.InvWavelength * SkyFromAtmoVConstants.KrESun );
	pOutSecondaryColor.rgb = lPrimaryColor * SkyFromAtmoVConstants.KmESun;
	pOutDirection = SkyFromAtmoVVariables.CameraPos - lPosition;
	pOutPosition  = mul( float4( pInPosition, 1.0 ), SkyFromAtmoVVariables.WorldViewProj );
}

/*************************************************************
 * Pixel Stage
 *************************************************************/
void SkyFromAtmoPixelStage( in float4 pInColor : COLOR0,
							in float4 pInSecondaryColor : COLOR1,
							in float3 pInDirection : DIRECTION, 
							out float4 pOutColor : SV_Target0)
{
	float lCos  = dot( SkyFromAtmoPVariables.LightPos, pInDirection ) / length( pInDirection );
	float lCos2 = lCos * lCos;
	float lRayleighPhase = 0.75 * ( 1.0 + lCos2 );
	float lMiePhase = 1.5 * ( ( 1.0 - SkyFromAtmoPVariables.G2 ) / ( 2.0 + SkyFromAtmoPVariables.G2 ) ) * (1.0 + lCos2) / pow( 1.0 + SkyFromAtmoPVariables.G2 - 2.0 * SkyFromAtmoPVariables.G * lCos, 1.5 );
    pOutColor = lRayleighPhase * pInColor + lMiePhase * pInSecondaryColor;
}
