/******************************************************************************
 * BumpedGroundFromSpaceShader.usf
 *
 * NOTE : Used to render the planet sky watching it from the space.
 *
 ******************************************************************************/

#include "PlanetaryShaderCommon.usf"

/*************************************************************
 * Vertex Stage
 *************************************************************/
void BumpedGroundFromSpaceVertexStage( float3 pInPosition : ATTRIBUTE0,
									   float3 pInNormal : ATTRIBUTE1,
									   float2 pInTexCoords : ATTRIBUTE2,
									   out float2 pOutTexCoords : TEXCOORD0,
									   out float4 pOutColor : COLOR0,
									   out float4 pOutSecondaryColor : COLOR1,
									   out float4 pOutPosition : SV_POSITION )
{
	// Get the ray from the camera to the vertex and its length (which is the far point of the ray passing through the atmosphere)
	float3 lPosition = pInPosition;
	float3 lRay = lPosition - BumpedGroundFromSpaceVVariables.CameraPos;
	float lFar  = length( lRay );
	lRay /= lFar;

	// Calculate the closest intersection of the ray with the outer atmosphere 
	// (which is the near point of the ray passing through the atmosphere)
	float B = 2.0 * dot( BumpedGroundFromSpaceVVariables.CameraPos, lRay );
	float C = BumpedGroundFromSpaceVVariables.CameraHeight2 - BumpedGroundFromSpaceVConstants.OuterRadius2;
	float lDet  = max( 0.0, B * B - 4.0 * C );
	float lNear = 0.5 * ( -B - sqrt( lDet ) );

	// Calculate the ray's starting position, then calculate its scattering offset
	float3 lStart = BumpedGroundFromSpaceVVariables.CameraPos + lRay * lNear;
	lFar -= lNear;
	float lPosLen = length( lPosition );
	float lDepth  = exp( ( BumpedGroundFromSpaceVConstants.InnerRadius - BumpedGroundFromSpaceVConstants.OuterRadius ) / BumpedGroundFromSpaceVConstants.ScaleDepth );
	float lCameraAngle  = dot( -lRay, lPosition ) / lPosLen;
	float lLightAngle   = dot( BumpedGroundFromSpaceVVariables.LightPos, lPosition ) / lPosLen;
	float lCameraScale  = GetScale( BumpedGroundFromSpaceVConstants.ScaleDepth, lCameraAngle );
	float lLightScale   = GetScale( BumpedGroundFromSpaceVConstants.ScaleDepth, lLightAngle );
	float lCameraOffset = lDepth * lCameraScale;
	float lTemp = ( lLightScale + lCameraScale );

	// Initialize the scattering loop variables
	float lSampleLength = lFar / BumpedGroundFromSpaceVConstants.fSamples;
	float lScaledLength = lSampleLength * BumpedGroundFromSpaceVConstants.Scale;
	float3 lSampleRay   = lRay * lSampleLength;
	float3 lSamplePoint = lStart + lSampleRay * 0.5;

	// Now loop through the sample rays
	float3 lFrontColor = float3( 0.0, 0.0, 0.0 );
	float3 lAttenuate;
	for
		( int i = 0; i < BumpedGroundFromSpaceVConstants.iSamples; i++ )
	{
		float lHeight  = length( lSamplePoint );
		float lDepth   = exp( BumpedGroundFromSpaceVConstants.ScaleOverScaleDepth * ( BumpedGroundFromSpaceVConstants.InnerRadius - lHeight ) );
		float lScatter = lDepth * lTemp - lCameraOffset;
		lAttenuate     = exp( -lScatter * ( BumpedGroundFromSpaceVConstants.InvWavelength * BumpedGroundFromSpaceVConstants.Kr4PI + BumpedGroundFromSpaceVConstants.Km4PI ) );
		lFrontColor  += lAttenuate * ( lDepth * lScaledLength );
		lSamplePoint += lSampleRay;
	}

	pOutColor.rgb = lFrontColor * ( BumpedGroundFromSpaceVConstants.InvWavelength * BumpedGroundFromSpaceVConstants.KrESun + BumpedGroundFromSpaceVConstants.KmESun );

	// Calculate the attenuation factor for the ground
	pOutSecondaryColor.rgb = lAttenuate;

	// Calculate the final position
	pOutPosition    = mul( float4( pInPosition, 1.0 ), BumpedGroundFromSpaceVVariables.WorldViewProj );

	// Just pass texture coordinates
	pOutTexCoords = pInTexCoords;
}

// Texture(s)
Texture2D    BumpTexture;
Texture2D    GroundTexture;
SamplerState TextureSampler;

/*************************************************************
 * Pixel Stage
 *************************************************************/
void BumpedGroundFromSpacePixelStage( in float2 pInTexCoords : TEXCOORD0,
									  in float4 pInColor : COLOR0,
									  in float4 pInSecondaryColor : COLOR1,
									  out float4 pOutColor : SV_Target0)
{
	float3 lNormal = Texture2DSample( BumpTexture, TextureSampler, pInTexCoords ).xyz - 0.5;
	float4 lTexCol = Texture2DSample( GroundTexture, TextureSampler, pInTexCoords );
	float lBump = sqrt( max( 0.01, dot( BumpedGroundFromSpacePVariables.LightPos, lNormal ) / length( lNormal ) ) );
	pOutColor   = pInColor + pInSecondaryColor * lBump * lTexCol;
}
