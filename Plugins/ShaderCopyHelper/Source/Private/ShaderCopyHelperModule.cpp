/******************************************************************************
* The MIT License (MIT)
*
* Copyright (c) 2015 Fredrik Lindh
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
******************************************************************************/

//==========================================
// Includes
//==========================================
#include "ShaderCopyHelperPrivatePCH.h" 
#include "Developer/DesktopPlatform/public/DesktopPlatformModule.h"
#include "GenericPlatformFile.h"
#include "PlatformFilemanager.h"

/**
 * Used to define a new log file for log write calls
 */
DEFINE_LOG_CATEGORY_STATIC( ShaderCopyHelper, Log, All );

/********************************************************************************************************************/
void FShaderCopyHelperModule::StartupModule()
{
	UE_LOG( ShaderCopyHelper, Log, TEXT( "Shader Copy Helper Plugin loaded!" ) );

	FString lGameShadersDirectory   = FPaths::Combine( *FPaths::GameDir(), TEXT("Shaders"));
	FString lEngineShadersDirectory = FPaths::Combine( *FPaths::EngineDir(), TEXT("Shaders"));

	this->mShaderFiles = new FShaderFileVisitor();
	IPlatformFile& lPlatformFile = FPlatformFileManager::Get().GetPlatformFile();
	lPlatformFile.IterateDirectoryRecursively( *lGameShadersDirectory, *this->mShaderFiles );
	
	UE_LOG( ShaderCopyHelper, Log, TEXT( "Copying project shader files to Engine/Shaders/" ) );
	for
		( int32 lShaderFileIndex = 0; 
				lShaderFileIndex < this->mShaderFiles->ShaderFilePaths.Num(); 
				lShaderFileIndex++ )
	{
		FString lCurrentShaderFile    = this->mShaderFiles->ShaderFilePaths[ lShaderFileIndex ];
		FString lGameShaderFullPath   = FPaths::Combine( *lGameShadersDirectory, *lCurrentShaderFile );
		FString lEngineShaderFullPath = FPaths::Combine( *lEngineShadersDirectory, *lCurrentShaderFile );

		// Copy all shaders found
		if 
			( lPlatformFile.CopyFile( *lEngineShaderFullPath, *lGameShaderFullPath ) )
		{
			UE_LOG( ShaderCopyHelper, Log, TEXT( "Shader file %s copied to %s." ), *lGameShaderFullPath, *lEngineShaderFullPath );
		}
		else
		{
			UE_LOG( ShaderCopyHelper, Warning, TEXT( "Could not copy %s to %s!" ), *lGameShaderFullPath, *lEngineShaderFullPath );
		}
	}
}

/********************************************************************************************************************/
void FShaderCopyHelperModule::ShutdownModule()
{
	FString lEngineShadersDirectory = FPaths::Combine( *FPaths::EngineDir(), TEXT( "Shaders" ) );
	IPlatformFile& lPlatformFile    = FPlatformFileManager::Get().GetPlatformFile();

	UE_LOG( ShaderCopyHelper, Log, TEXT( "Deleting project shaders from Engine/Shaders/" ) );
	for 
		( int32 lShaderFileIndex = 0; 
				lShaderFileIndex < this->mShaderFiles->ShaderFilePaths.Num(); 
				lShaderFileIndex++ )
	{
		FString lEngineShaderFullPath = FPaths::Combine( *lEngineShadersDirectory, *this->mShaderFiles->ShaderFilePaths[ lShaderFileIndex ] );

		if 
			( lPlatformFile.DeleteFile( *lEngineShaderFullPath ) )
		{
			UE_LOG( ShaderCopyHelper, Log, TEXT( "Shader file %s deleted." ), *lEngineShaderFullPath );
		}
		else
		{
			UE_LOG( ShaderCopyHelper, Warning, TEXT( "Could not delete %s!" ), *lEngineShaderFullPath );
		}
	}

	delete this->mShaderFiles;

	UE_LOG( ShaderCopyHelper, Log, TEXT( "Shader Copy Helper Plugin unloaded!" ) );
}

/**
 * Used to initialize that new module into the UE engine load process
 */
IMPLEMENT_MODULE( FShaderCopyHelperModule, ShaderCopyHelper )