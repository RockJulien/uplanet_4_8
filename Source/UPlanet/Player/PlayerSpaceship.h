// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "GameFramework/Pawn.h"
#include "PlayerSpaceship.generated.h"

//==========================================
// Class definition
//==========================================
UCLASS()
class UPLANET_API APlayerSpaceship : public APawn
{
	GENERATED_BODY()

private:

	//==========================================
	// Attributes
	//==========================================
	/** 
	 * StaticMesh component that will be the visuals for our flying pawn 
	 */
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* PlaneMesh;

	/** 
	 * Spring arm that will offset the camera 
	 */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* SpringArm;

	/** 
	 * Camera component that will be our viewpoint 
	 */
	UPROPERTY(Category = Camera, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* Camera;

	/** 
	 * How quickly forward speed changes 
	 */
	UPROPERTY(Category = Plane, EditAnywhere)
	float Acceleration;

	/** 
	 * How quickly pawn can steer 
	 */
	UPROPERTY(Category = Plane, EditAnywhere)
	float TurnSpeed;

	/** 
	 * Max forward speed 
	 */
	UPROPERTY(Category = Pitch, EditAnywhere)
	float MaxSpeed;

	/** 
	 * Min forward speed 
	 */
	UPROPERTY(Category = Yaw, EditAnywhere)
	float MinSpeed;

	/** 
	 * Current forward speed 
	 */
	float mCurrentForwardSpeed;

	/** 
	 * Current yaw speed 
	 */
	float mCurrentYawSpeed;

	/** 
	 * Current pitch speed 
	 */
	float mCurrentPitchSpeed;

	/** 
	 * Current roll speed 
	 */
	float mCurrentRollSpeed;

	/**
	 * Spaceship is stopped or not
	 */
	bool  mIsStopped;

protected:

	// Begin APawn overrides

	/**
	 * Setup the player input component and 
	 * Allows binding actions/axes to functions
	 */
	virtual void SetupPlayerInputComponent(class UInputComponent* pInputComponent) override;

	// End APawn overrides

	/**
	 * Give default accel to the spaceship
	 */
	void MoveSpaceshipInput();

	/**
	 * Stop the spaceship
	 */
	void StopSpaceshipInput();

	/** 
	 * Bound to the thrust axis 
	 */
	void ThrustInput(float pValue);

	/** 
	 * Bound to the vertical axis 
	 */
	void MoveUpInput(float pValue);

	/** 
	 * Bound to the horizontal axis 
	 */
	void MoveRightInput(float pValue);

public:

	//==========================================
	// Constructor & Destructor
	//==========================================
	APlayerSpaceship(const FObjectInitializer& pInitializer);

	//==========================================
	// Methods
	//==========================================
	/**
	 * Called when the game starts or when spawned
	 */
	virtual void BeginPlay() override;
	
	/** 
	 * Called every frame 
	 */
	virtual void Tick( float pDeltaSeconds ) override;

	/**
	 * Receive a hit
	 */
	virtual void NotifyHit(class UPrimitiveComponent* pMyComp, class AActor* pOther, class UPrimitiveComponent* pOtherComp, bool pIsSelfMoved, FVector pHitLocation, FVector pHitNormal, FVector pNormalImpulse, const FHitResult& pHit) override;

	/** 
	 * Returns PlaneMesh subobject 
	 */
	FORCEINLINE class UStaticMeshComponent* GetPlaneMesh() const 
	{
		return PlaneMesh; 
	}

	/** 
	 * Returns SpringArm subobject 
	 */
	FORCEINLINE class USpringArmComponent* GetSpringArm() const 
	{
		return SpringArm; 
	}

	/** 
	 * Returns Camera subobject 
	 */
	FORCEINLINE class UCameraComponent* GetCamera() const 
	{
		return Camera; 
	}
	
};
