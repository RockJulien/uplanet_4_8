// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlayerSpaceship.h"

/************************************************************************************************************************************/
APlayerSpaceship::APlayerSpaceship(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	this->PrimaryActorTick.bCanEverTick = true;

	// Structure to hold one-time initialization
	struct FConstructorStatics
	{
		ConstructorHelpers::FObjectFinderOptional<UStaticMesh> PlaneMesh;
		FConstructorStatics() : 
		PlaneMesh(TEXT("/Game/Meshes/Spaceship"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Create static mesh component
	this->PlaneMesh = pInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("PlaneMesh0"));
	this->PlaneMesh->SetStaticMesh(ConstructorStatics.PlaneMesh.Get());
	this->RootComponent = this->PlaneMesh;

	// Create a spring arm component
	this->SpringArm = pInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("SpringArm0"));
	this->SpringArm->AttachTo( this->RootComponent );
	this->SpringArm->TargetArmLength = 400.0f; // The camera follows at this distance behind the character	
	this->SpringArm->SocketOffset = FVector(-100.f, -90.f, 200.f); // LocalZ, LocalX, LocalY
	this->SpringArm->bEnableCameraLag = false;
	this->SpringArm->CameraLagSpeed = 15.f;

	// Create camera component 
	this->Camera = pInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera0"));
	this->Camera->AttachTo( SpringArm, USpringArmComponent::SocketName);
	this->Camera->bUsePawnControlRotation = false; // Don't rotate camera with controller

	// Set handling parameters
	Acceleration = 500.f;
	TurnSpeed    = 50.f;
	MaxSpeed     = 4000.f;
	MinSpeed     = 500.f;
	this->mCurrentForwardSpeed = 0.0f;
	this->mIsStopped = true;
}

/************************************************************************************************************************************/
void APlayerSpaceship::BeginPlay()
{
	Super::BeginPlay();
	
}

/************************************************************************************************************************************/
void APlayerSpaceship::Tick(float pDeltaTime)
{
	const FVector lLocalMove = FVector( this->mCurrentForwardSpeed * pDeltaTime, 0.f, 0.f );

	// Move plan forwards (with sweep so we stop when we collide with things)
	this->AddActorLocalOffset( lLocalMove, true );

	// Calculate change in rotation this frame
	FRotator lDeltaRotation( 0, 0, 0 );
	lDeltaRotation.Pitch = this->mCurrentPitchSpeed * pDeltaTime;
	lDeltaRotation.Yaw   = this->mCurrentYawSpeed * pDeltaTime;
	lDeltaRotation.Roll  = this->mCurrentRollSpeed * pDeltaTime;

	// Rotate plane
	this->AddActorLocalRotation( lDeltaRotation );

	Super::Tick( pDeltaTime );
}

/************************************************************************************************************************************/
void APlayerSpaceship::NotifyHit(class UPrimitiveComponent* pMyComp, class AActor* pOther, class UPrimitiveComponent* pOtherComp, bool pIsSelfMoved, FVector pHitLocation, FVector pHitNormal, FVector pNormalImpulse, const FHitResult& pHit)
{
	Super::NotifyHit( pMyComp, pOther, pOtherComp, pIsSelfMoved, pHitLocation, pHitNormal, pNormalImpulse, pHit );

	// Set velocity to zero upon collision
	this->mCurrentForwardSpeed = 0.0f;
}

/************************************************************************************************************************************/
void APlayerSpaceship::SetupPlayerInputComponent(class UInputComponent* pInputComponent)
{
	check( pInputComponent );

	// Bind our control axis' to callback functions
	pInputComponent->BindAction( "MoveSpaceship", IE_Pressed, this, &APlayerSpaceship::MoveSpaceshipInput );
	pInputComponent->BindAction( "StopSpaceship", IE_Pressed, this, &APlayerSpaceship::StopSpaceshipInput);
	pInputComponent->BindAxis( "Thrust",    this, &APlayerSpaceship::ThrustInput );
	pInputComponent->BindAxis( "MoveUp",    this, &APlayerSpaceship::MoveUpInput );
	pInputComponent->BindAxis( "MoveRight", this, &APlayerSpaceship::MoveRightInput );
}

/************************************************************************************************************************************/
void APlayerSpaceship::MoveSpaceshipInput()
{
	this->mCurrentForwardSpeed = 500.0f;
	this->mIsStopped = false;
}

/************************************************************************************************************************************/
void APlayerSpaceship::StopSpaceshipInput()
 {
 	this->mCurrentForwardSpeed = 0.0f;
	this->mIsStopped = true;
}

/************************************************************************************************************************************/
void APlayerSpaceship::ThrustInput(float pValue)
{
	if 
		( this->mIsStopped )
	{
		return;
	}

	// Is there no input?
	bool lHasInput = FMath::IsNearlyEqual(pValue, 0.0f) == false;
	// If input is not held down, reduce speed
	float lCurrentAcc = lHasInput ? (pValue * this->Acceleration) : (-0.5f * this->Acceleration);
	// Calculate new speed
	float lNewForwardSpeed = this->mCurrentForwardSpeed + (GetWorld()->GetDeltaSeconds() * lCurrentAcc);
	// Clamp between MinSpeed and MaxSpeed
	this->mCurrentForwardSpeed = FMath::Clamp( lNewForwardSpeed, this->MinSpeed, this->MaxSpeed );
}

/************************************************************************************************************************************/
void APlayerSpaceship::MoveUpInput(float pValue)
{
	if 
		( this->mIsStopped )
	{
		return;
	}

	// Target pitch speed is based in input
	float lTargetPitchSpeed = (pValue * this->TurnSpeed * -1.f);

	// When steering, we decrease pitch slightly
	lTargetPitchSpeed += (FMath::Abs(this->mCurrentYawSpeed) * -0.2f);

	// Smoothly interpolate to target pitch speed
	this->mCurrentPitchSpeed = FMath::FInterpTo(this->mCurrentPitchSpeed, lTargetPitchSpeed, GetWorld()->GetDeltaSeconds(), 2.0f);
}

/************************************************************************************************************************************/
void APlayerSpaceship::MoveRightInput(float pValue)
{
	if
		( this->mIsStopped )
	{
		return;
	}

	// Target yaw speed is based on input
	float lTargetYawSpeed = (pValue * this->TurnSpeed);

	// Smoothly interpolate to target yaw speed
	this->mCurrentYawSpeed = FMath::FInterpTo( this->mCurrentYawSpeed, lTargetYawSpeed, GetWorld()->GetDeltaSeconds(), 2.f);

	// Is there any left/right input?
	const bool lIsTurning = FMath::Abs( pValue ) > 0.2f;

	// If turning, yaw value is used to influence roll
	// If not turning, roll to reverse current roll value
	float lTargetRollSpeed = lIsTurning ? ( this->mCurrentYawSpeed * 0.5f) : (GetActorRotation().Roll * -2.f);

	// Smoothly interpolate roll speed
	this->mCurrentRollSpeed = FMath::FInterpTo( this->mCurrentRollSpeed, lTargetRollSpeed, GetWorld()->GetDeltaSeconds(), 2.f);
}
