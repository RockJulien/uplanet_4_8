// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Helpers/PlanetaryHelpers.h"

//==========================================
// Class definition
//==========================================
/**
 * Base Coordinates class offering base conversion 
 * from face coordinates to 3D positions.
 */
class UPLANET_API FaceCoordinates
{
protected:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The Face corrdinate on X (from 0 to 1)
	 */
	float mXCoordinate;

	/**
	 * The Face corrdinate on X (from 0 to 1)
	 */
	float mYCoordinate;

	//==========================================
	// Private Methods
	//==========================================

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FaceCoordinates();
	FaceCoordinates(const FaceCoordinates& pToCopy);
	explicit FaceCoordinates(const float& pXCoordinate, const float& pYCoordinate);
	explicit FaceCoordinates(PlanetaryFace::PlanetaryFace pFace, const FVector& pPosition);
	virtual  ~FaceCoordinates();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Get the position from face coordinates
	 */
	FORCEINLINE FVector GetPosition(PlanetaryFace::PlanetaryFace pFace, const float& pSize = 1.0f) const;

	/**
	 * Get the coordinates as Vector2D struct
	 */
	FORCEINLINE FVector2D AsVector2D() const;

	/**
	 * Get the coordinate on X
	 */
	FORCEINLINE float   GetXCoordinate() const;

	/**
	 * Get the coordinate on Y
	 */
	FORCEINLINE float   GetYCoordinate() const;

	/**
	 * Assignment operator
	 */
	const FaceCoordinates& operator = (const FaceCoordinates& pToAssign);
};

//==========================================
// Inline
//==========================================
#include "FaceCoordinates.inl"
