// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Platform.h"
#include "../Math/UnrealMath.h"
#include "../PlanetaryFace.h"

//==========================================
// Class definition
//==========================================
/**
 * Planetary helper class
 */
class UPLANET_API PlanetaryHelper
{
public:

	//==========================================
	// Methods
	//==========================================

	/**
	 * Method getting the corresponding face supplying the 3D position
	 */
	static FORCEINLINE PlanetaryFace::PlanetaryFace GetFace(const FVector& pPosition)
	{
		PlanetaryFace::PlanetaryFace lFace = PlanetaryFace::eBackFace;

		float lX = FMath::Abs( pPosition.X );
		float lY = FMath::Abs( pPosition.Y );
		float lZ = FMath::Abs( pPosition.Z );
		bool lIsXGreaterThanY = lX > lY;
		bool lIsXGreaterThanZ = lX > lZ;
		bool lIsYGreaterThanX = lY > lX;
		bool lIsYGreaterThanZ = lY > lZ;
		if 
			( lIsXGreaterThanY && 
			  lIsXGreaterThanZ )
		{
			bool lIsXPositive = pPosition.X > 0;
			if 
				( lIsXPositive )
			{
				lFace = PlanetaryFace::eRightFace;
			}
			else
			{
				lFace = PlanetaryFace::eLeftFace;
			}
		}
		else if 
			( lIsYGreaterThanX && 
			  lIsYGreaterThanZ )
		{
			bool lIsYPositive = pPosition.Y > 0;
			if
				( lIsYPositive )
			{
				lFace = PlanetaryFace::eTopFace;
			}
			else
			{
				lFace = PlanetaryFace::eBottomFace;
			}
		}
		else
		{
			bool lIsZPositive = pPosition.Z > 0;
			if
				( lIsZPositive )
			{
				lFace = PlanetaryFace::eFrontFace;
			}
			else
			{
				lFace = PlanetaryFace::eBackFace;
			}
		}

		return lFace;
	}

	/**
	 * Method getting the face coordinates using the face in which look for the coordinates and the 3D position.
	 * In the case that position is not in the supplied face, the closest coordinates within the face from the 3D
	 * position are given.
	 */
	static FORCEINLINE PlanetaryFace::PlanetaryFace GetFaceCoordinatesFromPosition(const FVector& pPosition, float& pXCoordinate, float& pYCoordinate)
	{
		PlanetaryFace::PlanetaryFace lFace = GetFace(pPosition);
		GetFaceCoordinatesFromPosition( lFace, pPosition, pXCoordinate, pYCoordinate );
		return lFace;
	}

	/**
	 * Method getting the face coordinates using the face in which look for the coordinates and the 3D position.
	 * In the case that position is not in the supplied face, the closest coordinates within the face from the 3D 
	 * position are given.
	 */
	static FORCEINLINE void GetFaceCoordinatesFromPosition(PlanetaryFace::PlanetaryFace pFace, const FVector& pPosition, float& pXCoordinate, float& pYCoordinate)
	{
		float lXAbs = FMath::Abs( pPosition.X );
		float lYAbs = FMath::Abs( pPosition.Y );
		float lZAbs = FMath::Abs( pPosition.Z );

		switch 
			( pFace )
		{
		case PlanetaryFace::eRightFace:
			{
				bool lXAbsSmallerEqualThanZAbs = lXAbs <= lZAbs;
				bool lXAbsSmallerEqualThanYAbs = lXAbs <= lYAbs;
				if 
					( lXAbsSmallerEqualThanZAbs )
				{
					bool lIsZPositive = pPosition.Z > 0;
					if 
						( lIsZPositive )
					{
						pXCoordinate = -1.0f;
					}
					else
					{
						pXCoordinate = 1.0f;
					}
				}
				else
				{
					pXCoordinate = -pPosition.Z / lXAbs;
				}

				if
					( lXAbsSmallerEqualThanYAbs )
				{
					bool lIsYPositive = pPosition.Y > 0;
					if
						( lIsYPositive )
					{
						pYCoordinate = -1.0f;
					}
					else
					{
						pYCoordinate = 1.0f;
					}
				}
				else
				{
					pYCoordinate = -pPosition.Y / lXAbs;
				}

				// If the 3D position is on the opposite face,
				// the coordinates must be forced to the edge of this face.
				bool lIsXNegative = pPosition.X < 0;
				bool lXAbsGreaterThanZAbs = lXAbs > lZAbs;
				bool lXAbsGreaterThanYAbs = lXAbs > lYAbs;
				if 
					( lIsXNegative && 
					  lXAbsGreaterThanZAbs && 
					  lXAbsGreaterThanYAbs )
				{
					if 
						( FMath::Abs( pXCoordinate ) > FMath::Abs(pYCoordinate))
					{
						bool lIsXCoordinatePositive = pXCoordinate > 0;
						if 
							( lIsXCoordinatePositive )
						{
							pXCoordinate = 1.0f;
						}
						else
						{
							pXCoordinate = -1.0f;
						}
					}
					else
					{
						bool lIsYCoordinatePositive = pYCoordinate > 0;
						if 
							( lIsYCoordinatePositive )
						{
							pYCoordinate = 1.0f;
						}
						else
						{
							pYCoordinate = -1.0f;
						}
					}
				}
			}
			break;
		case PlanetaryFace::eLeftFace:
			{
				bool lXAbsSmallerEqualThanZAbs = lXAbs <= lZAbs;
				bool lXAbsSmallerEqualThanYAbs = lXAbs <= lYAbs;
				if
					( lXAbsSmallerEqualThanZAbs )
				{
					bool lIsZPositive = pPosition.Z > 0;
					if
						(lIsZPositive)
					{
						pXCoordinate = 1.0f;
					}
					else
					{
						pXCoordinate = -1.0f;
					}
				}
				else
				{
					pXCoordinate = pPosition.Z / lXAbs;
				}

				if
					( lXAbsSmallerEqualThanYAbs )
				{
					bool lIsYPositive = pPosition.Y > 0;
					if
						( lIsYPositive )
					{
						pYCoordinate = -1.0f;
					}
					else
					{
						pYCoordinate = 1.0f;
					}
				}
				else
				{
					pYCoordinate = -pPosition.Y / lXAbs;
				}

				// If the 3D position is on the opposite face,
				// the coordinates must be forced to the edge of this face.
				bool lIsXPositive = pPosition.X > 0;
				bool lXAbsGreaterThanZAbs = lXAbs > lZAbs;
				bool lXAbsGreaterThanYAbs = lXAbs > lYAbs;
				if
					( lIsXPositive &&
					  lXAbsGreaterThanZAbs &&
					  lXAbsGreaterThanYAbs )
				{
					if
						( FMath::Abs( pXCoordinate ) > FMath::Abs( pYCoordinate ) )
					{
						bool lIsXCoordinatePositive = pXCoordinate > 0;
						if
							( lIsXCoordinatePositive )
						{
							pXCoordinate = 1.0f;
						}
						else
						{
							pXCoordinate = -1.0f;
						}
					}
					else
					{
						bool lIsYCoordinatePositive = pYCoordinate > 0;
						if
							( lIsYCoordinatePositive )
						{
							pYCoordinate = 1.0f;
						}
						else
						{
							pYCoordinate = -1.0f;
						}
					}
				}
			}
			break;
		case PlanetaryFace::eTopFace:
			{
				bool lYAbsSmallerEqualThanXAbs = lYAbs <= lXAbs;
				bool lYAbsSmallerEqualThanZAbs = lYAbs <= lZAbs;
				if
					( lYAbsSmallerEqualThanXAbs )
				{
					bool lIsXPositive = pPosition.X > 0;
					if
						( lIsXPositive )
					{
						pXCoordinate = 1.0f;
					}
					else
					{
						pXCoordinate = -1.0f;
					}
				}
				else
				{
					pXCoordinate = pPosition.X / lYAbs;
				}

				if
					( lYAbsSmallerEqualThanZAbs )
				{
					bool lIsZPositive = pPosition.Z > 0;
					if
						( lIsZPositive )
					{
						pYCoordinate = 1.0f;
					}
					else
					{
						pYCoordinate = -1.0f;
					}
				}
				else
				{
					pYCoordinate = pPosition.Z / lYAbs;
				}

				// If the 3D position is on the opposite face,
				// the coordinates must be forced to the edge of this face.
				bool lIsYNegative = pPosition.Y < 0;
				bool lYAbsGreaterThanXAbs = lYAbs > lXAbs;
				bool lYAbsGreaterThanZAbs = lYAbs > lZAbs;
				if
					( lIsYNegative &&
					  lYAbsGreaterThanXAbs &&
					  lYAbsGreaterThanZAbs )
				{
					if
						( FMath::Abs( pXCoordinate ) > FMath::Abs( pYCoordinate ) )
					{
						bool lIsXCoordinatePositive = pXCoordinate > 0;
						if
							( lIsXCoordinatePositive )
						{
							pXCoordinate = 1.0f;
						}
						else
						{
							pXCoordinate = -1.0f;
						}
					}
					else
					{
						bool lIsYCoordinatePositive = pYCoordinate > 0;
						if
							( lIsYCoordinatePositive )
						{
							pYCoordinate = 1.0f;
						}
						else
						{
							pYCoordinate = -1.0f;
						}
					}
				}
			}
			break;
		case PlanetaryFace::eBottomFace:
			{
				bool lYAbsSmallerEqualThanXAbs = lYAbs <= lXAbs;
				bool lYAbsSmallerEqualThanZAbs = lYAbs <= lZAbs;
				if
					( lYAbsSmallerEqualThanXAbs )
				{
					bool lIsXPositive = pPosition.X > 0;
					if
						( lIsXPositive )
					{
						pXCoordinate = 1.0f;
					}
					else
					{
						pXCoordinate = -1.0f;
					}
				}
				else
				{
					pXCoordinate = pPosition.X / lYAbs;
				}

				if
					( lYAbsSmallerEqualThanZAbs )
				{
					bool lIsZPositive = pPosition.Z > 0;
					if
						( lIsZPositive )
					{
						pYCoordinate = -1.0f;
					}
					else
					{
						pYCoordinate = 1.0f;
					}
				}
				else
				{
					pYCoordinate = -pPosition.Z / lYAbs;
				}

				// If the 3D position is on the opposite face,
				// the coordinates must be forced to the edge of this face.
				bool lIsYPositive = pPosition.Y > 0;
				bool lYAbsGreaterThanXAbs = lYAbs > lXAbs;
				bool lYAbsGreaterThanZAbs = lYAbs > lZAbs;
				if
					( lIsYPositive &&
					  lYAbsGreaterThanXAbs &&
					  lYAbsGreaterThanZAbs )
				{
					if
						( FMath::Abs( pXCoordinate ) > FMath::Abs( pYCoordinate ) )
					{
						bool lIsXCoordinatePositive = pXCoordinate > 0;
						if
							( lIsXCoordinatePositive )
						{
							pXCoordinate = 1.0f;
						}
						else
						{
							pXCoordinate = -1.0f;
						}
					}
					else
					{
						bool lIsYCoordinatePositive = pYCoordinate > 0;
						if
							( lIsYCoordinatePositive )
						{
							pYCoordinate = 1.0f;
						}
						else
						{
							pYCoordinate = -1.0f;
						}
					}
				}
			}
			break;
		case PlanetaryFace::eFrontFace:
			{
				bool lZAbsSmallerEqualThanXAbs = lZAbs <= lXAbs;
				bool lZAbsSmallerEqualThanYAbs = lZAbs <= lYAbs;
				if
					( lZAbsSmallerEqualThanXAbs )
				{
					bool lIsXPositive = pPosition.X > 0;
					if
						( lIsXPositive )
					{
						pXCoordinate = 1.0f;
					}
					else
					{
						pXCoordinate = -1.0f;
					}
				}
				else
				{
					pXCoordinate = pPosition.X / lZAbs;
				}

				if
					( lZAbsSmallerEqualThanYAbs )
				{
					bool lIsYPositive = pPosition.Y > 0;
					if
						( lIsYPositive )
					{
						pYCoordinate = -1.0f;
					}
					else
					{
						pYCoordinate = 1.0f;
					}
				}
				else
				{
					pYCoordinate = -pPosition.Y / lZAbs;
				}

				// If the 3D position is on the opposite face,
				// the coordinates must be forced to the edge of this face.
				bool lIsZNegative = pPosition.Z < 0;
				bool lZAbsGreaterThanXAbs = lZAbs > lXAbs;
				bool lZAbsGreaterThanYAbs = lZAbs > lYAbs;
				if
					( lIsZNegative &&
					  lZAbsGreaterThanXAbs &&
					  lZAbsGreaterThanYAbs )
				{
					if
						( FMath::Abs( pXCoordinate ) > FMath::Abs( pYCoordinate ) )
					{
						bool lIsXCoordinatePositive = pXCoordinate > 0;
						if
							( lIsXCoordinatePositive )
						{
							pXCoordinate = 1.0f;
						}
						else
						{
							pXCoordinate = -1.0f;
						}
					}
					else
					{
						bool lIsYCoordinatePositive = pYCoordinate > 0;
						if
							( lIsYCoordinatePositive )
						{
							pYCoordinate = 1.0f;
						}
						else
						{
							pYCoordinate = -1.0f;
						}
					}
				}
			}
			break;
		case PlanetaryFace::eBackFace:
			{
				bool lZAbsSmallerEqualThanXAbs = lZAbs <= lXAbs;
				bool lZAbsSmallerEqualThanYAbs = lZAbs <= lYAbs;
				if
					( lZAbsSmallerEqualThanXAbs )
				{
					bool lIsXPositive = pPosition.X > 0;
					if
						( lIsXPositive )
					{
						pXCoordinate = -1.0f;
					}
					else
					{
						pXCoordinate = 1.0f;
					}
				}
				else
				{
					pXCoordinate = -pPosition.X / lZAbs;
				}

				if
					( lZAbsSmallerEqualThanYAbs )
				{
					bool lIsYPositive = pPosition.Y > 0;
					if
						( lIsYPositive )
					{
						pYCoordinate = -1.0f;
					}
					else
					{
						pYCoordinate = 1.0f;
					}
				}
				else
				{
					pYCoordinate = -pPosition.Y / lZAbs;
				}

				// If the 3D position is on the opposite face,
				// the coordinates must be forced to the edge of this face.
				bool lIsZPositive = pPosition.Z > 0;
				bool lZAbsGreaterThanXAbs = lZAbs > lXAbs;
				bool lZAbsGreaterThanYAbs = lZAbs > lYAbs;
				if 
					( lIsZPositive && 
					  lZAbsGreaterThanXAbs && 
					  lZAbsGreaterThanYAbs )
				{
					if 
						( FMath::Abs( pXCoordinate ) > FMath::Abs( pYCoordinate ) )
					{
						bool lIsXCoordinatePositive = pXCoordinate > 0;
						if
							( lIsXCoordinatePositive )
						{
							pXCoordinate = 1.0f;
						}
						else
						{
							pXCoordinate = -1.0f;
						}
					}
					else
					{
						bool lIsYCoordinatePositive = pYCoordinate > 0;
						if
							( lIsYCoordinatePositive )
						{
							pYCoordinate = 1.0f;
						}
						else
						{
							pYCoordinate = -1.0f;
						}
					}
				}
			}
			break;
		}

		// XCoordinate and YCoordinate should be approximately in the range -1 to 1, 
		// so scale and clamp coordinates to the range 0 to 1
		pXCoordinate = FMath::Max( 0.0f, FMath::Min( 1.0f, (pXCoordinate + 1.0f) * 0.5f ) );
		pYCoordinate = FMath::Max( 0.0f, FMath::Min( 1.0f, (pYCoordinate + 1.0f) * 0.5f ) );
	}

	/**
	 * Method getting the corresponding 3D position giving the face and coordinates within it.
	 */
	static FORCEINLINE FVector GetPlanetaryPositionFromCoordinates(PlanetaryFace::PlanetaryFace pFace, const float& pXCoordinate, const float& pYCoordinate, const float& pLength = 1.0f)
	{
		FVector lPosition;
		float lX = (pXCoordinate * 2.0f) - 1.0f;
		float lY = (pYCoordinate * 2.0f) - 1.0f;
		switch 
			( pFace )
		{
		case PlanetaryFace::eRightFace:
			{
				lPosition.X = 1;
				lPosition.Y = -lY;
				lPosition.Z = -lX;
			}
			break;
		case PlanetaryFace::eLeftFace:
			{
				lPosition.X = -1;
				lPosition.Y = -lY;
				lPosition.Z = lX;
			}
			break;
		case PlanetaryFace::eTopFace:
			{
				lPosition.X = lX;
				lPosition.Y = 1;
				lPosition.Z = lY;
			}
			break;
		case PlanetaryFace::eBottomFace:
			{
				lPosition.X = lX;
				lPosition.Y = -1;
				lPosition.Z = -lY;
			}
			break;
		case PlanetaryFace::eFrontFace:
			{
				lPosition.X = lX;
				lPosition.Y = -lY;
				lPosition.Z = 1;
			}
			break;
		case PlanetaryFace::eBackFace:
			{
				lPosition.X = -lX;
				lPosition.Y = -lY;
				lPosition.Z = -1;
			}
			break;
		}

		float lMagnitude = lPosition.Size();
		float fScale = pLength / lMagnitude;
		return lPosition * fScale;
	}
};