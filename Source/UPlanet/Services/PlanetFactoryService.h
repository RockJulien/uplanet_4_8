// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Map.h"
#include "Interface_PluginService.h"

//==========================================
// Class definition 
//==========================================
class FPlanetFactoryService
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * Stores the set of plugin's factory services
	 */
	TMap<const UClass*, IInterface_PluginService*> mActiveServices;

	//==========================================
	// Sorter
	//==========================================
	struct FSortByPriority
	{
		FORCEINLINE bool operator ()(const IInterface_PluginService& pServiceA, const IInterface_PluginService& pServiceB) const
		{
			// Sort ascending
			return StaticCast<const UPlanetaryFactory*>( &pServiceA )->Priority() < StaticCast<const UPlanetaryFactory*>( &pServiceB )->Priority();
		}
	};

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	FPlanetFactoryService();
	~FPlanetFactoryService();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Registers a new service
	 */
	void RegisterService(IInterface_PluginService* pNewService);

	/**
	 * Release the active services cache
	 */
	void Release();

	/**
	 * Returns all the active services
	 */
	FORCEINLINE TArray<IInterface_PluginService*> GetActiveServices() const
	{
		TArray<IInterface_PluginService*> lValues;
		this->mActiveServices.GenerateValueArray( lValues );
		return lValues;
	}

	/**
	 * Returns corresponding active service
	 */
	FORCEINLINE IInterface_PluginService* GetActiveService(const UClass* pType) const
	{
		if
			( this->mActiveServices.Contains( pType ) )
		{
			return this->mActiveServices[ pType ];
		}

		return NULL;
	}
};
