// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "UnrealString.h"
#include "Interface_PluginService.generated.h"

//==========================================
// Interface definition
//==========================================
/**
 * Plugin service base interface UObject version castable
 */
UINTERFACE(meta=(CannotImplementInterfaceInBlueprint))
class UPLANET_API UInterface_PluginService : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * Plugin service base interface
 */
class UPLANET_API IInterface_PluginService
{
	GENERATED_IINTERFACE_BODY()

public:

	//==========================================
	// Methods
	//==========================================
	/**
	 * Gets the service name
	 */
	virtual const FString& Name() const = 0;

	/**
	 * Gets the service description
	 */
	virtual const FString& Description() const = 0;

	/**
	 * Gets the flag indicating whether the service is active or not.
	 */
	virtual bool IsActive() const = 0;

	/**
	 * Releases resources
	 */
	virtual void Release() = 0;
};

