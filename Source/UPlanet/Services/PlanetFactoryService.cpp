
#include "../UPlanet.h"
#include "PlanetFactoryService.h"

/************************************************************************************************/
FPlanetFactoryService::FPlanetFactoryService()
{

}

/************************************************************************************************/
FPlanetFactoryService::~FPlanetFactoryService()
{
	
}

/************************************************************************************************/
void FPlanetFactoryService::RegisterService(IInterface_PluginService* pNewService)
{
	if 
		( pNewService == NULL )
	{
		return;
	}

	UObject* lCast = Cast<UObject>( pNewService );
	this->mActiveServices.Add( lCast->GetClass(), pNewService );

	// Sort by value priority
	this->mActiveServices.ValueSort( FSortByPriority() );
}

/************************************************************************************************/
void FPlanetFactoryService::Release()
{
	for 
		( auto lIterator = this->mActiveServices.CreateIterator(); 
			   lIterator; 
			   ++lIterator )
	{
		lIterator->Value->Release();
	}

	this->mActiveServices.Empty();
}
