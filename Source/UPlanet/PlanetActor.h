// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "GameFramework/Actor.h"
#include "PlanetaryMap.h"
#include "PlanetActor.generated.h"

//==========================================
// Class definition
//==========================================
/**
 * Planetary actor class spawnable into the scene
 */
UCLASS()
class UPLANET_API APlanetActor : public AActor
{
	GENERATED_BODY()
	
public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	APlanetActor(const FObjectInitializer& pPCIP);

	//==========================================
	// Properties
	//==========================================
	/**
	 * The current distance to the horizon
	 */
	UPROPERTY(VisibleAnywhere)
	UPlanetaryMap* Planet;

	//==========================================
	// Methods
	//==========================================
	/**
	 * Update function called every frame
	 * 
	 * @param pDeltaSeconds The delta time in seconds
	 */
	virtual void Tick(float pDeltaSeconds) override;

	/** 
	 * Event when play begins for this actor. 
	 */
	virtual void BeginPlay() override;

	/** 
	 * Overridable function called whenever this actor is being removed from a level 
	 */
	virtual void EndPlay(const EEndPlayReason::Type pEndPlayReason) override;

	/**
	 * Returns the flag indicating whether the actor must tick 
	 */
	virtual bool ShouldTickIfViewportsOnly() const override;
};
