// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "FaceCoordinates.h"

/***************************************************************************************/
FaceCoordinates::FaceCoordinates() :
mXCoordinate(0.0f), mYCoordinate(0.0f)
{

}

/***************************************************************************************/
FaceCoordinates::FaceCoordinates(const FaceCoordinates& pToCopy) :
mXCoordinate(pToCopy.mXCoordinate), mYCoordinate(pToCopy.mYCoordinate)
{

}

/***************************************************************************************/
FaceCoordinates::FaceCoordinates(const float& pXCoordinate, const float& pYCoordinate) :
mXCoordinate(pXCoordinate), mYCoordinate(pYCoordinate)
{

}

/***************************************************************************************/
FaceCoordinates::FaceCoordinates(PlanetaryFace::PlanetaryFace pFace, const FVector& pPosition)
{
	PlanetaryHelper::GetFaceCoordinatesFromPosition( pFace, pPosition, this->mXCoordinate, this->mYCoordinate );
}

/***************************************************************************************/
FaceCoordinates::~FaceCoordinates()
{

}

/***************************************************************************************/
const FaceCoordinates& FaceCoordinates::operator = (const FaceCoordinates& pToAssign)
{
	this->mXCoordinate = pToAssign.mXCoordinate;
	this->mYCoordinate = pToAssign.mYCoordinate;

	return *this;
}

