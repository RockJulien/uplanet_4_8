// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace PlanetaryQuadrant
{

	/**
	 * Enumeration offering the available quadrants of a planetary face
	 */
	enum PlanetaryQuadrant
	{
		/**
		 * No quadrant
		 */
		eNone = -1,

		/**
		 * The top left quadrant
		 */
		 eTopLeft = 0,

		 /**
		  * The top right quadrant
		  */
		  eTopRight,

		  /**
		   * The Bottom left quadrant
		   */
		   eBottomLeft,

		   /**
			* The Bottom right quadrant
			*/
			eBottomRight
	};

}
