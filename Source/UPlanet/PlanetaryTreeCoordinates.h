// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "FaceCoordinates.h"
#include "PlanetaryCoordinateState.h"

//==========================================
// Class definition
//==========================================
/**
 * Higher helper class offering conversions between 3D positions to quad-tree coordinates
 */
class UPLANET_API PlanetaryTreeCoordinates : public FaceCoordinates
{
private:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * Terrain height above the planet surface (that is above the planet radius)
	 */
	float	mSurfaceHeight;

	/**
	 * The cube face this coordinate lies on
	 */
	PlanetaryFace::PlanetaryFace mFace;

	/**
	 * The planetary coordinate state
	 */
	PlanetaryCoordinateState::PlanetaryCoordinateState mState;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	PlanetaryTreeCoordinates();
	PlanetaryTreeCoordinates(const PlanetaryTreeCoordinates& pToCopy);
	explicit PlanetaryTreeCoordinates(PlanetaryFace::PlanetaryFace pFace, const FaceCoordinates& pCoordinate, const float& pHeight = 0);
	explicit PlanetaryTreeCoordinates(const FVector& pPosition, const float& pHeight = 0);
	explicit PlanetaryTreeCoordinates(PlanetaryFace::PlanetaryFace pFace, const FVector& pPosition, const float& pHeight = 0);
	explicit PlanetaryTreeCoordinates(PlanetaryFace::PlanetaryFace pFace, const float& pXCoordinate, const float& pYCoordinate, const float& pHeight = 0);
	~PlanetaryTreeCoordinates();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Get the planetary direction at these coordinates
	 */
	FORCEINLINE FVector GetDirection() const;

	/**
	 * Get the planetary position at these coordinates
	 */
	FORCEINLINE FVector GetPosition() const;

	/**
	 * Get the planetary position at these coordinates involving the surface height
	 */
	FORCEINLINE FVector GetPosition(const float& pHeight) const;

	/**
	 * Get the planetary face at these coordinates
	 */
	FORCEINLINE PlanetaryFace::PlanetaryFace GetFace() const;

	/**
	 * Get the planetary coordinates state
	 */
	FORCEINLINE PlanetaryCoordinateState::PlanetaryCoordinateState GetState() const;

	/**
	 * Get the planetary surface height at these position
	 */
	FORCEINLINE float GetSurfaceHeight() const;

	/**
	 * Set the planetary coordinates state
	 */
	FORCEINLINE void SetState(PlanetaryCoordinateState::PlanetaryCoordinateState pState);

	/**
	 * Set the planetary surface height at these position
	 */
	FORCEINLINE void SetSurfaceHeight(const float& pHeight);

	/**
	 * Assignment operator
	 */
	const PlanetaryTreeCoordinates& operator = (const PlanetaryTreeCoordinates& pCoordinate);
};

//==========================================
// Inline
//==========================================
#include "PlanetaryTreeCoordinates.inl"
