// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================

//==========================================
// Class definition
//==========================================
/**
 * Generic quad tree  class
 */
template<typename ElementType>
class TQuadTree
{
protected:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * The quad tree root node
	 */
	ElementType* mRoot;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	TQuadTree();
	virtual ~TQuadTree();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Get the root node of the quad tree
	 */
	const ElementType* GetRoot() const;

};

/******************************************************************************************************/
template<typename ElementType>
TQuadTree<ElementType>::TQuadTree()
{
	
}

/******************************************************************************************************/
template<typename ElementType>
TQuadTree<ElementType>::~TQuadTree()
{
	
}

/******************************************************************************************************/
template<typename ElementType>
const ElementType* TQuadTree<ElementType>::GetRoot() const
{
	return this->mRoot;
}
