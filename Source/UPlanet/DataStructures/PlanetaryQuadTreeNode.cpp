
#include "../UPlanet.h"
#include "../PlanetaryMap.h"
#include "../Factories/PlanetaryFactory.h"
#include "PlanetaryQuadTree.h"
#include "PlanetaryQuadTreeNode.h"
#include "../Services/PlanetFactoryService.h"


//==========================================
// Static variables initialization
//==========================================
/************************************************************************************************/
uint16 FPlanetaryQuadTreeNode::sQuadrantOffset[4] = { 0, 4, 36, 40 };

/************************************************************************************************/
uint16 FPlanetaryQuadTreeNode::sQuadrantHeightOffset[4] = { 0, 8, 136, 144 };

/************************************************************************************************/
SPlanetaryQuadTreeNodeInitializer g_sStaticInit; // Just for static init

/************************************************************************************************/
SPlanetaryQuadTreeNodeInitializer::SPlanetaryQuadTreeNodeInitializer()
{
	FPlanetaryQuadTreeNode::InitializeQuadrantTables();
}

/************************************************************************************************/
FPlanetaryQuadTreeNode::FPlanetaryQuadTreeNode(FPlanetaryQuadTree* pOwner, PlanetaryFace::PlanetaryFace pFace) :
TQuadTreeNode(), mOwner( pOwner )
{
	this->InitializeNode( pFace, PlanetaryQuadrant::eNone );
}

/************************************************************************************************/
FPlanetaryQuadTreeNode::FPlanetaryQuadTreeNode(FPlanetaryQuadTreeNode* pParent, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) :
TQuadTreeNode(pParent), mOwner( pParent->mOwner )
{
	this->InitializeNode( pParent->GetFace(), pQuadrant );
}

/************************************************************************************************/
FPlanetaryQuadTreeNode::~FPlanetaryQuadTreeNode()
{
	// Clear the parent and neighbor pointers that point to this node
	if 
		( this->mParent )
	{
		FPlanetaryQuadTreeNode* lParent = StaticCast<FPlanetaryQuadTreeNode*>( this->mParent );
		PlanetaryQuadrant::PlanetaryQuadrant lQuadrant = this->GetQuadrant();
		lParent->mChildren[ lQuadrant ] = NULL;
		for 
			( uint8 lCurrNeighbor = 0; lCurrNeighbor < 4; lCurrNeighbor++ )
		{
			bool lIsNeighborValid = this->mNodeNeighbors[ lCurrNeighbor ] != NULL;
			if 
				( lIsNeighborValid )
			{
				for 
					( uint8 lCurrSubNeighbor = 0; lCurrSubNeighbor < 4; lCurrSubNeighbor++ )
				{
					FPlanetaryQuadTreeNode* lSubNeighbor = this->mNodeNeighbors[ lCurrNeighbor ]->mNodeNeighbors[ lCurrSubNeighbor ];
					bool lIsSubNeighborIsThis = lSubNeighbor == this;
					if 
						( lIsSubNeighborIsThis )
					{
						lSubNeighbor = NULL;
					}
				}
			}
		}
	}

	// Get planetary factories services and destroy the node
	TArray<IInterface_PluginService*> lServices = this->mOwner->OwnerMap()->FactoryService()->GetActiveServices();
	for
		( uint8 lCurrService = 0; lCurrService < lServices.Num(); lCurrService++ )
	{
		IInterface_PluginService* lService = lServices[ lCurrService ];
		UPlanetaryFactory* lFactoryService = Cast<UPlanetaryFactory>( lService );
		if
			( lFactoryService != NULL &&
			  lFactoryService->AffectsNode( this ) )
		{
			lFactoryService->DestroyNode( this );
		}
	}
}

/************************************************************************************************/
void FPlanetaryQuadTreeNode::InitializeQuadrantTables()
{
	// Calculate the offset to each quadrant in the vertex table
	FPlanetaryQuadTreeNode::sQuadrantOffset[ PlanetaryQuadrant::eTopLeft ]     = 0;
	FPlanetaryQuadTreeNode::sQuadrantOffset[ PlanetaryQuadrant::eTopRight ]    = (SURFACE_MAP_WIDTH - 1) / 2;
	FPlanetaryQuadTreeNode::sQuadrantOffset[ PlanetaryQuadrant::eBottomRight ] = (SURFACE_MAP_COUNT - 1) / 2;
	FPlanetaryQuadTreeNode::sQuadrantOffset[ PlanetaryQuadrant::eBottomLeft ]  = FPlanetaryQuadTreeNode::sQuadrantOffset[ PlanetaryQuadrant::eBottomRight ] - FPlanetaryQuadTreeNode::sQuadrantOffset[ PlanetaryQuadrant::eTopRight ];

	// Calculate the offset to each quadrant in the height table
	FPlanetaryQuadTreeNode::sQuadrantHeightOffset[ PlanetaryQuadrant::eTopLeft ]     = 0;
	FPlanetaryQuadTreeNode::sQuadrantHeightOffset[ PlanetaryQuadrant::eTopRight ]    = (HEIGHT_MAP_WIDTH - 1) / 2;
	FPlanetaryQuadTreeNode::sQuadrantHeightOffset[ PlanetaryQuadrant::eBottomRight ] = (HEIGHT_MAP_COUNT - 1) / 2;
	FPlanetaryQuadTreeNode::sQuadrantHeightOffset[ PlanetaryQuadrant::eBottomLeft ]  = FPlanetaryQuadTreeNode::sQuadrantHeightOffset[ PlanetaryQuadrant::eBottomRight ] - FPlanetaryQuadTreeNode::sQuadrantHeightOffset[ PlanetaryQuadrant::eTopRight ];
}

/************************************************************************************************/
float			  FPlanetaryQuadTreeNode::GetHeightFromMap(const PlanetaryTreeCoordinates& pCoordinates) const
{
	return this->GetHeightFromMap( pCoordinates.GetXCoordinate(), pCoordinates.GetYCoordinate() );
}

/************************************************************************************************/
float			  FPlanetaryQuadTreeNode::GetHeightFromMap(float pXCoordinate, float pYCoordinate) const
{
	// width = SURFACE_MAP_WIDTH
	// ElementSize = 4 bytes
	// Channels = 1 (even if 4 channels of 1 bytes actually)
	float lHeight;
	int8 lElementSize = 4;
	float lX = (pXCoordinate - this->mNodeCorners[ 0 ]) / (this->mNodeCorners[ 2 ] - this->mNodeCorners[ 0 ] ) * ( SURFACE_MAP_WIDTH - 1 );
	float lY = (pYCoordinate - this->mNodeCorners[ 1 ]) / (this->mNodeCorners[ 3 ] - this->mNodeCorners[ 1 ] ) * ( SURFACE_MAP_WIDTH - 1 );
	int32 nX = FMath::Min( SURFACE_MAP_WIDTH - 2, FMath::Max( 0, (int32)lX ) );
	int32 nY = FMath::Min( SURFACE_MAP_WIDTH - 2, FMath::Max( 0, (int32)lY ) );
	float lRatioX = lX - nX;
	float lRatioY = lY - nY;

	float* lValue = StaticCast<float*>(this->mElement->Heightmap->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE)) + lElementSize * (SURFACE_MAP_WIDTH * nY + nX);
	lHeight =   lValue[ 0 ] * (1 - lRatioX) * (1 - lRatioY) +
				lValue[ 1 ] * lRatioX * (1 - lRatioY) +
				lValue[ SURFACE_MAP_WIDTH ] * (1 - lRatioX) * lRatioY +
				lValue[ SURFACE_MAP_WIDTH + 1 ] * lRatioX * lRatioY;
	this->mElement->Heightmap->PlatformData->Mips[0].BulkData.Unlock();

	return lHeight;
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::Release()
{
	this->mElement->ReleasePhysics();
	this->mElement->UnregisterComponent();
	this->mElement->DetachFromParent();
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::InitializeNode(PlanetaryFace::PlanetaryFace pFace, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant)
{
	UPlanetaryMap* lPlanet = const_cast<UPlanetaryMap*>( this->GetOwnerTree()->OwnerMap() );
	UObject* lOuter = lPlanet->GetOuter();

	// Set up node flags
	FPlanetaryQuadTreeNode* lParent = NULL;
	int32 lLevel = 0;
	if 
		( this->mParent )
	{
		lParent = StaticCast<FPlanetaryQuadTreeNode*>( this->mParent );
		lLevel = lParent->GetLevel() + 1;
	}

	// Allocate the owned elements and its resources
	FString lNodeName = "PlanetaryNode_";
	lNodeName.AppendInt(pFace);
	lNodeName.Append("_");
	lNodeName.AppendInt(pQuadrant);
	lNodeName.Append("_");
	lNodeName.AppendInt(lLevel);
	this->mElement = NewObject<UPlanetaryNodeMeshComponent>( lOuter, UPlanetaryNodeMeshComponent::StaticClass(), *lNodeName );
	this->mElement->AttachTo(lPlanet);
	this->mElement->RegisterComponent();
	this->mElement->Heightmap = UTexture2D::CreateTransient(SURFACE_MAP_WIDTH, SURFACE_MAP_WIDTH, EPixelFormat::PF_FloatRGBA);
	this->mElement->Heightmap->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
	this->mElement->Heightmap->SRGB = 0;
	this->mElement->Heightmap->AddToRoot();
	this->mElement->Heightmap->UpdateResource();
	this->mElement->BumpTexture = UTexture2D::CreateTransient(HEIGHT_MAP_WIDTH, HEIGHT_MAP_WIDTH, EPixelFormat::PF_FloatRGBA);
	this->mElement->BumpTexture->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
	this->mElement->BumpTexture->SRGB = 0;
	this->mElement->BumpTexture->AddToRoot();
	this->mElement->BumpTexture->UpdateResource();
	this->mElement->GroundTexture = UTexture2D::CreateTransient(HEIGHT_MAP_WIDTH, HEIGHT_MAP_WIDTH, EPixelFormat::PF_FloatRGBA);
	this->mElement->GroundTexture->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
	this->mElement->GroundTexture->SRGB = 0;
	this->mElement->GroundTexture->AddToRoot();
	this->mElement->GroundTexture->UpdateResource();

	int32 lNodeFlags = PlanetaryNodeFlags::eNodeDirty | ((lLevel << 5) & PlanetaryNodeFlags::eLevelMask) | ((pQuadrant << 3) & PlanetaryNodeFlags::eQuadrantMask) | (pFace & PlanetaryNodeFlags::eFaceMask);
	this->mNodeFlags = StaticCast<PlanetaryNodeFlags::PlanetaryNodeFlags>( lNodeFlags );
	this->mSortedQuadrants[ 0 ] = PlanetaryQuadrant::eTopLeft; 
	this->mSortedQuadrants[ 1 ] = PlanetaryQuadrant::eTopRight; 
	this->mSortedQuadrants[ 2 ] = PlanetaryQuadrant::eBottomLeft; 
	this->mSortedQuadrants[ 3 ] = PlanetaryQuadrant::eBottomRight;

	if 
		( lParent )
	{
		// Update neighbor pointers (and our neighbors' neighbor pointers)
		lParent->mChildren[ pQuadrant ] = this;
		this->RefreshNeighbors();
		for 
			( int8 lCurrNeighbor = 0; lCurrNeighbor < 4; lCurrNeighbor++ )
		{
			if 
				( this->mNodeNeighbors[ lCurrNeighbor ] )
			{
				this->mNodeNeighbors[ lCurrNeighbor ]->RefreshNeighbors();
			}
		}
	}

	// Set up the corner boundaries for this node
	switch 
		( pQuadrant )
	{
	case PlanetaryQuadrant::eTopLeft:
		{
			this->mNodeCorners[ 0 ] = lParent->mNodeCorners[ 0 ];
			this->mNodeCorners[ 1 ] = lParent->mNodeCorners[ 1 ];
			this->mNodeCorners[ 2 ] = lParent->mNodeCorners[ 0 ] + (lParent->mNodeCorners[ 2 ] - lParent->mNodeCorners[ 0 ]) * 0.5f;
			this->mNodeCorners[ 3 ] = lParent->mNodeCorners[ 1 ] + (lParent->mNodeCorners[ 3 ] - lParent->mNodeCorners[ 1 ]) * 0.5f;
		}
		break;
	case PlanetaryQuadrant::eTopRight:
		{
			this->mNodeCorners[ 0 ] = lParent->mNodeCorners[ 0 ] + (lParent->mNodeCorners[ 2 ] - lParent->mNodeCorners[ 0 ]) * 0.5f;
			this->mNodeCorners[ 1 ] = lParent->mNodeCorners[ 1 ];
			this->mNodeCorners[ 2 ] = lParent->mNodeCorners[ 2 ];
			this->mNodeCorners[ 3 ] = lParent->mNodeCorners[ 1 ] + (lParent->mNodeCorners[ 3 ] - lParent->mNodeCorners[ 1 ]) * 0.5f;
		}
		break;
	case PlanetaryQuadrant::eBottomLeft:
		{
			this->mNodeCorners[ 0 ] = lParent->mNodeCorners[ 0 ];
			this->mNodeCorners[ 1 ] = lParent->mNodeCorners[ 1 ] + (lParent->mNodeCorners[ 3 ] - lParent->mNodeCorners[ 1 ]) * 0.5f;
			this->mNodeCorners[ 2 ] = lParent->mNodeCorners[ 0 ] + (lParent->mNodeCorners[ 2 ] - lParent->mNodeCorners[ 0 ]) * 0.5f;
			this->mNodeCorners[ 3 ] = lParent->mNodeCorners[ 3 ];
		}
		break;
	case PlanetaryQuadrant::eBottomRight:
		{
			this->mNodeCorners[ 0 ] = lParent->mNodeCorners[ 0 ] + (lParent->mNodeCorners[ 2 ] - lParent->mNodeCorners[ 0 ]) * 0.5f;
			this->mNodeCorners[ 1 ] = lParent->mNodeCorners[ 1 ] + (lParent->mNodeCorners[ 3 ] - lParent->mNodeCorners[ 1 ]) * 0.5f;
			this->mNodeCorners[ 2 ] = lParent->mNodeCorners[ 2 ];
			this->mNodeCorners[ 3 ] = lParent->mNodeCorners[ 3 ];
		}
		break;
	default:	// Top-level node, no parent exists
		{
			this->mNodeCorners[ 0 ] = 0.0f;
			this->mNodeCorners[ 1 ] = 0.0f;
			this->mNodeCorners[ 2 ] = 1.0f;
			this->mNodeCorners[ 3 ] = 1.0f;
		}
		break;
	}

	// Build coordinates
	this->BuildCoordinates( pFace );

	// Get planetary factories services and build the node
	TArray<IInterface_PluginService*> lServices = lPlanet->FactoryService()->GetActiveServices();
	for 
		( uint8 lCurrService = 0; lCurrService < lServices.Num(); lCurrService++ )
	{
		IInterface_PluginService* lService = lServices[ lCurrService ];
		UPlanetaryFactory* lFactoryService = Cast<UPlanetaryFactory>( lService );
		if 
			( lFactoryService != NULL &&
			  lFactoryService->AffectsNode( this ) )
		{
			lFactoryService->BuildNode( this );
		}
	}

	// Compute normals
	this->ComputeNormals();

	// Build the bumpmap
	this->BuildBumpmap();

	// Build vertices and heightmap
	this->BuildVertices();

	// Update RHI Textures resources
	this->mElement->BumpTexture->UpdateResource();
	this->mElement->GroundTexture->UpdateResource();
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::BuildCoordinates(PlanetaryFace::PlanetaryFace pFace)
{
	int32 lIndex = 0;
	float lPlanetaryRadius = this->mOwner->OwnerMap()->PlanetaryRadius;
	float lXOffset = (this->mNodeCorners[ 2 ] - this->mNodeCorners[ 0 ]) / (HEIGHT_MAP_WIDTH - 1);
	float lYOffset = (this->mNodeCorners[ 3 ] - this->mNodeCorners[ 1 ]) / (HEIGHT_MAP_WIDTH - 1);
	float lY = this->mNodeCorners[ 1 ] - lYOffset;
	for
		( int32 lCurrY = 0; lCurrY < BORDER_MAP_WIDTH; lCurrY++ )
	{
		float lX = this->mNodeCorners[0] - lXOffset;
		for
			( int32 lCurrX = 0; lCurrX < BORDER_MAP_WIDTH; lCurrX++ )
		{
			this->mCookedCoordinates[ lIndex++ ] = PlanetaryTreeCoordinates( pFace, lX, lY, lPlanetaryRadius );
			lX += lXOffset;
		}

		lY += lYOffset;
	}
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::ComputeNormals()
{
	int32 lIndex = 0;
	for
		( int32 lY = 0; lY < HEIGHT_MAP_WIDTH; lY++ )
	{
		int32 lCoord = (lY + 1) * BORDER_MAP_WIDTH + 1;
		for
			( int32 lX = 0; lX < HEIGHT_MAP_WIDTH; lX++ )
		{
			FVector lCenter = this->mCookedCoordinates[lCoord].GetPosition();
			FVector lNorth  = this->mCookedCoordinates[lCoord - BORDER_MAP_WIDTH].GetPosition() - lCenter;
			FVector lWest   = this->mCookedCoordinates[lCoord - 1].GetPosition() - lCenter;
			FVector lSouth  = this->mCookedCoordinates[lCoord + BORDER_MAP_WIDTH].GetPosition() - lCenter;
			FVector lEast   = this->mCookedCoordinates[lCoord + 1].GetPosition() - lCenter;
			this->mCookedNormals[lIndex] = (lNorth ^ lWest) + (lSouth ^ lEast);
			this->mCookedNormals[lIndex].Normalize();
			lCoord++;
			lIndex++;
		}
	}
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::BuildBumpmap()
{
	// Use normals for filling the Bumpmap texture
	uint8* lData = StaticCast<uint8*>( this->mElement->BumpTexture->PlatformData->Mips[ 0 ].BulkData.Lock( LOCK_READ_WRITE ) );
	{
		int32 lIndex = 0;
		for
			( int32 y = 0; y < HEIGHT_MAP_WIDTH; y++ )
		{
			for
				( int32 x = 0; x < HEIGHT_MAP_WIDTH; x++ )
			{
				FVector lNormal = this->mCookedNormals[ lIndex ];
				lNormal *= 128.0f / lNormal.Size();
				*lData++ = StaticCast<uint8>( lNormal.X + 128.0f );
				*lData++ = StaticCast<uint8>( lNormal.Y + 128.0f );
				*lData++ = StaticCast<uint8>( lNormal.Z + 128.0f );
				*lData++ = 255; // See if Okay
				lIndex++;
			}
		}
	}
	this->mElement->BumpTexture->PlatformData->Mips[ 0 ].BulkData.Unlock();
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::BuildVertices()
{
	float* lHeightMap = StaticCast<float*>( this->mElement->Heightmap->PlatformData->Mips[ 0 ].BulkData.Lock( LOCK_READ_WRITE ) );
	int32 lIndex = 0;
	for
		( int32 lY = 0; lY < SURFACE_MAP_WIDTH; lY++ )
	{
		int32 lCoord  = (lY * HEIGHT_MAP_SCALE + 1) * BORDER_MAP_WIDTH + 1;
		int32 lNormal = lY * HEIGHT_MAP_SCALE * HEIGHT_MAP_WIDTH;
		for
			( int32 lX = 0; lX < SURFACE_MAP_WIDTH; lX++ )
		{
			PlanetaryTreeCoordinates lCurrCoordinate = this->mCookedCoordinates[ lCoord ];
			*lHeightMap++ = lCurrCoordinate.GetSurfaceHeight();
			FPlanetaryMeshVertex* lCurrVertex = &this->mCookedVertices[ lIndex++ ];
			lCurrVertex->Position  = lCurrCoordinate.GetPosition();
			lCurrVertex->Normal    = this->mCookedNormals[ lNormal ];
			lCurrVertex->TexCoords = lCurrCoordinate.AsVector2D();
			lCoord  += HEIGHT_MAP_SCALE;
			lNormal += HEIGHT_MAP_SCALE;
		}
	}

	// Unlock once updated
	this->mElement->Heightmap->PlatformData->Mips[ 0 ].BulkData.Unlock();
}

/************************************************************************************************/
int32			  FPlanetaryQuadTreeNode::UpdateNodeMesh()
{
	const UPlanetaryMap* lOwnerMap = this->mOwner->OwnerMap();

	if
		( lOwnerMap != NULL )
	{
		UE_LOG( LogClass, Log, TEXT( "FPlanetaryQuadTreeNode::UpdateNodeMesh" ) );

		// Update the maximal depth if needed
		const_cast<UPlanetaryMap*>( lOwnerMap )->SetMaxDepth( this->GetLevel() );

		// Get the camera position and clear the node flags set by this function
		FVector lCameraLocation = lOwnerMap->CameraLocation();
		
		uint32 lNodeFlags = StaticCast<uint32>( this->mNodeFlags );
		lNodeFlags &= ~( PlanetaryNodeFlags::eCameraInMask | PlanetaryNodeFlags::eBeyondHorizonMask | PlanetaryNodeFlags::eOutsideFrustumMask );
		this->mNodeFlags = StaticCast<PlanetaryNodeFlags::PlanetaryNodeFlags>( lNodeFlags );

		// Calculate the map coordinates for the center of each quadrant
		PlanetaryFace::PlanetaryFace lFace = this->GetFace();
		float lMiddles[2] = { this->GetMidX(), this->GetMidY() };
		float lXMinMaxCoordinates[2] = { (this->GetMinX() + lMiddles[0]) * 0.5, (this->GetMaxX() + lMiddles[0]) * 0.5 };
		float lYMinMaxCoordinates[2] = { (this->GetMinY() + lMiddles[1]) * 0.5, (this->GetMaxY() + lMiddles[1]) * 0.5 };
		PlanetaryTreeCoordinates lQuadrantCoordinates[4] =
		{
			PlanetaryTreeCoordinates( lFace, lXMinMaxCoordinates[ 0 ], lYMinMaxCoordinates[ 0 ] ),
			PlanetaryTreeCoordinates( lFace, lXMinMaxCoordinates[ 1 ], lYMinMaxCoordinates[ 0 ] ),
			PlanetaryTreeCoordinates( lFace, lXMinMaxCoordinates[ 0 ], lYMinMaxCoordinates[ 1 ] ),
			PlanetaryTreeCoordinates( lFace, lXMinMaxCoordinates[ 1 ], lYMinMaxCoordinates[ 1 ] )
		};

		float lPlanetaryRadius    = lOwnerMap->PlanetaryRadius;
		float lPlanetaryMaxHeight = lOwnerMap->PlanetaryMaxHeight;

		// Calculate the 3D position for the center of each quadrant
		FVector lQuadrantCenters[4] =
		{
			lQuadrantCoordinates[ 0 ].GetPosition( lPlanetaryRadius ),
			lQuadrantCoordinates[ 1 ].GetPosition( lPlanetaryRadius ),
			lQuadrantCoordinates[ 2 ].GetPosition( lPlanetaryRadius ),
			lQuadrantCoordinates[ 3 ].GetPosition( lPlanetaryRadius )
		};

		// Calculate the distance to the camera for the center of each quadrant
		float lDistanceToCenters[4] =
		{
			FVector::Dist( lCameraLocation, lQuadrantCenters[ 0 ] ),
			FVector::Dist( lCameraLocation, lQuadrantCenters[ 1 ] ),
			FVector::Dist( lCameraLocation, lQuadrantCenters[ 2 ] ),
			FVector::Dist( lCameraLocation, lQuadrantCenters[ 3 ] )
		};

		// Now sort the quadrants by distance (used for updating and rendering order)
		// Don't use the actual distances because there will be precision problems
		float lXCoordinate;
		float lYCoordinate;
		PlanetaryHelper::GetFaceCoordinatesFromPosition( lFace, lCameraLocation, lXCoordinate, lYCoordinate );
		if 
			( lXCoordinate < lMiddles[ 0 ] )
		{
			if 
				( lYCoordinate < lMiddles[ 1 ] )
			{
				this->mSortedQuadrants[ 0 ] = PlanetaryQuadrant::eTopLeft;	    // Closest
				this->mSortedQuadrants[ 1 ] = PlanetaryQuadrant::eTopRight;	    // Doesn't matter
				this->mSortedQuadrants[ 2 ] = PlanetaryQuadrant::eBottomLeft;	// Doesn't matter
				this->mSortedQuadrants[ 3 ] = PlanetaryQuadrant::eBottomRight;	// Farthest
			}
			else
			{
				this->mSortedQuadrants[ 0 ] = PlanetaryQuadrant::eBottomLeft;	// Closest
				this->mSortedQuadrants[ 1 ] = PlanetaryQuadrant::eTopLeft;		// Doesn't matter
				this->mSortedQuadrants[ 2 ] = PlanetaryQuadrant::eBottomRight;	// Doesn't matter
				this->mSortedQuadrants[ 3 ] = PlanetaryQuadrant::eTopRight;		// Farthest
			}
		}
		else
		{
			if 
				( lYCoordinate < lMiddles[ 1 ] )
			{
				this->mSortedQuadrants[ 0 ] = PlanetaryQuadrant::eTopRight;		// Closest
				this->mSortedQuadrants[ 1 ] = PlanetaryQuadrant::eTopLeft;		// Doesn't matter
				this->mSortedQuadrants[ 2 ] = PlanetaryQuadrant::eBottomRight;	// Doesn't matter
				this->mSortedQuadrants[ 3 ] = PlanetaryQuadrant::eBottomLeft;	// Farthest
			}
			else
			{
				this->mSortedQuadrants[ 0 ] = PlanetaryQuadrant::eBottomRight;	// Closest
				this->mSortedQuadrants[ 1 ] = PlanetaryQuadrant::eTopRight;	    // Doesn't matter
				this->mSortedQuadrants[ 2 ] = PlanetaryQuadrant::eBottomLeft;	// Doesn't matter
				this->mSortedQuadrants[ 3 ] = PlanetaryQuadrant::eTopLeft;	    // Farthest
			}
		}

		// Calculate the quadrant size (the length of the diagonal), and use that to calculate the radius for view frustum culling
		FVector lTopLeftQuadrantCenter     = lQuadrantCenters[ PlanetaryQuadrant::eTopLeft ];
		FVector lBottomRightQuadrantCenter = lQuadrantCenters[ PlanetaryQuadrant::eBottomRight ];
		float lQuadrantSize = FVector::Dist( lTopLeftQuadrantCenter, lBottomRightQuadrantCenter );
		float lRadius = FMath::Max( lQuadrantSize, lPlanetaryMaxHeight );

		// For split priority, we can't use the true quadrant size because quadrants near the corners of cube faces are smaller than quadrants mear the center
		// This caused an odd-looking artifact at the corners where nodes nearest the camera were at a lower detail level than nodes farther away
		lQuadrantSize   = (this->GetMaxX() - this->GetMinX()) * lPlanetaryRadius;
		float lPriority = (lQuadrantSize <= lPlanetaryMaxHeight * 0.001f) ? 0.0f : lOwnerMap->SplitFactor * powf( lQuadrantSize, lOwnerMap->SplitPower );

		float lHorizonDistance = lOwnerMap->HorizonDistance();
		FConvexVolume lFrustum = lOwnerMap->Frustum();
		PlanetaryTreeCoordinates lCameraCoordinates = lOwnerMap->CameraCoordinates();

		// Update each quadrant in forward Z order (to split nodes closer to the camera first)
		for 
			( uint8 lCurrQuadrant = 0; lCurrQuadrant < 4; lCurrQuadrant++ )
		{
			PlanetaryQuadrant::PlanetaryQuadrant lQuadrant = this->mSortedQuadrants[ lCurrQuadrant ];

			// Check to see if the camera's in this quadrant
			if 
				( this->HitTest( lCameraCoordinates, lQuadrant ) )
			{
				lNodeFlags |= (lQuadrant << 10) & PlanetaryNodeFlags::eCameraInMask;
				this->mNodeFlags = StaticCast<PlanetaryNodeFlags::PlanetaryNodeFlags>( lNodeFlags );
			}
			else
			{
				// Check the quadrant against the horizon distance
				if 
					( lHorizonDistance > 0.0f && 
					  lDistanceToCenters[ lQuadrant ] - lRadius > lHorizonDistance )
				{
					lNodeFlags |= 1 << (16 + lQuadrant);
					this->mNodeFlags = StaticCast<PlanetaryNodeFlags::PlanetaryNodeFlags>( lNodeFlags );
					this->Merge( lQuadrant );
					continue;
				}

				// Check the quadrant against the view frustum
				if 
					( lFrustum.IntersectSphere( lQuadrantCenters[ lQuadrant ], lRadius ) == false )
				{
					lNodeFlags |= 1 << (20 + lQuadrant);
					this->mNodeFlags = StaticCast<PlanetaryNodeFlags::PlanetaryNodeFlags>( lNodeFlags );
					this->Merge( lQuadrant );
					continue;
				}
			}

			// If the quadrant wasn't culled, check its distance against the priority 
			// to see if it should be merged
			if 
				( lDistanceToCenters[ lQuadrant ] > lPriority )
			{
				this->Merge( lQuadrant );
				continue;
			}

			// If we're not trying to merge it, we should split it 
			// (if it does not already exist) and update its children
			this->Split( lQuadrant );
			bool lIsChildValid = this->mChildren[ lQuadrant ] != NULL;
			if 
				( lIsChildValid )
			{
				FPlanetaryQuadTreeNode* lCurrChild = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[ lQuadrant ] );
				lCurrChild->UpdateNodeMesh();
			}
		}

		int32 lTriangleCount = this->UpdateTriangles();

		UE_LOG( LogClass, Log, TEXT( "End of FPlanetaryQuadTreeNode::UpdateNodeMesh with %d Triangles" ), lTriangleCount );
		return lTriangleCount;
	}

	UE_LOG( LogClass, Log, TEXT( "End of FPlanetaryQuadTreeNode::UpdateNodeMesh with 0 Triangles" ) );
	return 0;
}

/************************************************************************************************/
int32			  FPlanetaryQuadTreeNode::UpdateTriangles()
{
	static const uint16 lFan[10]     = { 20, 22, 12, 2, 10, 18, 28, 38, 30, 22 };
	static const uint16 lOther[4][6] = { { 0, 10, 2, 2, 12, 4 }, { 4, 12, 22, 22, 30, 40 }, { 40, 30, 38, 38, 28, 36 }, { 36, 28, 18, 18, 10, 0 } };
	static const uint16 lEdge[4][2]  = { { 1, 3 }, { 13, 31 }, { 39, 37 }, { 27, 9 } };
	static uint16 lMesh[48];
	static uint16 lMeshIndex;
	PlanetaryQuadrant::PlanetaryQuadrant lQuadrant;
	int32 lTriangleCount = 0;

	TArray<FPlanetaryMeshTriangle> lTriangles;
	// Compute indices and make triangles
	for
		( int8 lCurrQuadrant = 0; lCurrQuadrant < 4; lCurrQuadrant++ )
	{
		lQuadrant = this->mSortedQuadrants[ lCurrQuadrant ];
		if
			( this->IsBeyondHorizon( lQuadrant ) ||
			  this->IsOutsideFrustum( lQuadrant ) )
		{
			continue;
		}

		if
			( this->mChildren[ lQuadrant ] != NULL )
		{
			lTriangleCount += StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[ lQuadrant ] )->UpdateTriangles();
		}
		else
		{
			// Add a new node to the planetary map counter.
			const_cast<UPlanetaryMap*>( this->mOwner->OwnerMap() )->IncrementNodeCount();

			// Regardless of whether any of the neighbors are split, 
			// each quadrant has a fan of 8 triangles in its center
			for
				( int32 lCurrFanIndex = 0; lCurrFanIndex < 10; lCurrFanIndex++ )
			{
				lMesh[ lCurrFanIndex ] = lFan[ lCurrFanIndex ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lCurrQuadrant ];
			}

			// First triangles of the Fan (always starts by vertex 0)
			for 
				( int8 lCurrTriangle = 0; lCurrTriangle < 8; lCurrTriangle++ )
			{
				FPlanetaryMeshTriangle lTriangle;
				lTriangle.Vertex0 = this->mCookedVertices[ lMesh[ 0 ] ];
				lTriangle.Vertex1 = this->mCookedVertices[ lMesh[ lCurrTriangle + 1 ] ];
				lTriangle.Vertex2 = this->mCookedVertices[ lMesh[ lCurrTriangle + 2 ] ];
				lTriangles.Add( lTriangle );
			}

			lTriangleCount += 8;

			// Each quadrant has 2 triangles on each edge, 
			// which are split into 4 if the quadrant's neighbor is split.
			lMeshIndex = 0;
			for 
				( int8 lCurrNeighbor = 0; lCurrNeighbor < 4; lCurrNeighbor++ )
			{
				bool lIsNeighborSplit = this->GetQuadrantNeighbor( lQuadrant, (PlanetaryEdge::PlanetaryEdge)lCurrNeighbor ) != NULL;

				lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 0 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
				lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 1 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
				if 
					( lIsNeighborSplit )
				{
					lMesh[ lMeshIndex++ ] = lEdge[ lCurrNeighbor ][ 0 ]  + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
					lMesh[ lMeshIndex++ ] = lEdge[ lCurrNeighbor ][ 0 ]  + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
					lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 1 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
				}
				lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 2 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];

				lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 3 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
				lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 4 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
				if 
					( lIsNeighborSplit )
				{
					lMesh[ lMeshIndex++ ] = lEdge[ lCurrNeighbor ][ 1 ]  + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
					lMesh[ lMeshIndex++ ] = lEdge[ lCurrNeighbor ][ 1 ]  + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
					lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 4 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
				}
				lMesh[ lMeshIndex++ ] = lOther[ lCurrNeighbor ][ 5 ] + FPlanetaryQuadTreeNode::sQuadrantOffset[ lQuadrant ];
			}

			int16 lEdgeTriangleCount = 0;
			for 
				( int16 lCurrIndex = 0; lCurrIndex < lMeshIndex; lCurrIndex += 3, lEdgeTriangleCount++ )
			{
				FPlanetaryMeshTriangle lTriangle;
				lTriangle.Vertex0 = this->mCookedVertices[ lMesh[ lCurrIndex ] ];
				lTriangle.Vertex1 = this->mCookedVertices[ lMesh[ lCurrIndex + 1 ] ];
				lTriangle.Vertex2 = this->mCookedVertices[ lMesh[ lCurrIndex + 2 ] ];
				lTriangles.Add( lTriangle );
			}

			lTriangleCount += lEdgeTriangleCount;
		}
	}

	this->mElement->SetMeshTriangles( lTriangles );

	return lTriangleCount;
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::RefreshNeighbors()
{
	for
		( uint8 lCurrNeighbor = 0; lCurrNeighbor < 4; lCurrNeighbor++ )
	{
		this->mNodeNeighbors[lCurrNeighbor] = this->GetNeighborAtEdge( (PlanetaryEdge::PlanetaryEdge)lCurrNeighbor );
	}
}

/************************************************************************************************/
void			  FPlanetaryQuadTreeNode::Split(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant)
{
	if
		( pQuadrant >= 4 )
	{
		return;
	}

	// If the requested quadrant is already split, return
	if
		( this->mChildren[ pQuadrant ] != NULL )
	{
		return;
	}

	// Make sure the affected edges have neighbors by calling the force-splitting version of FindNeighbor()
	switch
		( pQuadrant )
	{
		case PlanetaryQuadrant::eTopLeft:
			{
				if
					( this->GetNeighborAtEdge( PlanetaryEdge::eTopEdge, true ) == false ||
					  this->GetNeighborAtEdge( PlanetaryEdge::eLeftEdge, true ) == false )
				{
					return;
				}
			}
			break;
		case PlanetaryQuadrant::eTopRight:
			{
				if
					( this->GetNeighborAtEdge( PlanetaryEdge::eTopEdge, true ) == false ||
					  this->GetNeighborAtEdge( PlanetaryEdge::eRightEdge, true ) == false )
				{
					return;
				}
			}
			break;
		case PlanetaryQuadrant::eBottomLeft:
			{
				if
					( this->GetNeighborAtEdge( PlanetaryEdge::eBottomEdge, true ) == false ||
					  this->GetNeighborAtEdge( PlanetaryEdge::eLeftEdge, true ) == false )
				{
					return;
				}
			}
			break;
		case PlanetaryQuadrant::eBottomRight:
			{
				if
					( this->GetNeighborAtEdge( PlanetaryEdge::eBottomEdge, true ) == false ||
					  this->GetNeighborAtEdge( PlanetaryEdge::eRightEdge, true ) == false )
				{
					return;
				}
			}
			break;
	}

	UPlanetaryMap* lPlanet = const_cast<UPlanetaryMap*>( this->GetOwnerTree()->OwnerMap() );

	// Finally, check the map to see if we've already split 
	// too many times during this frame
	if
		( lPlanet->CanSplit() )
	{
		this->mChildren[ pQuadrant ] = new FPlanetaryQuadTreeNode( this, pQuadrant );
	}
}

/************************************************************************************************/
bool			  FPlanetaryQuadTreeNode::CanMerge()
{
	// If this node has children of its own, it cannot be merged
	if
		( this->IsLeaf() == false )
	{
		return false;
	}

	// If this node has any neighbors with children bordering this node, it can't be merged
	if
		(this->mNodeNeighbors[PlanetaryEdge::eTopEdge] &&
		(this->GetQuadrantNeighbor(PlanetaryQuadrant::eTopLeft, PlanetaryEdge::eTopEdge) || this->GetQuadrantNeighbor(PlanetaryQuadrant::eTopRight, PlanetaryEdge::eTopEdge)))
	{
		return false;
	}

	if
		(this->mNodeNeighbors[PlanetaryEdge::eRightEdge] &&
		(this->GetQuadrantNeighbor(PlanetaryQuadrant::eTopRight, PlanetaryEdge::eRightEdge) || this->GetQuadrantNeighbor(PlanetaryQuadrant::eBottomRight, PlanetaryEdge::eRightEdge)))
	{
		return false;
	}

	if
		(this->mNodeNeighbors[PlanetaryEdge::eBottomEdge] &&
		(this->GetQuadrantNeighbor(PlanetaryQuadrant::eBottomRight, PlanetaryEdge::eBottomEdge) || this->GetQuadrantNeighbor(PlanetaryQuadrant::eBottomLeft, PlanetaryEdge::eBottomEdge)))
	{
		return false;
	}

	if
		(this->mNodeNeighbors[PlanetaryEdge::eLeftEdge] &&
		(this->GetQuadrantNeighbor(PlanetaryQuadrant::eBottomLeft, PlanetaryEdge::eLeftEdge) || this->GetQuadrantNeighbor(PlanetaryQuadrant::eTopLeft, PlanetaryEdge::eLeftEdge)))
	{
		return false;
	}

	// If none of the above restrictions apply, this node can be merged
	return true;
}

/************************************************************************************************/
bool			  FPlanetaryQuadTreeNode::Merge(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant)
{
	// If the requested quadrant does not exist, return success (nothing to do)
	FPlanetaryQuadTreeNode* lNode = StaticCast<FPlanetaryQuadTreeNode*>(this->mChildren[pQuadrant]);
	if
		(lNode == NULL)
	{
		return true;
	}

	// If any of its children fail to merge, or if it fails the merge test, return failure
	bool lSuccess = true;
	if
		(lNode->Merge(PlanetaryQuadrant::eTopLeft) == false)
	{
		lSuccess = false;
	}

	if
		(lNode->Merge(PlanetaryQuadrant::eTopRight) == false)
	{
		lSuccess = false;
	}

	if
		(lNode->Merge(PlanetaryQuadrant::eBottomLeft) == false)
	{
		lSuccess = false;
	}

	if
		(lNode->Merge(PlanetaryQuadrant::eBottomRight) == false)
	{
		lSuccess = false;
	}

	if
		(lSuccess == false ||
		 lNode->CanMerge() == false)
	{
		return false;
	}

	// Remove the quadrant
	this->mChildren[pQuadrant]->Release();
	delete this->mChildren[pQuadrant];

	return true;
}

/************************************************************************************************/
bool			  FPlanetaryQuadTreeNode::AffectsNode(const PlanetaryTreeCoordinates& pCoordinates, float pRadius)
{
	bool lIsInThisNodeFace = pCoordinates.GetFace() == this->GetFace();
	if
		(lIsInThisNodeFace)
	{
		if
			(pCoordinates.GetXCoordinate() + pRadius > this->GetMinX() &&
			 pCoordinates.GetXCoordinate() - pRadius < this->GetMaxX() &&
			 pCoordinates.GetYCoordinate() + pRadius > this->GetMinY() &&
			 pCoordinates.GetYCoordinate() - pRadius < this->GetMaxY())
		{
			return true;
		}
	}

	return false;
}

/************************************************************************************************/
bool			  FPlanetaryQuadTreeNode::HitTest(const PlanetaryTreeCoordinates& pCoordinates, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const
{
	bool lIsNotInThisNodeFace = pCoordinates.GetFace() != this->GetFace();
	if
		( lIsNotInThisNodeFace )
	{
		return false;
	}

	switch
		( pQuadrant )
	{
		case PlanetaryQuadrant::eTopLeft:	// Is coordinate in top-left quadrant?
			{
				return (pCoordinates.GetXCoordinate() >= this->GetMinX() &&
						pCoordinates.GetXCoordinate() <= this->GetMidX() &&
						pCoordinates.GetYCoordinate() >= this->GetMinY() &&
						pCoordinates.GetYCoordinate() <= this->GetMidY());
			}
		case PlanetaryQuadrant::eTopRight:	 // Is coordinate in top-right quadrant?
			{
				return (pCoordinates.GetXCoordinate() >= this->GetMidX() &&
						pCoordinates.GetXCoordinate() <= this->GetMaxX() &&
						pCoordinates.GetYCoordinate() >= this->GetMinY() &&
						pCoordinates.GetYCoordinate() <= this->GetMidY());
			}
		case PlanetaryQuadrant::eBottomLeft:	// Is coordinate in bottom-left quadrant?
			{
				return (pCoordinates.GetXCoordinate() >= this->GetMinX() &&
						pCoordinates.GetXCoordinate() <= this->GetMidX() &&
						pCoordinates.GetYCoordinate() >= this->GetMidY() &&
						pCoordinates.GetYCoordinate() <= this->GetMaxY());
			}
		case PlanetaryQuadrant::eBottomRight:	// Is coordinate in bottom-right quadrant?
			{
				return (pCoordinates.GetXCoordinate() >= this->GetMidX() &&
						pCoordinates.GetXCoordinate() <= this->GetMaxX() &&
						pCoordinates.GetYCoordinate() >= this->GetMidY() &&
						pCoordinates.GetYCoordinate() <= this->GetMaxY());
			}
	}
	// Is coordinate in node?
	return (pCoordinates.GetXCoordinate() >= this->GetMinX() &&
			pCoordinates.GetXCoordinate() <= this->GetMaxX() &&
			pCoordinates.GetYCoordinate() >= this->GetMinY() &&
			pCoordinates.GetYCoordinate() <= this->GetMaxY());
}

/************************************************************************************************/
int8			  FPlanetaryQuadTreeNode::GetNeighborIdAtEdge(PlanetaryEdge::PlanetaryEdge pEdge)
{
	if
		( pEdge == PlanetaryEdge::eNone )
	{
		return -1;
	}

	for
		( uint8 lCurrNeigbor = 0; lCurrNeigbor < 4; lCurrNeigbor++ )
	{
		if
			( this->mNodeNeighbors[ pEdge ] &&
			  this->mNodeNeighbors[ pEdge ]->mNodeNeighbors[ lCurrNeigbor ] == this )
		{
			return lCurrNeigbor;
		}
	}

	return -1;
}

/************************************************************************************************/
FPlanetaryQuadTreeNode* FPlanetaryQuadTreeNode::GetQuadrantNeighbor(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant, PlanetaryEdge::PlanetaryEdge pEdge, bool pForceSplit)
{
	// When neighbors cross from one face of the cube to another, a mapping is needed to find the correct quadrant
	static const uint8 sTopLeftTopEdge[6]        = { PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eTopRight };
	static const uint8 sTopLeftLeftEdge[6]       = { PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eTopRight };
	static const uint8 sTopRightTopEdge[6]       = { PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eTopLeft };
	static const uint8 sTopRightRightEdge[6]     = { PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eTopLeft };
	static const uint8 sBottomLeftBottomEdge[6]  = { PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eBottomRight };
	static const uint8 sBottomLeftLeftEdge[6]    = { PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eBottomRight };
	static const uint8 sBottomRightBottomEdge[6] = { PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eTopRight, PlanetaryQuadrant::eBottomLeft };
	static const uint8 sBottomRightRightEdge[6]  = { PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eTopLeft, PlanetaryQuadrant::eBottomRight, PlanetaryQuadrant::eBottomLeft, PlanetaryQuadrant::eBottomLeft };

	FPlanetaryQuadTreeNode* lNeighbor = NULL;
	uint8 lChild;
	switch
		(pQuadrant)
	{
	case PlanetaryQuadrant::eTopLeft:
	{
		switch
			(pEdge)
		{
		case PlanetaryEdge::eTopEdge:
		{
			bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
			if
				(lIsNeighborValid)
			{
				PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
				bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
				if
					(lIsThisFaceInSameNeighborFace)
				{
					lChild = PlanetaryQuadrant::eBottomLeft;
				}
				else
				{
					lChild = sTopLeftTopEdge[lThisFace];
				}

				if
					(pForceSplit)
				{
					this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
				}

				lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>(this->mNodeNeighbors[pEdge]->mChildren[lChild]);
			}
		}
		break;
		case PlanetaryEdge::eRightEdge:
		{
			lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>(this->mChildren[PlanetaryQuadrant::eTopRight]);
		}
		break;
		case PlanetaryEdge::eBottomEdge:
		{
			lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>(this->mChildren[PlanetaryQuadrant::eBottomLeft]);
		}
		break;
		case PlanetaryEdge::eLeftEdge:
		{
			bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
			if
				(lIsNeighborValid)
			{
				PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
				bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
				if
					(lIsThisFaceInSameNeighborFace)
				{
					lChild = PlanetaryQuadrant::eTopRight;
				}
				else
				{
					lChild = sTopLeftLeftEdge[lThisFace];
				}

				if
					(pForceSplit)
				{
					this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
				}

				lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>(this->mNodeNeighbors[pEdge]->mChildren[lChild]);
			}
		}
		break;
		}
	}
	break;
	case PlanetaryQuadrant::eTopRight:
	{
		switch
			(pEdge)
		{
		case PlanetaryEdge::eTopEdge:
		{
			bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
			if
				(lIsNeighborValid)
			{
				PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
				bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
				if
					(lIsThisFaceInSameNeighborFace)
				{
					lChild = PlanetaryQuadrant::eBottomRight;
				}
				else
				{
					lChild = sTopRightTopEdge[lThisFace];
				}

				if
					(pForceSplit)
				{
					this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
				}

				lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mNodeNeighbors[pEdge]->mChildren[lChild] );
			}
		}
		break;
		case PlanetaryEdge::eRightEdge:
		{
			bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
			if
				(lIsNeighborValid)
			{
				PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
				bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
				if
					(lIsThisFaceInSameNeighborFace)
				{
					lChild = PlanetaryQuadrant::eTopLeft;
				}
				else
				{
					lChild = sTopRightRightEdge[lThisFace];
				}

				if
					(pForceSplit)
				{
					this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
				}

				lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mNodeNeighbors[pEdge]->mChildren[lChild] );
			}
		}
		break;
		case PlanetaryEdge::eBottomEdge:
		{
			lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[PlanetaryQuadrant::eBottomRight] );
		}
		break;
		case PlanetaryEdge::eLeftEdge:
		{
			lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[PlanetaryQuadrant::eTopLeft] );
		}
		break;
		}
	}
	break;
		case PlanetaryQuadrant::eBottomLeft:
			switch
				(pEdge)
			{
				case PlanetaryEdge::eTopEdge:
				{
					lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[PlanetaryQuadrant::eTopLeft] );
				}
				break;
				case PlanetaryEdge::eRightEdge:
				{
					lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[PlanetaryQuadrant::eBottomRight] );
				}
				break;
				case PlanetaryEdge::eBottomEdge:
				{
					bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
					if
						(lIsNeighborValid)
					{
						PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
						bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
						if
							(lIsThisFaceInSameNeighborFace)
						{
							lChild = PlanetaryQuadrant::eTopLeft;
						}
						else
						{
							lChild = sBottomLeftBottomEdge[lThisFace];
						}

						if
							(pForceSplit)
						{
							this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
						}

						lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mNodeNeighbors[pEdge]->mChildren[lChild] );
					}
				}
				break;
				case PlanetaryEdge::eLeftEdge:
					{
						bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
						if
							(lIsNeighborValid)
						{
							PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
							bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
							if
								(lIsThisFaceInSameNeighborFace)
							{
								lChild = PlanetaryQuadrant::eBottomRight;
							}
							else
							{
								lChild = sBottomLeftLeftEdge[lThisFace];
							}

							if
								(pForceSplit)
							{
								this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
							}

							lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mNodeNeighbors[pEdge]->mChildren[lChild] );
						}
					}
					break;
				}
				break;
		case PlanetaryQuadrant::eBottomRight:
			{
				switch
					( pEdge )
				{
					case PlanetaryEdge::eTopEdge:
						{
							lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[PlanetaryQuadrant::eTopRight] );
						}
						break;
					case PlanetaryEdge::eRightEdge:
						{
							bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
							if
								(lIsNeighborValid)
							{
								PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
								bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
								if
									(lIsThisFaceInSameNeighborFace)
								{
									lChild = PlanetaryQuadrant::eBottomLeft;
								}
								else
								{
									lChild = sBottomRightRightEdge[lThisFace];
								}

								if
									(pForceSplit)
								{
									this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
								}

								lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mNodeNeighbors[pEdge]->mChildren[lChild] );
							}
						}
						break;
					case PlanetaryEdge::eBottomEdge:
						{
							bool lIsNeighborValid = this->mNodeNeighbors[pEdge] != NULL;
							if
								(lIsNeighborValid)
							{
								PlanetaryFace::PlanetaryFace lThisFace = this->GetFace();
								bool lIsThisFaceInSameNeighborFace = this->mNodeNeighbors[pEdge]->GetFace() == lThisFace;
								if
									(lIsThisFaceInSameNeighborFace)
								{
									lChild = PlanetaryQuadrant::eTopRight;
								}
								else
								{
									lChild = sBottomRightBottomEdge[lThisFace];
								}

								if
									(pForceSplit)
								{
									this->mNodeNeighbors[pEdge]->Split((PlanetaryQuadrant::PlanetaryQuadrant)lChild);
								}

								lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mNodeNeighbors[pEdge]->mChildren[lChild] );
							}
						}
						break;
					case PlanetaryEdge::eLeftEdge:
						{
							lNeighbor = StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[PlanetaryQuadrant::eBottomLeft] );
						}
						break;
					}
					break;
			}
	}

	return lNeighbor;
}

/************************************************************************************************/
FPlanetaryQuadTreeNode* FPlanetaryQuadTreeNode::GetNeighborAtEdge(PlanetaryEdge::PlanetaryEdge pEdge, bool pForceSplit)
{
	if
		( this->mParent == NULL )
	{
		return this->mNodeNeighbors[ pEdge ];
	}

	FPlanetaryQuadTreeNode* lParent = StaticCast<FPlanetaryQuadTreeNode*>( this->mParent );
	PlanetaryQuadrant::PlanetaryQuadrant lQuadrant = this->GetQuadrant();
	return lParent->GetQuadrantNeighbor( lQuadrant, pEdge, pForceSplit );
}

/************************************************************************************************/
PlanetaryTreeCoordinates			 FPlanetaryQuadTreeNode::GetNearestCoord(const PlanetaryTreeCoordinates& pCoordinates, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant)
{
	float lXCoordinate;
	float lYCoordinate;
	PlanetaryFace::PlanetaryFace lFace = this->GetFace();
	PlanetaryTreeCoordinates lTempCoordinates(lFace, pCoordinates.GetDirection());
	switch
		( pQuadrant )
	{
		case PlanetaryQuadrant::eTopLeft:		// Get nearest coordinate for top-left quadrant
			{
				lXCoordinate = FMath::Max(this->GetMinX(), FMath::Min(this->GetMidX(), lTempCoordinates.GetXCoordinate()));
				lYCoordinate = FMath::Max(this->GetMinY(), FMath::Min(this->GetMidY(), lTempCoordinates.GetYCoordinate()));
			}
			break;
		case PlanetaryQuadrant::eTopRight:		// Get nearest coordinate to top-right quadrant
			{
				lXCoordinate = FMath::Max(this->GetMidX(), FMath::Min(this->GetMaxX(), lTempCoordinates.GetXCoordinate()));
				lYCoordinate = FMath::Max(this->GetMinY(), FMath::Min(this->GetMidY(), lTempCoordinates.GetYCoordinate()));
			}
			break;
		case PlanetaryQuadrant::eBottomLeft:	// Get nearest coordinate to bottom-left quadrant
			{
				lXCoordinate = FMath::Max(this->GetMinX(), FMath::Min(this->GetMidX(), lTempCoordinates.GetXCoordinate()));
				lYCoordinate = FMath::Max(this->GetMidY(), FMath::Min(this->GetMaxY(), lTempCoordinates.GetYCoordinate()));
			}
			break;
		case PlanetaryQuadrant::eBottomRight:	// Get nearest coordinate to bottom-right quadrant
			{
				lXCoordinate = FMath::Max(this->GetMidX(), FMath::Min(this->GetMaxX(), lTempCoordinates.GetXCoordinate()));
				lYCoordinate = FMath::Max(this->GetMidY(), FMath::Min(this->GetMaxY(), lTempCoordinates.GetYCoordinate()));
			}
			break;
		default:			// Get nearest coordinate to entire node
			{
				lXCoordinate = FMath::Max(this->GetMinX(), FMath::Min(this->GetMaxX(), lTempCoordinates.GetXCoordinate()));
				lYCoordinate = FMath::Max(this->GetMinY(), FMath::Min(this->GetMaxY(), lTempCoordinates.GetYCoordinate()));
			}
			break;
	}

	return PlanetaryTreeCoordinates(lFace, lXCoordinate, lYCoordinate);
}
