#pragma once

//==========================================
// Includes
//==========================================
#include "GenericQuadTree.h"
#include "../PlanetaryFace.h"
#include "PlanetaryQuadTreeNode.h"
#include "../Renderer/PlanetaryNodeMeshComponent.h"

//==========================================
// Forward declarations
//==========================================
class UPlanetaryMap;

//==========================================
// Class definition
//==========================================
/**
 * Planetary quad tree  class
 */
class FPlanetaryQuadTree : public TQuadTree<FPlanetaryQuadTreeNode>
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The Planetary map owner component
	 */
	const UPlanetaryMap* mOwnerPlanetMap;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FPlanetaryQuadTree(const UPlanetaryMap* pOwnerPlanetMap, PlanetaryFace::PlanetaryFace pFace);
	~FPlanetaryQuadTree();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Update the tree nodes
	 */
	void UpdateTree();

	/**
	 * Set the tree neighbors
	 */
	void SetNeighbors(FPlanetaryQuadTree* pTop, FPlanetaryQuadTree* pRight, FPlanetaryQuadTree* pBottom, FPlanetaryQuadTree* pLeft);

	/**
	 * Releases the tree resources
	 */
	void ReleaseTree();

	/**
	 * Get the Planetary map owner component
	 */
	FORCEINLINE const UPlanetaryMap* OwnerMap() const;

};

//==========================================
// Inline
//==========================================
#include "PlanetaryQuadTree.inl"
