// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "GenericQuadTreeNode.h"
#include "../PlanetaryNodeFlags.h"
#include "../PlanetaryQuadrant.h"
#include "../PlanetaryFace.h"
#include "../PlanetaryEdge.h"
#include "../PlanetaryTreeCoordinates.h"
#include "../Helpers/PlanetaryHelpers.h"
#include "../Renderer/PlanetaryNodeMeshComponent.h"

//==========================================
// Forward declarations
//==========================================
class FPlanetaryQuadTree;

//==========================================
// Static Initializer of Planetary Node
//==========================================
class SPlanetaryQuadTreeNodeInitializer
{
public:

	//==========================================
	// Constructor
	//==========================================
	SPlanetaryQuadTreeNodeInitializer();

};

//==========================================
// Class definition
//==========================================
/**
 * Planetary map node used a quad tree node for the planet generation
 */
class FPlanetaryQuadTreeNode : public TQuadTreeNode<UPlanetaryNodeMeshComponent>
{
private:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * The quadrant offsets look up table
	 */
	static uint16 sQuadrantOffset[4];

	/**
	 * The quadrant height offsets look up table
	 */
	static uint16 sQuadrantHeightOffset[4];

	/**
	 * Stores the planetary node coordinates
	 */
	PlanetaryTreeCoordinates mCookedCoordinates[ BORDER_MAP_COUNT ];

	/**
	 * Stores the planetary node normals
	 */
	FVector				 mCookedNormals[ HEIGHT_MAP_COUNT ];

	/**
	 * The planetary cooked vertices
	 */
	FPlanetaryMeshVertex mCookedVertices[ SURFACE_MAP_COUNT ];

	/**
	 * The planetary node neighbors ( NULL if the neighbor is not split down to the current node's level)
	 */
	FPlanetaryQuadTreeNode* mNodeNeighbors[4];

	/**
	 * The planetary node corners ( that is XMin, YMin, XMax, YMax )
	 */
	float mNodeCorners[4];

	/**
	 * The planetary node quadrant Indices sorted by distance from the camera
	 */
	PlanetaryQuadrant::PlanetaryQuadrant mSortedQuadrants[4];

	/**
	 * The quad tree owner
	 */
	FPlanetaryQuadTree* mOwner;

	/**
	 * The planetary node flags tracking the node states
	 */
	PlanetaryNodeFlags::PlanetaryNodeFlags mNodeFlags;

	//==========================================
	// Private Methods
	//==========================================
	/**
	 * Update the node mesh triangles
	 *
	 * @return The planetary node triangle count
	 */
	int32			  UpdateTriangles();

	/**
	 * Gets the height from the heightmap for the specified coordinates
	 */
	float			  GetHeightFromMap(const PlanetaryTreeCoordinates& pCoordinates) const;

	/**
	 * Gets the height from the heightmap for the specified coordinates
	 */
	float			  GetHeightFromMap(float pXCoordinate, float pYCoordinate) const;

	/**
	 * Generate the planet node coordinates
	 * 
	 * @param pFace The face this node belongs to
	 */
	void			  BuildCoordinates(PlanetaryFace::PlanetaryFace pFace);

	/**
	 * Generate the node surface normals
	 */
	void			  ComputeNormals();

	/**
	 * Generate the bumpmap texture thanks to cooked normals
	 */
	void			  BuildBumpmap();

	/**
	 * Generate the planet node vertices (and fill the heightmap)
	 */
	void			  BuildVertices();

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FPlanetaryQuadTreeNode(FPlanetaryQuadTree* pOwner, PlanetaryFace::PlanetaryFace pFace);
	FPlanetaryQuadTreeNode(FPlanetaryQuadTreeNode* pParent, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant);
	~FPlanetaryQuadTreeNode();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Initialize the planetary node static tables
	 */
	static void InitializeQuadrantTables();

	/**
	 * Releases node's unmanaged resources
	 */
	virtual void	  Release() override;

	/**
	 * Initialize the planetary node
	 */
	void			  InitializeNode(PlanetaryFace::PlanetaryFace pFace, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant);

	/**
	 * Update the node mesh primitive
	 * 
	 * @return The planetary node triangle count
	 */
	int32			  UpdateNodeMesh();

	/**
	 * Refresh the planetary node neighbors
	 */
	void			  RefreshNeighbors();

	/**
	 * Update the tree node by splitting it to the specified quadrant
	 */
	void			  Split(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant);

	/**
	 * Test whether this node can be merged
	 */
	bool			  CanMerge();

	/**
	 * Merge this node in the specified quadrant
	 */
	bool			  Merge(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant);

	/**
	 * Test whether something happening in an area drawn by supplied coordinates 
	 * and a radius affects this node.
	 */
	bool			  AffectsNode(const PlanetaryTreeCoordinates& pCoordinates, float pRadius);

	/**
	 * Test whether supplied coordinates is in the node bounds or in any quadrant bounds if provided.
	 */
	bool			  HitTest(const PlanetaryTreeCoordinates& pCoordinates, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant = PlanetaryQuadrant::eNone) const;

	/**
	 * Get the corresponding neighbor supplying the edge index.
	 * Return the neighbor index if matches, -1 otherwise.
	 */
	int8					 GetNeighborIdAtEdge(PlanetaryEdge::PlanetaryEdge pEdge);

	/**
	 * Get the neighbor of a quadrant supplying the quadrant and the edge
	 */
	FPlanetaryQuadTreeNode*  GetQuadrantNeighbor(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant, PlanetaryEdge::PlanetaryEdge pEdge, bool pForceSplit = false);

	/**
	 * Get the neighbor of a quadrant supplying the quadrant and the edge
	 */
	FPlanetaryQuadTreeNode*  GetNeighborAtEdge(PlanetaryEdge::PlanetaryEdge pEdge, bool pForceSplit = false);

	/**
	 * Get the nearest coordinates regarding to another tree coordinates
	 */
	PlanetaryTreeCoordinates GetNearestCoord(const PlanetaryTreeCoordinates& pCoordinates, PlanetaryQuadrant::PlanetaryQuadrant pQuadrant = PlanetaryQuadrant::eNone);

	/**
	 * Get the element bump texture
	 */
	FORCEINLINE UTexture2D* EditBumpTexture()
	{
		return this->mElement->BumpTexture;
	}

	/**
	 * Get the element ground texture
	 */
	FORCEINLINE UTexture2D* EditGroundTexture()
	{
		return this->mElement->GroundTexture;
	}

	/**
	 * Get the cooked vertex at the specified index
	 */
	FORCEINLINE const FPlanetaryMeshVertex& GetCookedVertex(int32 pIndex) const;

	/**
	 * Get the face in which is this node
	 */
	FORCEINLINE PlanetaryFace::PlanetaryFace GetFace() const;

	/**
	 * Get the quadrant in which this node belongs to
	 */
	FORCEINLINE PlanetaryQuadrant::PlanetaryQuadrant GetQuadrant() const;

	/**
	 * Get the quad tree level in which is this node
	 */
	FORCEINLINE int32			  GetLevel() const;

	/**
	 * Get the direction at the surface position
	 */
	FORCEINLINE FVector			  GetDirection(float pX, float pY) const;

	/**
	 * Flag indicating whether this node is dirty
	 */
	FORCEINLINE bool			  IsDirty() const;

	/**
	 * Flag indicating whether the camera is inside the node bounds
	 */
	FORCEINLINE bool			  IsCameraIn(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const;

	/**
	 * Flag indicating whether the node is beyond the horizon
	 */
	FORCEINLINE bool			  IsBeyondHorizon(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const;

	/**
	 * Flag indicating whether the node is outside the camera frustum
	 */
	FORCEINLINE bool			  IsOutsideFrustum(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const;

	/**
	 * Get the minimal node bounding value on X
	 */
	FORCEINLINE float			  GetMinX() const;

	/**
	 * Get the minimal node bounding value on Y
	 */
	FORCEINLINE float			  GetMinY() const;

	/**
	 * Get the maximal node bounding value on X
	 */
	FORCEINLINE float			  GetMaxX() const;

	/**
	 * Get the maximal node bounding value on Y
	 */
	FORCEINLINE float			  GetMaxY() const;

	/**
	 * Get the middle node bounding value on X
	 */
	FORCEINLINE float			  GetMidX() const;

	/**
	 * Get the middle node bounding value on Y
	 */
	FORCEINLINE float			  GetMidY() const;

	/**
	 * Get the node bounding width
	 */
	FORCEINLINE float			  GetWidth() const;

	/**
	 * Get the node bounding height
	 */
	FORCEINLINE float			  GetHeight() const;

	/**
	 * Get the quadrant offset table index
	 */
	FORCEINLINE uint32			  GetIndex(int32 pX, int32 pY);

	/**
	 * Get the quadrant height offset table index
	 */
	FORCEINLINE uint32			  GetHeightIndex(int32 pX, int32 pY);

	/**
	 * Get the height at the specified coordinates
	 */
	FORCEINLINE float			  GetHeightAtCoordinates(const PlanetaryTreeCoordinates& pCoordinates) const;

	/**
	 * Get the owner quad tree owning that node.
	 */
	FORCEINLINE const FPlanetaryQuadTree* GetOwnerTree() const;

	/**
	 * Set the node's neighbors
	 */
	FORCEINLINE void			  SetNeighbors(FPlanetaryQuadTreeNode* pTop, FPlanetaryQuadTreeNode* pRight, FPlanetaryQuadTreeNode* pBottom, FPlanetaryQuadTreeNode* pLeft);

	/**
	 * Edit the cooked coordinate at the specified index of the node
	 */
	FORCEINLINE PlanetaryTreeCoordinates& EditCookedCoordinate(int32 pIndex)
	{
		return this->mCookedCoordinates[ pIndex ];
	}

	/**
	 * Edit the cooked normal at the specified index on the node
	 */
	FORCEINLINE FVector& EditNormal(int32 pIndex)
	{
		return this->mCookedNormals[ pIndex ];
	}

	/**
	 * Get the cooked coordinate at the specified index of the node
	 */
	FORCEINLINE PlanetaryTreeCoordinates GetCookedCoordinate(int32 pIndex) const
	{
		return this->mCookedCoordinates[ pIndex ];
	}

	/**
	 * Get the cooked normal at the specified index on the node
	 */
	FORCEINLINE FVector GetNormal(int32 pIndex) const
	{
		return this->mCookedNormals[ pIndex ];
	}
};

//==========================================
// Inline
//==========================================
#include "PlanetaryQuadTreeNode.inl"

