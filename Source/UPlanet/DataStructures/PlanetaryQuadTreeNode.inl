
/************************************************************************************************/
FORCEINLINE const FPlanetaryMeshVertex& FPlanetaryQuadTreeNode::GetCookedVertex(int32 pIndex) const
{
	return this->mCookedVertices[ pIndex ];
}

/************************************************************************************************/
FORCEINLINE PlanetaryFace::PlanetaryFace     FPlanetaryQuadTreeNode::GetFace() const
{
	return StaticCast<PlanetaryFace::PlanetaryFace>( this->mNodeFlags & PlanetaryNodeFlags::eFaceMask );
}

/************************************************************************************************/
FORCEINLINE PlanetaryQuadrant::PlanetaryQuadrant FPlanetaryQuadTreeNode::GetQuadrant() const
{
	return StaticCast<PlanetaryQuadrant::PlanetaryQuadrant>( (this->mNodeFlags & PlanetaryNodeFlags::eQuadrantMask) >> 3 );
}

/************************************************************************************************/
FORCEINLINE int32			  FPlanetaryQuadTreeNode::GetLevel() const
{
	return (this->mNodeFlags & PlanetaryNodeFlags::eLevelMask) >> 5;
}

/************************************************************************************************/
FORCEINLINE FVector			  FPlanetaryQuadTreeNode::GetDirection(float pX, float pY) const
{
	PlanetaryFace::PlanetaryFace lFace = this->GetFace();
	return PlanetaryHelper::GetPlanetaryPositionFromCoordinates( lFace, pX, pY );
}

/************************************************************************************************/
FORCEINLINE bool			  FPlanetaryQuadTreeNode::IsDirty() const
{
	return (this->mNodeFlags & PlanetaryNodeFlags::eNodeDirty) != 0;
}

/************************************************************************************************/
FORCEINLINE bool			  FPlanetaryQuadTreeNode::IsCameraIn(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const
{
	return (this->mNodeFlags & ( 1 << ( 10 + pQuadrant ) ) ) != 0;
}

/************************************************************************************************/
FORCEINLINE bool			  FPlanetaryQuadTreeNode::IsBeyondHorizon(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const
{
	return (this->mNodeFlags & ( 1 << ( 16 + pQuadrant ) ) ) != 0;
}

/************************************************************************************************/
FORCEINLINE bool			  FPlanetaryQuadTreeNode::IsOutsideFrustum(PlanetaryQuadrant::PlanetaryQuadrant pQuadrant) const
{
	return (this->mNodeFlags & ( 1 << ( 20 + pQuadrant ) ) ) != 0;
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetMinX() const
{
	return this->mNodeCorners[ 0 ];
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetMinY() const
{
	return this->mNodeCorners[ 1 ];
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetMaxX() const
{
	return this->mNodeCorners[ 2 ];
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetMaxY() const
{
	return this->mNodeCorners[ 3 ];
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetMidX() const
{
	return (this->GetMaxX() + this->GetMinX()) * 0.5f;
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetMidY() const
{
	return (this->GetMaxY() + this->GetMinY()) * 0.5f;
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetWidth() const
{
	return this->GetMaxX() - this->GetMinX();
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetHeight() const
{
	return this->GetMaxY() - this->GetMinY();
}

/************************************************************************************************/
FORCEINLINE uint32			  FPlanetaryQuadTreeNode::GetIndex(int32 pX, int32 pY)
{
	return pY * SURFACE_MAP_WIDTH + pX;
}

/************************************************************************************************/
FORCEINLINE uint32			  FPlanetaryQuadTreeNode::GetHeightIndex(int32 pX, int32 pY)
{
	return pY * HEIGHT_MAP_WIDTH + pX;
}

/************************************************************************************************/
FORCEINLINE float			  FPlanetaryQuadTreeNode::GetHeightAtCoordinates(const PlanetaryTreeCoordinates& pCoordinates) const
{
	PlanetaryQuadrant::PlanetaryQuadrant lQuadrant = PlanetaryQuadrant::eNone;
	float lMidX = this->GetMidX();
	float lMidY = this->GetMidY();
	if 
		( pCoordinates.GetXCoordinate() <= lMidX && 
		  pCoordinates.GetYCoordinate() <= lMidY )
	{
		lQuadrant = PlanetaryQuadrant::eTopLeft;
	}
	else if 
		( pCoordinates.GetXCoordinate() >= lMidX && 
		  pCoordinates.GetYCoordinate() <= lMidY )
	{
		lQuadrant = PlanetaryQuadrant::eTopRight;
	}
	else if 
		( pCoordinates.GetXCoordinate() <= lMidX && 
		  pCoordinates.GetYCoordinate() >= lMidY )
	{
		lQuadrant = PlanetaryQuadrant::eBottomLeft;
	}
	else if 
		( pCoordinates.GetXCoordinate() >= lMidX && 
		  pCoordinates.GetYCoordinate() >= lMidY )
	{
		lQuadrant = PlanetaryQuadrant::eBottomRight;
	}

	if 
		( this->mChildren[ lQuadrant ] )
	{
		return StaticCast<FPlanetaryQuadTreeNode*>( this->mChildren[ lQuadrant ] )->GetHeightAtCoordinates( pCoordinates );
	}

	return this->GetHeightFromMap( pCoordinates );
}

/************************************************************************************************/
FORCEINLINE const FPlanetaryQuadTree* FPlanetaryQuadTreeNode::GetOwnerTree() const
{
	return this->mOwner;
}

/************************************************************************************************/
FORCEINLINE void			  FPlanetaryQuadTreeNode::SetNeighbors(FPlanetaryQuadTreeNode* pTop, FPlanetaryQuadTreeNode* pRight, FPlanetaryQuadTreeNode* pBottom, FPlanetaryQuadTreeNode* pLeft)
{
	this->mNodeNeighbors[ PlanetaryEdge::eTopEdge ]    = pTop;
	this->mNodeNeighbors[ PlanetaryEdge::eRightEdge ]  = pRight;
	this->mNodeNeighbors[ PlanetaryEdge::eBottomEdge ] = pBottom;
	this->mNodeNeighbors[ PlanetaryEdge::eLeftEdge ]   = pLeft;
}
