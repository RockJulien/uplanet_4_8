
#include "../UPlanet.h"
#include "../PlanetaryMap.h"
#include "PlanetaryQuadTree.h"

/****************************************************************************************************************/
FPlanetaryQuadTree::FPlanetaryQuadTree(const UPlanetaryMap* pOwnerPlanetMap, PlanetaryFace::PlanetaryFace pFace) :
TQuadTree(), mOwnerPlanetMap( pOwnerPlanetMap )
{
	this->mRoot = new FPlanetaryQuadTreeNode( this, pFace );
}

/****************************************************************************************************************/
FPlanetaryQuadTree::~FPlanetaryQuadTree()
{
	this->ReleaseTree();
}

/****************************************************************************************************************/
void FPlanetaryQuadTree::UpdateTree()
{
	bool lIsValidRoot = this->mRoot != NULL;
	if 
		( lIsValidRoot )
	{
		PlanetaryFace::PlanetaryFace lFace = this->mRoot->GetFace();
		UE_LOG( LogClass, Log, TEXT( "FPlanetaryQuadTree::Update for face: %d" ), (int32)lFace );

		this->mRoot->UpdateNodeMesh();

		UE_LOG( LogClass, Log, TEXT( "End of FPlanetaryQuadTree::Update for face: %d" ), (int32)lFace );
	}
}

/****************************************************************************************************************/
void FPlanetaryQuadTree::SetNeighbors(FPlanetaryQuadTree* pTop, FPlanetaryQuadTree* pRight, FPlanetaryQuadTree* pBottom, FPlanetaryQuadTree* pLeft)
{
	bool lIsRootValid = this->mRoot != NULL;
	if 
		( lIsRootValid )
	{
		this->mRoot->SetNeighbors( pTop->mRoot, pRight->mRoot, pBottom->mRoot, pLeft->mRoot );
	}
}

/****************************************************************************************************************/
void FPlanetaryQuadTree::ReleaseTree()
{
	bool lIsRootValid = this->mRoot != NULL;
	if 
		( lIsRootValid )
	{
		delete this->mRoot;
		this->mRoot = NULL;
	}
}
