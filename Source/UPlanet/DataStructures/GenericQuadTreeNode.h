// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Platform.h"

//==========================================
// Class definition
//==========================================
/**
 * Generic quad tree node class
 */
template<typename ElementType>
class TQuadTreeNode
{
protected:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * The owned element
	 */
	ElementType*   mElement;

	/**
	 * The node children (up to 4)
	 */
	TQuadTreeNode* mChildren[4];

	/**
	 * The parent node
	 */
	TQuadTreeNode* mParent;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	TQuadTreeNode();
	TQuadTreeNode(TQuadTreeNode<ElementType>* pParent);
	virtual ~TQuadTreeNode();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Releases node's unmanaged resources
	 */
	virtual void Release();

	/**
	 * Flag indicating whether the node is a leaf node or not.
	 */
	bool IsLeaf() const;

	/**
	 * Get the parent node of that node
	 */
	const TQuadTreeNode* GetParent() const;

	/**
	 * Get a child node at the specified index
	 */
	const TQuadTreeNode* GetChild(uint8 pIndex) const;

	/**
	 * Get the owned object
	 */
	ElementType GetElement() const;

	/**
	 * Set the owned object
	 */
	void SetElement(ElementType pOwnedObject);

	/**
	 * Set the parent node of that node
	 */
	void SetParent(TQuadTreeNode* pParent);

	/**
	 * Set a child node at the specified index
	 */
	void SetChild(uint8 pIndex, TQuadTreeNode* pChild);
};

/**************************************************************************************************************/
template<typename ElementType>
TQuadTreeNode<ElementType>::TQuadTreeNode() :
mParent(NULL)
{
	this->mChildren[ 0 ] = NULL;
	this->mChildren[ 1 ] = NULL;
	this->mChildren[ 2 ] = NULL;
	this->mChildren[ 3 ] = NULL;
}

/**************************************************************************************************************/
template<typename ElementType>
TQuadTreeNode<ElementType>::TQuadTreeNode(TQuadTreeNode<ElementType>* pParent) :
mParent(pParent)
{
	this->mChildren[ 0 ] = NULL;
	this->mChildren[ 1 ] = NULL;
	this->mChildren[ 2 ] = NULL;
	this->mChildren[ 3 ] = NULL;
}

/**************************************************************************************************************/
template<typename ElementType>
TQuadTreeNode<ElementType>::~TQuadTreeNode()
{

}

/**************************************************************************************************************/
template<typename ElementType>
void TQuadTreeNode<ElementType>::Release()
{

}

/**************************************************************************************************************/
template<typename ElementType>
bool TQuadTreeNode<ElementType>::IsLeaf() const
{
	return ( this->mChildren[ 0 ] == NULL && this->mChildren[ 1 ] == NULL && this->mChildren[ 2 ] == NULL && this->mChildren[ 3 ] == NULL );
}

/**************************************************************************************************************/
template<typename ElementType>
const TQuadTreeNode<ElementType>* TQuadTreeNode<ElementType>::GetParent() const
{
	return this->mParent;
}

/**************************************************************************************************************/
template<typename ElementType>
const TQuadTreeNode<ElementType>* TQuadTreeNode<ElementType>::GetChild(uint8 pIndex) const
{
	if 
		( pIndex < 0 || 
		  pIndex >= 4 )
	{
		return NULL;
	}

	return this->mChildren[ pIndex ];
}

/**************************************************************************************************************/
template<typename ElementType>
ElementType TQuadTreeNode<ElementType>::GetElement() const
{
	return this->mElement;
}

/**************************************************************************************************************/
template<typename ElementType>
void TQuadTreeNode<ElementType>::SetElement(ElementType pOwnedObject)
{
	this->mElement = pOwnedObject;
}

/**************************************************************************************************************/
template<typename ElementType>
void TQuadTreeNode<ElementType>::SetParent(TQuadTreeNode* pParent)
{
	this->mParent = pParent;
}

/**************************************************************************************************************/
template<typename ElementType>
void TQuadTreeNode<ElementType>::SetChild(uint8 pIndex, TQuadTreeNode* pChild)
{
	if 
		( pIndex < 0 || 
		  pIndex >= 4 )
	{
		return;
	}

	this->mChildren[ pIndex ] = pChild;
}

