// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace PlanetaryNodeFlags
{

	/**
	 * Enumeration offering the planetary node flags (to use as masks)
	 */
	enum PlanetaryNodeFlags
	{
		/**
		 * These 3 bits indicate in which top-level face is the node
		 */
		eFaceMask = 0x0007,

		/**
		 * These 2 bits indicate which one of the parent's quadrants was split to create that node
		 */
		 eQuadrantMask = 0x0018,

		 /**
		  * These 5 bits indicate in which level of the tree is the node (O based up to 31)
		  */
		  eLevelMask = 0x03E0,

		  /**
		   * These 2 bits indicate in which quadrant is the camera (if it's in this node)
		   */
		   eCameraInMask = 0x0C00,

		   /**
			* This bit indicates whether the node is dirty (i.e. needs geo-morphing)
			*/
			eNodeDirty = 0x1000,

			/**
			 * These bits indicate whether each quadrant is beyond the horizon distance
			 */
			 eBeyondHorizonMask = 0x000F0000,

			 /**
			  * These bits indicate whether each quadrant is outside the view frustum
			  */
			  eOutsideFrustumMask = 0x00F00000
	};

}
