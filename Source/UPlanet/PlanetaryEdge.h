// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace PlanetaryEdge
{

	/**
	 * Enumeration offering the available edges linking with neighbors
	 */
	enum PlanetaryEdge
	{
		/**
		 * No edge
		 */
		eNone = -1,

		/**
		 * The top edge
		 */
		 eTopEdge = 0,

		 /**
		  * The right edge
		  */
		  eRightEdge,

		  /**
		   * The Bottom edge
		   */
		   eBottomEdge,

		   /**
			* The Left edge
			*/
			eLeftEdge
	};

}
