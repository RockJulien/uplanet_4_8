
#pragma once

//==========================================
// Includes
//==========================================
#include "Vector.h"

//==========================================
// Class definition
//==========================================
/**
 * Skydome Vertex struct definition
 */
struct FSkydomeVertex
{
	//==========================================
	// Properties
	//==========================================
	/**
	 * Skydome vertex position
	 */
	FVector		  Position;

	//==========================================
	// Constructor(s)
	//==========================================
	FSkydomeVertex(float pX = 0.0f, float pY = 0.0f, float pZ = 0.0f) :
	Position( pX, pY, pZ )
	{

	}
	FSkydomeVertex(FVector pPosition) :
	Position( pPosition )
	{

	}
};
