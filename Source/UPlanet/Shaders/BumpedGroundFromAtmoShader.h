
#pragma once

//==========================================
// Includes
//==========================================
#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"

//==========================================
// Vertex stage shader uniforms
//==========================================
/**
 * This buffer should contain variables that never, or rarely change
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromAtmoVertexConstantParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, InvWavelength)	  // 1 / pow(wavelength, 4) for the red, green, and blue channels
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, OuterRadius)		  // The outer (atmosphere) radius
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, OuterRadius2)		  // fOuterRadius^2
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, InnerRadius)		  // The inner (planetary) radius
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, InnerRadius2)		  // fInnerRadius^2
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, KrESun)				  // Kr * ESun
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, KmESun)				  // Km * ESun
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Kr4PI)				  // Kr * 4 * PI
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Km4PI)				  // Km * 4 * PI
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Scale)				  // 1 / (fOuterRadius - fInnerRadius)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, ScaleDepth)			  // The scale depth (i.e. the altitude at which the atmosphere's average density is found)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, ScaleOverScaleDepth)  // fScale / fScaleDepth
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(int32, iSamples)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, fSamples)
END_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromAtmoVertexConstantParameters)

/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromAtmoVertexVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FMatrix, WorldViewProj)      // The world view projection matrix
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, CameraPos)		  // The camera's current position
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, CameraHeight)		  // The camera's current height
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, CameraHeight2)		  // fCameraHeight^2
END_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromAtmoVertexVariableParameters)

//==========================================
// Pixel stage shader uniforms
//==========================================
/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromAtmoPixelVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
END_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromAtmoPixelVariableParameters)

//==========================================
// Typedefs & Constants
//==========================================
typedef TUniformBufferRef<FBumpedGroundFromAtmoVertexConstantParameters> FBumpedGroundFromAtmoVertexConstantParametersRef;
typedef TUniformBufferRef<FBumpedGroundFromAtmoVertexVariableParameters> FBumpedGroundFromAtmoVertexVariableParametersRef;
typedef TUniformBufferRef<FBumpedGroundFromAtmoPixelVariableParameters>  FBumpedGroundFromAtmoPixelVariableParametersRef;

//==========================================
// Class definition
//==========================================
/**
 * Bumped ground from atmosphere rendering vertex shader class
 */
class BumpedGroundFromAtmoVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(BumpedGroundFromAtmoVertexShader, Global);

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	BumpedGroundFromAtmoVertexShader();
	explicit BumpedGroundFromAtmoVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set constant uniforms to the shader (that never change)
	 */
	void SetConstantUniformParameters(FRHICommandList& pRHICmdList, 
									  const FBumpedGroundFromAtmoVertexConstantParameters& pVertexParameters);

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList, 
									  const FBumpedGroundFromAtmoVertexVariableParameters& pVertexParameters);
};

/**
 * Bumped ground from atmosphere rendering pixel shader class
 */
class BumpedGroundFromAtmoPixelShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(BumpedGroundFromAtmoPixelShader, Global);

private:

	/**
	 * The Bumpmap texture resource used in the shader pixel stage
	 */
	FShaderResourceParameter mBumpTextureParameter;

	/**
	 * The Ground texture resource used in the shader pixel stage
	 */
	FShaderResourceParameter mGroundTextureParameter;

	/**
	 * The Texture sampler used in the shader pixel stage
	 */
	FShaderResourceParameter mTextureParameterSampler;

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	BumpedGroundFromAtmoPixelShader();
	explicit BumpedGroundFromAtmoPixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FBumpedGroundFromAtmoPixelVariableParameters& pPixelParameters,
									  const UTexture2D* pBumpTexture,
									  const UTexture2D* pGroundTexture);
};
