
#include "UPlanet.h"
#include "SkyFromSpaceShader.h"
#include "ShaderParameterUtils.h"
#include "RHIStaticStates.h"

/**
 * These are needed to actually implement the constant buffers so 
 * they are available inside our shader.
 * They also need to be unique over the entire solution since 
 * they can in fact be accessed from any shader.
 */
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FSkyFromSpaceVertexConstantParameters, TEXT( "SkyFromSpaceVConstants" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FSkyFromSpaceVertexVariableParameters, TEXT( "SkyFromSpaceVVariables" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FSkyFromSpacePixelVariableParameters,  TEXT( "SkyFromSpacePVariables" ) )

/*****************************************************************************************************/
SkyFromSpaceVertexShader::SkyFromSpaceVertexShader()
{

}

/*****************************************************************************************************/
SkyFromSpaceVertexShader::SkyFromSpaceVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) : 
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool SkyFromSpaceVertexShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool SkyFromSpaceVertexShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void SkyFromSpaceVertexShader::SetConstantUniformParameters(FRHICommandList& pRHICmdList,
															const FSkyFromSpaceVertexConstantParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FSkyFromSpaceVertexConstantParametersRef lConstantParametersBuffer;
	lConstantParametersBuffer = FSkyFromSpaceVertexConstantParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the constant parameters container
	const TShaderUniformBufferParameter<FSkyFromSpaceVertexConstantParameters>& lConstantParameters = this->GetUniformBufferParameter<FSkyFromSpaceVertexConstantParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lConstantParameters, lConstantParametersBuffer );
}

/*****************************************************************************************************/
void SkyFromSpaceVertexShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
															const FSkyFromSpaceVertexVariableParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FSkyFromSpaceVertexVariableParametersRef lVertexVariableParametersBuffer;
	lVertexVariableParametersBuffer = FSkyFromSpaceVertexVariableParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FSkyFromSpaceVertexVariableParameters>& lVertexVariableParameters = this->GetUniformBufferParameter<FSkyFromSpaceVertexVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lVertexVariableParameters, lVertexVariableParametersBuffer );
}

/*****************************************************************************************************/
SkyFromSpacePixelShader::SkyFromSpacePixelShader()
{

}

/*****************************************************************************************************/
SkyFromSpacePixelShader::SkyFromSpacePixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool SkyFromSpacePixelShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool SkyFromSpacePixelShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void SkyFromSpacePixelShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
														   const FSkyFromSpacePixelVariableParameters& pPixelParameters)
{
	const FPixelShaderRHIParamRef lPixelShader = this->GetPixelShader();

	// Create the uniform buffer references
	FSkyFromSpacePixelVariableParametersRef lPixelVariableParametersBuffer;
	lPixelVariableParametersBuffer = FSkyFromSpacePixelVariableParametersRef::CreateUniformBufferImmediate( pPixelParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FSkyFromSpacePixelVariableParameters>& lPixelVariableParameters = this->GetUniformBufferParameter<FSkyFromSpacePixelVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lPixelShader, lPixelVariableParameters, lPixelVariableParametersBuffer );
}

/**
 * This is what will instantiate the shader into the engine FROM the Engine/Shaders folder of UE4
 * not yours for info ;-), so make sure your .usf is placed in UE4/Engine/Shaders
 */
//                       ShaderClass					 ShaderFileName (.usf)         Shader function name          Stage
IMPLEMENT_SHADER_TYPE( , SkyFromSpaceVertexShader, TEXT( "SkyFromSpaceShader" ), TEXT( "SkyFromSpaceVertexStage" ), SF_Vertex );
IMPLEMENT_SHADER_TYPE( , SkyFromSpacePixelShader,  TEXT( "SkyFromSpaceShader" ), TEXT( "SkyFromSpacePixelStage" ),  SF_Pixel );
