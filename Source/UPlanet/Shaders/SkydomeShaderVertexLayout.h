
#pragma once

//==========================================
// Includes
//==========================================
#include "RenderResource.h"

//==========================================
// Class definition
//==========================================
/**
* Planetary shader vertex layout definition
*/
class UPLANET_API FSkydomeShaderVertexLayout : public FRenderResource
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * Skydome vertex layout definition reference
	 */
	FVertexDeclarationRHIRef mVertexDeclarationRHI;

public:

	//==========================================
	// Methods
	//==========================================

	/**
	 * Initialize the Skydome vertex layout definition reference
	 */
	virtual void InitRHI() override;

	/**
	 * Release the Skydome vertex layout definition reference
	 */
	virtual void ReleaseRHI() override;

	/**
	 * Returns the Skydome vertex layout definition reference
	 */
	FORCEINLINE FVertexDeclarationRHIRef VertexDeclarationRHI() const;
};

//==========================================
// Inlines
//==========================================
#include "SkydomeShaderVertexLayout.inl"
