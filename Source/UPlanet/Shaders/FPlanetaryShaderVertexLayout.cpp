
#include "UPlanet.h"
#include "FPlanetaryMeshVertex.h"
#include "FPlanetaryShaderVertexLayout.h"

/*******************************************************************************************/
void FPlanetaryShaderVertexLayout::InitRHI()
{
	FVertexDeclarationElementList lDefinition;
	uint32 lStride = sizeof( FPlanetaryMeshVertex );
	lDefinition.Add( FVertexElement( 0, STRUCT_OFFSET( FPlanetaryMeshVertex, Position ),  VET_Float3, 0, lStride ) );
	lDefinition.Add( FVertexElement( 0, STRUCT_OFFSET( FPlanetaryMeshVertex, Normal ),    VET_Float3, 1, lStride ) );
	lDefinition.Add( FVertexElement( 0, STRUCT_OFFSET( FPlanetaryMeshVertex, TexCoords ), VET_Float2, 2, lStride ) );
	this->mVertexDeclarationRHI = RHICreateVertexDeclaration( lDefinition );
}

/*******************************************************************************************/
void FPlanetaryShaderVertexLayout::ReleaseRHI()
{
	this->mVertexDeclarationRHI.SafeRelease();
}
