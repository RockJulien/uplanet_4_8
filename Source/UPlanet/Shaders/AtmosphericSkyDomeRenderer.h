
#pragma once

//==========================================
// Includes
//==========================================
#include "SkyFromAtmoShader.h"
#include "SkyFromSpaceShader.h"

//==========================================
// Forward declarations
//==========================================
class FPlanetaryMapSceneProxy;

//==========================================
// Class definition
//==========================================
class UPLANET_API FAtmosphericSkyDomeRenderer
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The shader feature level (e.g: Shader model 4, 5, ...)
	 */
	ERHIFeatureLevel::Type mFeatureLevel;

public:

	//==========================================
	// Constructor & Destructor
	//==========================================
	FAtmosphericSkyDomeRenderer(ERHIFeatureLevel::Type pFeatureLevel);
	~FAtmosphericSkyDomeRenderer();

	//==========================================
	// Properties
	//==========================================

	// #region FromAtmo

	/**
	 * The vertex stage constants to use to render the planet skydome from atmosphere
	 */
	static FSkyFromAtmoVertexConstantParameters SkyFromAtmoVertexConstants;

	/**
	 * The vertex stage variables to use to render the planet skydome from atmosphere
	 */
	static FSkyFromAtmoVertexVariableParameters SkyFromAtmoVertexVariables;

	/**
	 * The pixel stage variables to use to render the planet skydome from atmosphere
	 */
	static FSkyFromAtmoPixelVariableParameters SkyFromAtmoPixelVariables;

	// #endregion FromAtmo

	// #region FromSpace

	/**
	 * The vertex stage constants to use to render the planet skydome from space
	 */
	static FSkyFromSpaceVertexConstantParameters SkyFromSpaceVertexConstants;

	/**
	 * The vertex stage variables to use to render the planet skydome from space
	 */
	static FSkyFromSpaceVertexVariableParameters SkyFromSpaceVertexVariables;

	/**
	 * The pixel stage variables to use to render the planet skydome from space
	 */
	static FSkyFromSpacePixelVariableParameters SkyFromSpacePixelVariables;

	// #endregion FromSpace

	//==========================================
	// Methods
	//==========================================

	/**
	 * Execute the shader to render the planet sky dome representing the atomsphere
	 * NOTE : Only execute this from the render thread!!!
	 * 
	 * @param pProxy The planetary map scene proxy
	 */
	void ExecuteShader(const FPlanetaryMapSceneProxy* pProxy);

	/**
	 * Execute the shader to render the planet sky dome representing the atomsphere
	 * NOTE : Only execute this from the render thread!!!
	 *
	 * @param pProxy The planetary map scene proxy
	 * @param pWorldViewMatrix The override world view proj matrix to use instead of the one of the proxy (Can be NULL)
	 */
	void ExecuteShader(const FPlanetaryMapSceneProxy* pProxy, const FMatrix* pWorldViewMatrix);
};
