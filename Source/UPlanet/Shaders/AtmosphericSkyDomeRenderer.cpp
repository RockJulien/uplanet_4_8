
//==========================================
// Includes
//==========================================
#include "UPlanet.h"
#include "RHIStaticStates.h"
#include "AtmosphericSkyDomeRenderer.h"
#include "SkydomeShaderVertexLayout.h"
#include "../Renderer/PlanetaryMapSceneProxy.h"

//==========================================
// Static initialization
//==========================================
FSkyFromAtmoVertexConstantParameters  FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants  = FSkyFromAtmoVertexConstantParameters();
FSkyFromAtmoVertexVariableParameters  FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables  = FSkyFromAtmoVertexVariableParameters();
FSkyFromAtmoPixelVariableParameters   FAtmosphericSkyDomeRenderer::SkyFromAtmoPixelVariables   = FSkyFromAtmoPixelVariableParameters();
FSkyFromSpaceVertexConstantParameters FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants = FSkyFromSpaceVertexConstantParameters();
FSkyFromSpaceVertexVariableParameters FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables = FSkyFromSpaceVertexVariableParameters();
FSkyFromSpacePixelVariableParameters  FAtmosphericSkyDomeRenderer::SkyFromSpacePixelVariables  = FSkyFromSpacePixelVariableParameters();

//It seems to be the convention to expose all vertex declarations as globals, and then reference them as externs in the headers where they are needed.
//It kind of makes sense since they do not contain any parameters that change and are purely used as their names suggest, as declarations :)
TGlobalResource<FSkydomeShaderVertexLayout> GSkydomeVertexLayout;

/************************************************************************************************/
FAtmosphericSkyDomeRenderer::FAtmosphericSkyDomeRenderer(ERHIFeatureLevel::Type pFeatureLevel) :
mFeatureLevel( pFeatureLevel )
{

}

/************************************************************************************************/
FAtmosphericSkyDomeRenderer::~FAtmosphericSkyDomeRenderer()
{

}

/************************************************************************************************/
void FAtmosphericSkyDomeRenderer::ExecuteShader(const FPlanetaryMapSceneProxy* pProxy)
{
	check( IsInRenderingThread() );

	FRHICommandListImmediate& lRHICmdList = GRHICommandList.GetImmediateCommandList();

	static FGlobalBoundShaderState lBoundShaderState;
	float lRayleighScaleDepth = 0.25f;
	float l4PI = 4.0f * PI;

	// Skydome from atmosphere effect
	if
		( pProxy->IsInAtmosphere )
	{
		TShaderMapRef<SkyFromAtmoVertexShader> lVertexStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		TShaderMapRef<SkyFromAtmoPixelShader>  lPixelStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );

		// Set the shader to use (make current in OpenGL) passing vertex layout and stages to use for graphic pipeline
		SetGlobalBoundShaderState( lRHICmdList, this->mFeatureLevel, lBoundShaderState, GSkydomeVertexLayout.VertexDeclarationRHI(), *lVertexStageRef, *lPixelStageRef );

		// Vertex constants
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.InvWavelength = FVector(1 / powf(pProxy->Wavelength[0], 4.0f), 1 / powf(pProxy->Wavelength[1], 4.0f), 1 / powf(pProxy->Wavelength[2], 4.0f));
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.InnerRadius   = pProxy->PlanetaryRadius;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.InnerRadius2  = FMath::Square(pProxy->PlanetaryRadius);
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.OuterRadius   = pProxy->AtmosphericRadius;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.OuterRadius2  = FMath::Square(pProxy->AtmosphericRadius);
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.KrESun = pProxy->Kr * pProxy->Esun;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.KmESun = pProxy->Km * pProxy->Esun;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.Kr4PI  = pProxy->Kr * l4PI;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.Km4PI  = pProxy->Km * l4PI;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.Scale  = 1.0f / (pProxy->AtmosphericRadius - pProxy->PlanetaryRadius);
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.ScaleDepth = lRayleighScaleDepth;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.ScaleOverScaleDepth = (1.0f / (pProxy->AtmosphericRadius - pProxy->PlanetaryRadius)) / lRayleighScaleDepth;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.iSamples = 2;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants.fSamples = 2.0;

		lVertexStageRef->SetConstantUniformParameters( lRHICmdList, FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexConstants );

		// Vertex variables
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables.WorldViewProj = pProxy->WorldViewProj;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables.CameraPos     = pProxy->CameraLocation;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables.LightPos      = pProxy->SunLightLocation;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables.CameraHeight  = pProxy->CameraLocation.Size();
		FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables.CameraHeight2 = pProxy->CameraLocation.SizeSquared();

		lVertexStageRef->SetVariableUniformParameters( lRHICmdList, FAtmosphericSkyDomeRenderer::SkyFromAtmoVertexVariables );

		// Pixel variables
		FAtmosphericSkyDomeRenderer::SkyFromAtmoPixelVariables.LightPos = pProxy->SunLightLocation;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoPixelVariables.G		= pProxy->G;
		FAtmosphericSkyDomeRenderer::SkyFromAtmoPixelVariables.G2		= FMath::Square( pProxy->G );

		lPixelStageRef->SetVariableUniformParameters( lRHICmdList, FAtmosphericSkyDomeRenderer::SkyFromAtmoPixelVariables );

	}
	// Skydome from space effect
	else
	{
		TShaderMapRef<SkyFromSpaceVertexShader> lVertexStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		TShaderMapRef<SkyFromSpacePixelShader>  lPixelStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );

		// Set the shader to use (make current in OpenGL) passing vertex layout and stages to use for graphic pipeline
		SetGlobalBoundShaderState( lRHICmdList, this->mFeatureLevel, lBoundShaderState, GSkydomeVertexLayout.VertexDeclarationRHI(), *lVertexStageRef, *lPixelStageRef );

		// Vertex constants
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.InvWavelength = FVector(1 / powf(pProxy->Wavelength[0], 4.0f), 1 / powf(pProxy->Wavelength[1], 4.0f), 1 / powf(pProxy->Wavelength[2], 4.0f));
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.InnerRadius   = pProxy->PlanetaryRadius;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.InnerRadius2  = FMath::Square( pProxy->PlanetaryRadius );
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.OuterRadius   = pProxy->AtmosphericRadius;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.OuterRadius2  = FMath::Square( pProxy->AtmosphericRadius );
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.KrESun = pProxy->Kr * pProxy->Esun;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.KmESun = pProxy->Km * pProxy->Esun;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.Kr4PI  = pProxy->Kr * l4PI;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.Km4PI  = pProxy->Km * l4PI;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.Scale  = 1.0f / (pProxy->AtmosphericRadius - pProxy->PlanetaryRadius);
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.ScaleDepth = lRayleighScaleDepth;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.ScaleOverScaleDepth = (1.0f / (pProxy->AtmosphericRadius - pProxy->PlanetaryRadius)) / lRayleighScaleDepth;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.iSamples = 2;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants.fSamples = 2.0;

		lVertexStageRef->SetConstantUniformParameters( lRHICmdList, FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexConstants );

		// Vertex variables
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables.WorldViewProj = pProxy->WorldViewProj;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables.CameraPos     = pProxy->CameraLocation;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables.LightPos	   = pProxy->SunLightLocation;
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables.CameraHeight  = pProxy->CameraLocation.Size();
		FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables.CameraHeight2 = pProxy->CameraLocation.SizeSquared();

		lVertexStageRef->SetVariableUniformParameters( lRHICmdList, FAtmosphericSkyDomeRenderer::SkyFromSpaceVertexVariables );

		// Pixel variables
		FAtmosphericSkyDomeRenderer::SkyFromSpacePixelVariables.LightPos = pProxy->SunLightLocation;
		FAtmosphericSkyDomeRenderer::SkyFromSpacePixelVariables.G		 = pProxy->G;
		FAtmosphericSkyDomeRenderer::SkyFromSpacePixelVariables.G2		 = FMath::Square( pProxy->G );

		lPixelStageRef->SetVariableUniformParameters( lRHICmdList, FAtmosphericSkyDomeRenderer::SkyFromSpacePixelVariables );
	}

	// Set the proper rendering states first
	lRHICmdList.SetBlendState( TStaticBlendState< CW_RGB, BO_Add, BF_One, BF_One >::GetRHI() );

	// Render the primitive
	uint32 lVertexStride = sizeof(FSkydomeVertex);
	uint32 lIndexStride  = sizeof(int32);
	DrawIndexedPrimitiveUP( lRHICmdList, 
							PT_TriangleList, 
							0, 
							pProxy->Vertices().Num(), 
							pProxy->PrimitiveCount(), 
							pProxy->Indices().GetData(), 
							lIndexStride, 
							pProxy->Vertices().GetData(), 
							lVertexStride );
}

/************************************************************************************************/
void FAtmosphericSkyDomeRenderer::ExecuteShader(const FPlanetaryMapSceneProxy* pProxy, const FMatrix* pWorldViewMatrix)
{
	// If override the world view projection, set it into the proxy instance
	// to replace the old one (that can be previously NULL actually depending 
	// on the previous process
	if 
		( pWorldViewMatrix != NULL )
	{
		FPlanetaryMapSceneProxy* lProxy = const_cast<FPlanetaryMapSceneProxy*>( pProxy );
		lProxy->WorldViewProj = *pWorldViewMatrix;
	}

	// Execute the shader afterward
	this->ExecuteShader( pProxy );
}
