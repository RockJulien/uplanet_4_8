
#pragma once

//==========================================
// Includes
//==========================================
#include "BumpedPlanetShader.h"
#include "BumpedGroundFromAtmoShader.h"
#include "BumpedGroundFromSpaceShader.h"

//==========================================
// Forward declarations
//==========================================
class FPlanetaryNodeMeshSceneProxy;

//==========================================
// Class definition
//==========================================
class UPLANET_API FPlanetaryNodeRenderer
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The shader feature level (e.g: Shader model 4, 5, ...)
	 */
	ERHIFeatureLevel::Type mFeatureLevel;

public:

	//==========================================
	// Constructor & Destructor
	//==========================================
	FPlanetaryNodeRenderer(ERHIFeatureLevel::Type pFeatureLevel);
	~FPlanetaryNodeRenderer();

	//==========================================
	// Properties
	//==========================================

	// #region NoAtmosphere

	/**
	 * The vertex stage variables to use to render the planet node without atmosphere
	 */
	static FBumpedPlanetVertexVariableParameters NoAtmosphereVertexVariables;

	/**
	 * The pixel stage variables to use to render the planet node without atmosphere
	 */
	static FBumpedPlanetPixelVariableParameters NoAtmospherePixelVariables;

	// #endregion NoAtmosphere

	// #region FromAtmo

	/**
	 * The vertex stage constants to use to render the planet node from atmosphere
	 */
	static FBumpedGroundFromAtmoVertexConstantParameters GroundFromAtmoVertexConstants;

	/**
	 * The vertex stage variables to use to render the planet node from atmosphere
	 */
	static FBumpedGroundFromAtmoVertexVariableParameters GroundFromAtmoVertexVariables;

	/**
	 * The pixel stage variables to use to render the planet node from atmosphere
	 */
	static FBumpedGroundFromAtmoPixelVariableParameters GroundFromAtmoPixelVariables;

	// #endregion FromAtmo

	// #region FromSpace

	/**
	 * The vertex stage constants to use to render the planet node from space
	 */
	static FBumpedGroundFromSpaceVertexConstantParameters GroundFromSpaceVertexConstants;

	/**
	 * The vertex stage variables to use to render the planet node from space
	 */
	static FBumpedGroundFromSpaceVertexVariableParameters GroundFromSpaceVertexVariables;

	/**
	 * The pixel stage variables to use to render the planet node from space
	 */
	static FBumpedGroundFromSpacePixelVariableParameters GroundFromSpacePixelVariables;

	// #endregion FromSpace

	//==========================================
	// Methods
	//==========================================

	/**
	 * Execute the shader to render the planet node.
	 * NOTE : Only execute this from the render thread!!!
	 *
	 * @param pProxy The planetary node scene proxy
	 */
	void ExecuteShader(const FPlanetaryNodeMeshSceneProxy* pProxy);

	/**
	 * Execute the shader to render the planet node.
	 * NOTE : Only execute this from the render thread!!!
	 *
	 * @param pProxy The planetary node scene proxy
	 * @param pWorldViewMatrix The override world view proj matrix to use instead of the one of the proxy (Can be NULL)
	 */
	void ExecuteShader(const FPlanetaryNodeMeshSceneProxy* pProxy, const FMatrix* pWorldViewMatrix);
};
