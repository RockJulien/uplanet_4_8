
#include "UPlanet.h"
#include "BumpedGroundFromAtmoShader.h"
#include "ShaderParameterUtils.h"
#include "RHIStaticStates.h"

/**
 * These are needed to actually implement the constant buffers so
 * they are available inside our shader.
 * They also need to be unique over the entire solution since
 * they can in fact be accessed from any shader.
 */
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedGroundFromAtmoVertexConstantParameters, TEXT( "BumpedGroundFromAtmoVConstants" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedGroundFromAtmoVertexVariableParameters, TEXT( "BumpedGroundFromAtmoVVariables" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedGroundFromAtmoPixelVariableParameters,  TEXT( "BumpedGroundFromAtmoPVariables" ) )

/*****************************************************************************************************/
BumpedGroundFromAtmoVertexShader::BumpedGroundFromAtmoVertexShader()
{

}

/*****************************************************************************************************/
BumpedGroundFromAtmoVertexShader::BumpedGroundFromAtmoVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool BumpedGroundFromAtmoVertexShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool BumpedGroundFromAtmoVertexShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void BumpedGroundFromAtmoVertexShader::SetConstantUniformParameters(FRHICommandList& pRHICmdList,
																	const FBumpedGroundFromAtmoVertexConstantParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FBumpedGroundFromAtmoVertexConstantParametersRef lConstantParametersBuffer;
	lConstantParametersBuffer = FBumpedGroundFromAtmoVertexConstantParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the constant parameters container
	const TShaderUniformBufferParameter<FBumpedGroundFromAtmoVertexConstantParameters>& lConstantParameters = this->GetUniformBufferParameter<FBumpedGroundFromAtmoVertexConstantParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lConstantParameters, lConstantParametersBuffer );
}

/*****************************************************************************************************/
void BumpedGroundFromAtmoVertexShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
																	const FBumpedGroundFromAtmoVertexVariableParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FBumpedGroundFromAtmoVertexVariableParametersRef lVertexVariableParametersBuffer;
	lVertexVariableParametersBuffer = FBumpedGroundFromAtmoVertexVariableParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FBumpedGroundFromAtmoVertexVariableParameters>& lVertexVariableParameters = this->GetUniformBufferParameter<FBumpedGroundFromAtmoVertexVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lVertexVariableParameters, lVertexVariableParametersBuffer );
}

/*****************************************************************************************************/
BumpedGroundFromAtmoPixelShader::BumpedGroundFromAtmoPixelShader()
{

}

/*****************************************************************************************************/
BumpedGroundFromAtmoPixelShader::BumpedGroundFromAtmoPixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{
	this->mBumpTextureParameter.Bind( pInitializer.ParameterMap, TEXT( "BumpTexture" ) );
	this->mGroundTextureParameter.Bind( pInitializer.ParameterMap, TEXT( "GroundTexture" ) );
	this->mTextureParameterSampler.Bind( pInitializer.ParameterMap, TEXT( "TextureSampler" ) );
}

/*****************************************************************************************************/
bool BumpedGroundFromAtmoPixelShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool BumpedGroundFromAtmoPixelShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there
	pArchive << this->mBumpTextureParameter;
	pArchive << this->mGroundTextureParameter;
	pArchive << this->mTextureParameterSampler;

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void BumpedGroundFromAtmoPixelShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
																   const FBumpedGroundFromAtmoPixelVariableParameters& pPixelParameters,
																   const UTexture2D* pBumpTexture,
																   const UTexture2D* pGroundTexture)
{
	const FPixelShaderRHIParamRef lPixelShader = this->GetPixelShader();

	// Create the uniform buffer references
	FBumpedGroundFromAtmoPixelVariableParametersRef lPixelVariableParametersBuffer;
	lPixelVariableParametersBuffer = FBumpedGroundFromAtmoPixelVariableParametersRef::CreateUniformBufferImmediate( pPixelParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FBumpedGroundFromAtmoPixelVariableParameters>& lPixelVariableParameters = this->GetUniformBufferParameter<FBumpedGroundFromAtmoPixelVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lPixelShader, lPixelVariableParameters, lPixelVariableParametersBuffer );
	
	FTexture2DResource* lBump2DResource   = StaticCast<FTexture2DResource*>( pBumpTexture->Resource );
	FTexture2DResource* lGround2DResource = StaticCast<FTexture2DResource*>( pGroundTexture->Resource );

	// Set the bumpmap texture to the pixel shader as well.
	SetTextureParameter( pRHICmdList,
						 lPixelShader,
						 this->mBumpTextureParameter,
						 lBump2DResource->GetTexture2DRHI() );

	// Set the ground texture to the pixel shader as well.
	SetTextureParameter( pRHICmdList,
						 lPixelShader,
						 this->mGroundTextureParameter,
						 lGround2DResource->GetTexture2DRHI() );

	FSamplerStateRHIParamRef lTextureSampler = TStaticSamplerState< SF_Point, AM_Clamp, AM_Clamp, AM_Clamp >::GetRHI();

	SetSamplerParameter( pRHICmdList,
						 lPixelShader,
						 this->mTextureParameterSampler,
						 lTextureSampler );
}

/**
 * This is what will instantiate the shader into the engine FROM the Engine/Shaders folder of UE4
 * not yours for info ;-), so make sure your .usf is placed in UE4/Engine/Shaders
 */
//                       ShaderClass                       ShaderFileName (.usf)                 Shader function name                       Stage
IMPLEMENT_SHADER_TYPE( , BumpedGroundFromAtmoVertexShader, TEXT( "BumpedGroundFromAtmoShader" ), TEXT( "BumpedGroundFromAtmoVertexStage" ), SF_Vertex );
IMPLEMENT_SHADER_TYPE( , BumpedGroundFromAtmoPixelShader,  TEXT( "BumpedGroundFromAtmoShader" ), TEXT( "BumpedGroundFromAtmoPixelStage" ),  SF_Pixel );
