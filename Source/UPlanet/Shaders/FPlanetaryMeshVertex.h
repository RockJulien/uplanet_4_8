
#pragma once

//==========================================
// Includes
//==========================================
#include "Vector.h"

//==========================================
// Class definition
//==========================================
/**
 * Planetary Vertex struct definition
 */
struct UPLANET_API FPlanetaryMeshVertex
{
	//==========================================
	// Properties
	//==========================================
	/**
	 * Planetary vertex position
	 */
	FVector		  Position;

	/**
	 * Planetary vertex normal
	 */
	FVector		  Normal;

	/**
	 * Planetary vertex texture coordinates
	 */
	FVector2D	  TexCoords;
};
