
#include "UPlanet.h"
#include "BumpedPlanetShader.h"
#include "ShaderParameterUtils.h"
#include "RHIStaticStates.h"

/**
 * These are needed to actually implement the constant buffers so
 * they are available inside our shader.
 * They also need to be unique over the entire solution since
 * they can in fact be accessed from any shader.
 */
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedPlanetVertexConstantParameters, TEXT( "BumpedPlanetVConstants" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedPlanetVertexVariableParameters, TEXT( "BumpedPlanetVVariables" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedPlanetPixelVariableParameters,  TEXT( "BumpedPlanetPVariables" ) )

/*****************************************************************************************************/
BumpedPlanetVertexShader::BumpedPlanetVertexShader()
{

}

/*****************************************************************************************************/
BumpedPlanetVertexShader::BumpedPlanetVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool BumpedPlanetVertexShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool BumpedPlanetVertexShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void BumpedPlanetVertexShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
															const FBumpedPlanetVertexVariableParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FBumpedPlanetVertexVariableParametersRef lVertexVariableParametersBuffer;
	lVertexVariableParametersBuffer = FBumpedPlanetVertexVariableParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FBumpedPlanetVertexVariableParameters>& lVertexVariableParameters = this->GetUniformBufferParameter<FBumpedPlanetVertexVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lVertexVariableParameters, lVertexVariableParametersBuffer );
}

/*****************************************************************************************************/
BumpedPlanetPixelShader::BumpedPlanetPixelShader()
{

}

/*****************************************************************************************************/
BumpedPlanetPixelShader::BumpedPlanetPixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{
	this->mBumpTextureParameter.Bind( pInitializer.ParameterMap, TEXT( "BumpTexture" ) );
	this->mGroundTextureParameter.Bind( pInitializer.ParameterMap, TEXT( "GroundTexture" ) );
	this->mTextureParameterSampler.Bind( pInitializer.ParameterMap, TEXT( "TextureSampler" ) );
}

/*****************************************************************************************************/
bool BumpedPlanetPixelShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool BumpedPlanetPixelShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there
	pArchive << this->mBumpTextureParameter;
	pArchive << this->mGroundTextureParameter;
	pArchive << this->mTextureParameterSampler;

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void BumpedPlanetPixelShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
														   const FBumpedPlanetPixelVariableParameters& pPixelParameters,
														   const UTexture2D* pBumpTexture,
														   const UTexture2D* pGroundTexture)
{
	const FPixelShaderRHIParamRef lPixelShader = this->GetPixelShader();

	// Create the uniform buffer references
	FBumpedPlanetPixelVariableParametersRef lPixelVariableParametersBuffer;
	lPixelVariableParametersBuffer = FBumpedPlanetPixelVariableParametersRef::CreateUniformBufferImmediate( pPixelParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FBumpedPlanetPixelVariableParameters>& lPixelVariableParameters = this->GetUniformBufferParameter<FBumpedPlanetPixelVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lPixelShader, lPixelVariableParameters, lPixelVariableParametersBuffer );

	FTexture2DResource* lBump2DResource   = StaticCast<FTexture2DResource*>( pBumpTexture->Resource );
	FTexture2DResource* lGround2DResource = StaticCast<FTexture2DResource*>( pGroundTexture->Resource );

	// Set the bumpmap texture to the pixel shader as well.
	SetTextureParameter( pRHICmdList,
						 lPixelShader,
						 this->mBumpTextureParameter,
						 lBump2DResource->GetTexture2DRHI() );

	// Set the ground texture to the pixel shader as well.
	SetTextureParameter( pRHICmdList,
						 lPixelShader,
						 this->mGroundTextureParameter,
						 lGround2DResource->GetTexture2DRHI() );

	FSamplerStateRHIParamRef lTextureSampler = TStaticSamplerState< SF_Point, AM_Clamp, AM_Clamp, AM_Clamp >::GetRHI();

	SetSamplerParameter( pRHICmdList,
						 lPixelShader,
						 this->mTextureParameterSampler,
						 lTextureSampler );
}

/**
 * This is what will instantiate the shader into the engine FROM the Engine/Shaders folder of UE4
 * not yours for info ;-), so make sure your .usf is placed in UE4/Engine/Shaders
 */
//                       ShaderClass					 ShaderFileName (.usf)         Shader function name         Stage
IMPLEMENT_SHADER_TYPE( , BumpedPlanetVertexShader, TEXT( "BumpedPlanetShader" ), TEXT( "BumpedPlanetVertexStage" ), SF_Vertex );
IMPLEMENT_SHADER_TYPE( , BumpedPlanetPixelShader,  TEXT( "BumpedPlanetShader" ), TEXT( "BumpedPlanetPixelStage" ),  SF_Pixel );
