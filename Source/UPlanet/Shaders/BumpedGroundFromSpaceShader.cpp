
#include "UPlanet.h"
#include "BumpedGroundFromSpaceShader.h"
#include "ShaderParameterUtils.h"
#include "RHIStaticStates.h"

/**
 * These are needed to actually implement the constant buffers so
 * they are available inside our shader.
 * They also need to be unique over the entire solution since
 * they can in fact be accessed from any shader.
 */
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedGroundFromSpaceVertexConstantParameters, TEXT( "BumpedGroundFromSpaceVConstants" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedGroundFromSpaceVertexVariableParameters, TEXT( "BumpedGroundFromSpaceVVariables" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FBumpedGroundFromSpacePixelVariableParameters,  TEXT( "BumpedGroundFromSpacePVariables" ) )

/*****************************************************************************************************/
BumpedGroundFromSpaceVertexShader::BumpedGroundFromSpaceVertexShader()
{

}

/*****************************************************************************************************/
BumpedGroundFromSpaceVertexShader::BumpedGroundFromSpaceVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool BumpedGroundFromSpaceVertexShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool BumpedGroundFromSpaceVertexShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void BumpedGroundFromSpaceVertexShader::SetConstantUniformParameters(FRHICommandList& pRHICmdList,
																	 const FBumpedGroundFromSpaceVertexConstantParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FBumpedGroundFromSpaceVertexConstantParametersRef lConstantParametersBuffer;
	lConstantParametersBuffer = FBumpedGroundFromSpaceVertexConstantParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the constant parameters container
	const TShaderUniformBufferParameter<FBumpedGroundFromSpaceVertexConstantParameters>& lConstantParameters = this->GetUniformBufferParameter<FBumpedGroundFromSpaceVertexConstantParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lConstantParameters, lConstantParametersBuffer );
}

/*****************************************************************************************************/
void BumpedGroundFromSpaceVertexShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
																	 const FBumpedGroundFromSpaceVertexVariableParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FBumpedGroundFromSpaceVertexVariableParametersRef lVertexVariableParametersBuffer;
	lVertexVariableParametersBuffer = FBumpedGroundFromSpaceVertexVariableParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FBumpedGroundFromSpaceVertexVariableParameters>& lVertexVariableParameters = this->GetUniformBufferParameter<FBumpedGroundFromSpaceVertexVariableParameters>();
	
	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lVertexVariableParameters, lVertexVariableParametersBuffer );
}

/*****************************************************************************************************/
BumpedGroundFromSpacePixelShader::BumpedGroundFromSpacePixelShader()
{

}

/*****************************************************************************************************/
BumpedGroundFromSpacePixelShader::BumpedGroundFromSpacePixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{
	this->mBumpTextureParameter.Bind( pInitializer.ParameterMap, TEXT( "BumpTexture" ) );
	this->mGroundTextureParameter.Bind( pInitializer.ParameterMap, TEXT( "GroundTexture" ) );
	this->mTextureParameterSampler.Bind( pInitializer.ParameterMap, TEXT( "TextureSampler" ) );
}

/*****************************************************************************************************/
bool BumpedGroundFromSpacePixelShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool BumpedGroundFromSpacePixelShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there
	pArchive << this->mBumpTextureParameter;
	pArchive << this->mGroundTextureParameter;
	pArchive << this->mTextureParameterSampler;

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void BumpedGroundFromSpacePixelShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
																	const FBumpedGroundFromSpacePixelVariableParameters& pPixelParameters,
																	const UTexture2D* pBumpTexture,
																	const UTexture2D* pGroundTexture)
{
	const FPixelShaderRHIParamRef lPixelShader = this->GetPixelShader();

	// Create the uniform buffer references
	FBumpedGroundFromSpacePixelVariableParametersRef lPixelVariableParametersBuffer;
	lPixelVariableParametersBuffer = FBumpedGroundFromSpacePixelVariableParametersRef::CreateUniformBufferImmediate( pPixelParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FBumpedGroundFromSpacePixelVariableParameters>& lPixelVariableParameters = this->GetUniformBufferParameter<FBumpedGroundFromSpacePixelVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lPixelShader, lPixelVariableParameters, lPixelVariableParametersBuffer );

	FTexture2DResource* lBump2DResource   = StaticCast<FTexture2DResource*>( pBumpTexture->Resource );
	FTexture2DResource* lGround2DResource = StaticCast<FTexture2DResource*>( pGroundTexture->Resource );

	// Set the bumpmap texture to the pixel shader as well.
	SetTextureParameter( pRHICmdList,
						 lPixelShader,
						 this->mBumpTextureParameter,
						 lBump2DResource->GetTexture2DRHI() );

	// Set the ground texture to the pixel shader as well.
	SetTextureParameter( pRHICmdList,
						 lPixelShader,
						 this->mGroundTextureParameter,
						 lGround2DResource->GetTexture2DRHI() );

	FSamplerStateRHIParamRef lTextureSampler = TStaticSamplerState< SF_Point, AM_Clamp, AM_Clamp, AM_Clamp >::GetRHI();

	SetSamplerParameter( pRHICmdList,
						 lPixelShader,
						 this->mTextureParameterSampler,
						 lTextureSampler );
}

/**
 * This is what will instantiate the shader into the engine FROM the Engine/Shaders folder of UE4
 * not yours for info ;-), so make sure your .usf is placed in UE4/Engine/Shaders
 */
//                       ShaderClass                        ShaderFileName (.usf)                  Shader function name                        Stage
IMPLEMENT_SHADER_TYPE( , BumpedGroundFromSpaceVertexShader, TEXT( "BumpedGroundFromSpaceShader" ), TEXT( "BumpedGroundFromSpaceVertexStage" ), SF_Vertex );
IMPLEMENT_SHADER_TYPE( , BumpedGroundFromSpacePixelShader,  TEXT( "BumpedGroundFromSpaceShader" ), TEXT( "BumpedGroundFromSpacePixelStage" ),  SF_Pixel );
