
//==========================================
// Includes
//==========================================
#include "UPlanet.h"
#include "RHIStaticStates.h"
#include "PlanetaryNodeRenderer.h"
#include "FPlanetaryShaderVertexLayout.h"
#include "../Renderer/PlanetaryNodeMeshSceneProxy.h"

//==========================================
// Static initialization
//==========================================
FBumpedPlanetVertexVariableParameters		   FPlanetaryNodeRenderer::NoAtmosphereVertexVariables    = FBumpedPlanetVertexVariableParameters();
FBumpedPlanetPixelVariableParameters		   FPlanetaryNodeRenderer::NoAtmospherePixelVariables     = FBumpedPlanetPixelVariableParameters();
FBumpedGroundFromAtmoVertexConstantParameters  FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants  = FBumpedGroundFromAtmoVertexConstantParameters();
FBumpedGroundFromAtmoVertexVariableParameters  FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables  = FBumpedGroundFromAtmoVertexVariableParameters();
FBumpedGroundFromAtmoPixelVariableParameters   FPlanetaryNodeRenderer::GroundFromAtmoPixelVariables   = FBumpedGroundFromAtmoPixelVariableParameters();
FBumpedGroundFromSpaceVertexConstantParameters FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants = FBumpedGroundFromSpaceVertexConstantParameters();
FBumpedGroundFromSpaceVertexVariableParameters FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables = FBumpedGroundFromSpaceVertexVariableParameters();
FBumpedGroundFromSpacePixelVariableParameters  FPlanetaryNodeRenderer::GroundFromSpacePixelVariables  = FBumpedGroundFromSpacePixelVariableParameters();

//It seems to be the convention to expose all vertex declarations as globals, and then reference them as externs in the headers where they are needed.
//It kind of makes sense since they do not contain any parameters that change and are purely used as their names suggest, as declarations :)
TGlobalResource<FPlanetaryShaderVertexLayout> GPlanetaryVertexLayout;

/************************************************************************************************/
FPlanetaryNodeRenderer::FPlanetaryNodeRenderer(ERHIFeatureLevel::Type pFeatureLevel) :
mFeatureLevel( pFeatureLevel )
{

}

/************************************************************************************************/
FPlanetaryNodeRenderer::~FPlanetaryNodeRenderer()
{

}

/************************************************************************************************/
void FPlanetaryNodeRenderer::ExecuteShader(const FPlanetaryNodeMeshSceneProxy* pProxy)
{
	check( IsInRenderingThread() );

	FRHICommandListImmediate& lRHICmdList = GRHICommandList.GetImmediateCommandList();

	static FGlobalBoundShaderState lBoundShaderState;
	float lRayleighScaleDepth = 0.25f;
	float l4PI = 4.0f * PI;

	// Bumped ground from atmosphere effect
	if 
		( pProxy->IsInAtmosphere )
	{
		TShaderMapRef<BumpedGroundFromAtmoVertexShader> lVertexStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		TShaderMapRef<BumpedGroundFromAtmoPixelShader>  lPixelStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );

		// Set the shader to use (make current in OpenGL) passing vertex layout and stages to use for graphic pipeline
		SetGlobalBoundShaderState( lRHICmdList, this->mFeatureLevel, lBoundShaderState, GPlanetaryVertexLayout.VertexDeclarationRHI(), *lVertexStageRef, *lPixelStageRef );

		// Vertex constants
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.InvWavelength = FVector( 1 / powf( pProxy->Wavelength[ 0 ], 4.0f ), 1 / powf( pProxy->Wavelength[ 1 ], 4.0f ), 1 / powf( pProxy->Wavelength[ 2 ], 4.0f ) );
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.InnerRadius   = pProxy->PlanetaryRadius;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.InnerRadius2  = FMath::Square( pProxy->PlanetaryRadius );
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.OuterRadius   = pProxy->AtmosphericRadius;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.OuterRadius2  = FMath::Square( pProxy->AtmosphericRadius );
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.KrESun = pProxy->Kr * pProxy->Esun;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.KmESun = pProxy->Km * pProxy->Esun;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.Kr4PI  = pProxy->Kr * l4PI;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.Km4PI  = pProxy->Km * l4PI;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.Scale  = 1.0f / ( pProxy->AtmosphericRadius - pProxy->PlanetaryRadius );
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.ScaleDepth = lRayleighScaleDepth;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.ScaleOverScaleDepth = ( 1.0f / ( pProxy->AtmosphericRadius - pProxy->PlanetaryRadius ) ) / lRayleighScaleDepth;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.iSamples = 2;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants.fSamples = 2.0;

		lVertexStageRef->SetConstantUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::GroundFromAtmoVertexConstants );

		// Vertex variables
		FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables.WorldViewProj = pProxy->WorldViewProj;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables.CameraPos     = pProxy->CameraLocation;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables.LightPos      = pProxy->SunLightLocation;
		FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables.CameraHeight  = pProxy->CameraLocation.Size();
		FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables.CameraHeight2 = pProxy->CameraLocation.SizeSquared();

		lVertexStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::GroundFromAtmoVertexVariables );

		// Pixel variables
		FPlanetaryNodeRenderer::GroundFromAtmoPixelVariables.LightPos = pProxy->SunLightLocation;

		lPixelStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::GroundFromAtmoPixelVariables, pProxy->BumpTexture, pProxy->GroundTexture );

	}
	// Bumped ground from space effect
	else if 
		( pProxy->HasAtmosphere )
	{
		TShaderMapRef<BumpedGroundFromSpaceVertexShader> lVertexStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		TShaderMapRef<BumpedGroundFromSpacePixelShader>  lPixelStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		
		// Set the shader to use (make current in OpenGL) passing vertex layout and stages to use for graphic pipeline
		SetGlobalBoundShaderState( lRHICmdList, this->mFeatureLevel, lBoundShaderState, GPlanetaryVertexLayout.VertexDeclarationRHI(), *lVertexStageRef, *lPixelStageRef );

		// Vertex constants
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.InvWavelength = FVector(1 / powf(pProxy->Wavelength[0], 4.0f), 1 / powf(pProxy->Wavelength[1], 4.0f), 1 / powf(pProxy->Wavelength[2], 4.0f));
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.InnerRadius   = pProxy->PlanetaryRadius;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.InnerRadius2  = FMath::Square(pProxy->PlanetaryRadius);
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.OuterRadius   = pProxy->AtmosphericRadius;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.OuterRadius2  = FMath::Square(pProxy->AtmosphericRadius);
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.KrESun = pProxy->Kr * pProxy->Esun;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.KmESun = pProxy->Km * pProxy->Esun;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.Kr4PI  = pProxy->Kr * l4PI;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.Km4PI  = pProxy->Km * l4PI;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.Scale  = 1.0f / (pProxy->AtmosphericRadius - pProxy->PlanetaryRadius);
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.ScaleDepth = lRayleighScaleDepth;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.ScaleOverScaleDepth = (1.0f / (pProxy->AtmosphericRadius - pProxy->PlanetaryRadius)) / lRayleighScaleDepth;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.iSamples = 2;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants.fSamples = 2.0;

		lVertexStageRef->SetConstantUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::GroundFromSpaceVertexConstants );

		// Vertex variables
		FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables.WorldViewProj = pProxy->WorldViewProj;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables.CameraPos	 = pProxy->CameraLocation;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables.LightPos		 = pProxy->SunLightLocation;
		FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables.CameraHeight  = pProxy->CameraLocation.Size();
		FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables.CameraHeight2 = pProxy->CameraLocation.SizeSquared();

		lVertexStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::GroundFromSpaceVertexVariables );

		// Pixel variables
		FPlanetaryNodeRenderer::GroundFromSpacePixelVariables.LightPos = pProxy->SunLightLocation;

		lPixelStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::GroundFromSpacePixelVariables, pProxy->BumpTexture, pProxy->GroundTexture );
	}
	// Simple bumped ground
	else
	{
		TShaderMapRef<BumpedPlanetVertexShader> lVertexStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		TShaderMapRef<BumpedPlanetPixelShader>  lPixelStageRef( GetGlobalShaderMap( this->mFeatureLevel ) );
		
		// Set the shader to use (make current in OpenGL) passing vertex layout and stages to use for graphic pipeline
		SetGlobalBoundShaderState( lRHICmdList, this->mFeatureLevel, lBoundShaderState, GPlanetaryVertexLayout.VertexDeclarationRHI(), *lVertexStageRef, *lPixelStageRef );

		// Vertex variables
		FPlanetaryNodeRenderer::NoAtmosphereVertexVariables.WorldViewProj = pProxy->WorldViewProj;

		lVertexStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::NoAtmosphereVertexVariables );

		// Pixel variables
		FPlanetaryNodeRenderer::NoAtmospherePixelVariables.LightPos = pProxy->SunLightLocation;

		if 
			( pProxy->DefaultTexture.IsValid() )
		{
			UTexture2D* lDefault = pProxy->DefaultTexture.Get();
			lPixelStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::NoAtmospherePixelVariables, lDefault, lDefault );
		}
		else
		{
			lPixelStageRef->SetVariableUniformParameters( lRHICmdList, FPlanetaryNodeRenderer::NoAtmospherePixelVariables, pProxy->BumpTexture, pProxy->GroundTexture );
		}
	}

	// Render the primitive
	uint32 lStride = sizeof(FPlanetaryMeshVertex);
	DrawPrimitiveUP( lRHICmdList, PT_TriangleList, pProxy->PrimitiveCount(), pProxy->Vertices().GetData(), lStride );
}

/************************************************************************************************/
void FPlanetaryNodeRenderer::ExecuteShader(const FPlanetaryNodeMeshSceneProxy* pProxy, const FMatrix* pWorldViewMatrix)
{
	// If override the world view projection, set it into the proxy instance
	// to replace the old one (that can be previously NULL actually depending 
	// on the previous process
	if
		( pWorldViewMatrix != NULL )
	{
		FPlanetaryNodeMeshSceneProxy* lProxy = const_cast<FPlanetaryNodeMeshSceneProxy*>( pProxy );
		lProxy->WorldViewProj = *pWorldViewMatrix;
	}

	// Execute the shader afterward
	this->ExecuteShader( pProxy );
}
