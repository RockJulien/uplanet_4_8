
#include "UPlanet.h"
#include "SkyFromAtmoShader.h"
#include "ShaderParameterUtils.h"
#include "RHIStaticStates.h"

/**
* These are needed to actually implement the constant buffers so
* they are available inside our shader.
* They also need to be unique over the entire solution since
* they can in fact be accessed from any shader.
*/
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FSkyFromAtmoVertexConstantParameters, TEXT( "SkyFromAtmoVConstants" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FSkyFromAtmoVertexVariableParameters, TEXT( "SkyFromAtmoVVariables" ) )
IMPLEMENT_UNIFORM_BUFFER_STRUCT( FSkyFromAtmoPixelVariableParameters,  TEXT( "SkyFromAtmoPVariables" ) )

/*****************************************************************************************************/
SkyFromAtmoVertexShader::SkyFromAtmoVertexShader()
{

}

/*****************************************************************************************************/
SkyFromAtmoVertexShader::SkyFromAtmoVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool SkyFromAtmoVertexShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool SkyFromAtmoVertexShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void SkyFromAtmoVertexShader::SetConstantUniformParameters(FRHICommandList& pRHICmdList,
														   const FSkyFromAtmoVertexConstantParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FSkyFromAtmoVertexConstantParametersRef lConstantParametersBuffer;
	lConstantParametersBuffer = FSkyFromAtmoVertexConstantParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the constant parameters container
	const TShaderUniformBufferParameter<FSkyFromAtmoVertexConstantParameters>& lConstantParameters = this->GetUniformBufferParameter<FSkyFromAtmoVertexConstantParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lConstantParameters, lConstantParametersBuffer );
}

/*****************************************************************************************************/
void SkyFromAtmoVertexShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
														   const FSkyFromAtmoVertexVariableParameters& pVertexParameters)
{
	const FVertexShaderRHIParamRef lVertexShader = this->GetVertexShader();

	// Create the uniform buffer references
	FSkyFromAtmoVertexVariableParametersRef lVertexVariableParametersBuffer;
	lVertexVariableParametersBuffer = FSkyFromAtmoVertexVariableParametersRef::CreateUniformBufferImmediate( pVertexParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FSkyFromAtmoVertexVariableParameters>& lVertexVariableParameters = this->GetUniformBufferParameter<FSkyFromAtmoVertexVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lVertexShader, lVertexVariableParameters, lVertexVariableParametersBuffer );
}

/*****************************************************************************************************/
SkyFromAtmoPixelShader::SkyFromAtmoPixelShader()
{

}

/*****************************************************************************************************/
SkyFromAtmoPixelShader::SkyFromAtmoPixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer) :
FGlobalShader( pInitializer )
{

}

/*****************************************************************************************************/
bool SkyFromAtmoPixelShader::ShouldCache(EShaderPlatform pPlatform)
{
	return IsFeatureLevelSupported( pPlatform, ERHIFeatureLevel::SM5 );
}

/*****************************************************************************************************/
bool SkyFromAtmoPixelShader::Serialize(FArchive& pArchive)
{
	bool lHasOutdatedParams = FGlobalShader::Serialize( pArchive );

	// Serialize what you want there

	return lHasOutdatedParams;
}

/*****************************************************************************************************/
void SkyFromAtmoPixelShader::SetVariableUniformParameters(FRHICommandList& pRHICmdList,
														  const FSkyFromAtmoPixelVariableParameters& pPixelParameters)
{
	const FPixelShaderRHIParamRef lPixelShader = this->GetPixelShader();

	// Create the uniform buffer references
	FSkyFromAtmoPixelVariableParametersRef lPixelVariableParametersBuffer;
	lPixelVariableParametersBuffer = FSkyFromAtmoPixelVariableParametersRef::CreateUniformBufferImmediate( pPixelParameters, UniformBuffer_SingleDraw );

	// Get the variable parameters containers
	const TShaderUniformBufferParameter<FSkyFromAtmoVertexVariableParameters>& lVertexVariableParameters = this->GetUniformBufferParameter<FSkyFromAtmoVertexVariableParameters>();
	const TShaderUniformBufferParameter<FSkyFromAtmoPixelVariableParameters>& lPixelVariableParameters   = this->GetUniformBufferParameter<FSkyFromAtmoPixelVariableParameters>();

	// Set uniforms to the shader
	SetUniformBufferParameter( pRHICmdList, lPixelShader, lPixelVariableParameters, lPixelVariableParametersBuffer );
}

/**
 * This is what will instantiate the shader into the engine FROM the Engine/Shaders folder of UE4
 * not yours for info ;-), so make sure your .usf is placed in UE4/Engine/Shaders
 */
//                       ShaderClass					ShaderFileName (.usf)        Shader function name        Stage
IMPLEMENT_SHADER_TYPE( , SkyFromAtmoVertexShader, TEXT( "SkyFromAtmoShader" ), TEXT( "SkyFromAtmoVertexStage" ), SF_Vertex );
IMPLEMENT_SHADER_TYPE( , SkyFromAtmoPixelShader,  TEXT( "SkyFromAtmoShader" ), TEXT( "SkyFromAtmoPixelStage" ),  SF_Pixel );
