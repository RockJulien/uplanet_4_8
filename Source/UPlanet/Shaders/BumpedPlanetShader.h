
#pragma once

//==========================================
// Includes
//==========================================
#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"

//==========================================
// Vertex stage shader uniforms
//==========================================
/**
 * This buffer should contain variables that never, or rarely change
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedPlanetVertexConstantParameters, )
// Nothing
END_UNIFORM_BUFFER_STRUCT(FBumpedPlanetVertexConstantParameters)

/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedPlanetVertexVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FMatrix, WorldViewProj)      // The world view projection matrix
END_UNIFORM_BUFFER_STRUCT(FBumpedPlanetVertexVariableParameters)

//==========================================
// Pixel stage shader uniforms
//==========================================
/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedPlanetPixelVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
END_UNIFORM_BUFFER_STRUCT(FBumpedPlanetPixelVariableParameters)

//==========================================
// Typedefs & Constants
//==========================================
typedef TUniformBufferRef<FBumpedPlanetVertexConstantParameters> FBumpedPlanetVertexConstantParametersRef;
typedef TUniformBufferRef<FBumpedPlanetVertexVariableParameters> FBumpedPlanetVertexVariableParametersRef;
typedef TUniformBufferRef<FBumpedPlanetPixelVariableParameters>  FBumpedPlanetPixelVariableParametersRef;

//==========================================
// Class definition
//==========================================
/**
 * Bumped planet rendering vertex shader class
 */
class BumpedPlanetVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(BumpedPlanetVertexShader, Global);

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	BumpedPlanetVertexShader();
	explicit BumpedPlanetVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FBumpedPlanetVertexVariableParameters& pVertexParameters);
};

/**
 * Bumped planet rendering pixel shader class
 */
class BumpedPlanetPixelShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(BumpedPlanetPixelShader, Global);

private:

	/**
	 * The Bumpmap texture resource used in the shader pixel stage
	 */
	FShaderResourceParameter mBumpTextureParameter;

	/**
	 * The Ground texture resource used in the shader pixel stage
	 */
	FShaderResourceParameter mGroundTextureParameter;

	/**
	 * The Texture sampler used in the shader pixel stage
	 */
	FShaderResourceParameter mTextureParameterSampler;

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	BumpedPlanetPixelShader();
	explicit BumpedPlanetPixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FBumpedPlanetPixelVariableParameters& pPixelParameters,
									  const UTexture2D* pBumpTexture,
									  const UTexture2D* pGroundTexture);
};
