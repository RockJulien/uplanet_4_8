

#pragma once

//==========================================
// Includes
//==========================================
#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"

//==========================================
// Vertex stage shader uniforms
//==========================================
/**
 * This buffer should contain variables that never, or rarely change
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FSkyFromAtmoVertexConstantParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, InvWavelength)	  // 1 / pow(wavelength, 4) for the red, green, and blue channels
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, OuterRadius)		  // The outer (atmosphere) radius
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, OuterRadius2)		  // fOuterRadius^2
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, InnerRadius)		  // The inner (planetary) radius
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, InnerRadius2)		  // fInnerRadius^2
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, KrESun)				  // Kr * ESun
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, KmESun)				  // Km * ESun
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Kr4PI)				  // Kr * 4 * PI
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Km4PI)				  // Km * 4 * PI
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Scale)				  // 1 / (fOuterRadius - fInnerRadius)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, ScaleDepth)			  // The scale depth (i.e. the altitude at which the atmosphere's average density is found)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, ScaleOverScaleDepth)  // fScale / fScaleDepth
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(int32, iSamples)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, fSamples)
END_UNIFORM_BUFFER_STRUCT(FSkyFromAtmoVertexConstantParameters)

/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FSkyFromAtmoVertexVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FMatrix, WorldViewProj)      // The world view projection matrix
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, CameraPos)		  // The camera's current position
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, CameraHeight)		  // The camera's current height
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, CameraHeight2)		  // fCameraHeight^2
END_UNIFORM_BUFFER_STRUCT(FSkyFromAtmoVertexVariableParameters)

//==========================================
// Pixel stage shader uniforms
//==========================================
/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FSkyFromAtmoPixelVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, G)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, G2)
END_UNIFORM_BUFFER_STRUCT(FSkyFromAtmoPixelVariableParameters)


//==========================================
// Typedefs & Constants
//==========================================
typedef TUniformBufferRef<FSkyFromAtmoVertexConstantParameters> FSkyFromAtmoVertexConstantParametersRef;
typedef TUniformBufferRef<FSkyFromAtmoVertexVariableParameters> FSkyFromAtmoVertexVariableParametersRef;
typedef TUniformBufferRef<FSkyFromAtmoPixelVariableParameters>  FSkyFromAtmoPixelVariableParametersRef;

//==========================================
// Class definition
//==========================================
/**
 * Sky from atmosphere rendering vertex shader class
 */
class SkyFromAtmoVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(SkyFromAtmoVertexShader, Global);

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	SkyFromAtmoVertexShader();
	explicit SkyFromAtmoVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set constant uniforms to the shader (that never change)
	 */
	void SetConstantUniformParameters(FRHICommandList& pRHICmdList,
									  const FSkyFromAtmoVertexConstantParameters& pVertexParameters);

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FSkyFromAtmoVertexVariableParameters& pVertexParameters);
};

/**
 * Sky from atmosphere rendering pixel shader class
 */
class SkyFromAtmoPixelShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(SkyFromAtmoPixelShader, Global);

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	SkyFromAtmoPixelShader();
	explicit SkyFromAtmoPixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FSkyFromAtmoPixelVariableParameters& pPixelParameters);
};
