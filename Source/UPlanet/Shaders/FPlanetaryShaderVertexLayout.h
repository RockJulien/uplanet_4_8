
#pragma once

//==========================================
// Includes
//==========================================
#include "RenderResource.h"

//==========================================
// Class definition
//==========================================
/**
 * Planetary shader vertex layout definition
 */
class UPLANET_API FPlanetaryShaderVertexLayout : public FRenderResource
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * Planetary vertex layout definition reference
	 */
	FVertexDeclarationRHIRef mVertexDeclarationRHI;

public:

	//==========================================
	// Methods
	//==========================================

	/**
	 * Initialize the Planetary vertex layout definition reference
	 */
	virtual void InitRHI() override;

	/**
	 * Release the Planetary vertex layout definition reference
	 */
	virtual void ReleaseRHI() override;

	/**
	 * Returns the Planetary vertex layout definition reference
	 */
	FORCEINLINE FVertexDeclarationRHIRef VertexDeclarationRHI() const;
};

//==========================================
// Inlines
//==========================================
#include "FPlanetaryShaderVertexLayout.inl"
