
#pragma once

//==========================================
// Includes
//==========================================
#include "GlobalShader.h"
#include "UniformBuffer.h"
#include "RHICommandList.h"

//==========================================
// Vertex stage shader uniforms
//==========================================
/**
 * This buffer should contain variables that never, or rarely change
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromSpaceVertexConstantParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, InvWavelength)	  // 1 / pow(wavelength, 4) for the red, green, and blue channels
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, OuterRadius)		  // The outer (atmosphere) radius
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, OuterRadius2)		  // fOuterRadius^2
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, InnerRadius)		  // The inner (planetary) radius
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, InnerRadius2)		  // fInnerRadius^2
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, KrESun)				  // Kr * ESun
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, KmESun)				  // Km * ESun
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Kr4PI)				  // Kr * 4 * PI
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Km4PI)				  // Km * 4 * PI
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, Scale)				  // 1 / (fOuterRadius - fInnerRadius)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, ScaleDepth)			  // The scale depth (i.e. the altitude at which the atmosphere's average density is found)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, ScaleOverScaleDepth)  // fScale / fScaleDepth
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(int32, iSamples)
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, fSamples)
END_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromSpaceVertexConstantParameters)

/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromSpaceVertexVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FMatrix, WorldViewProj)      // The world view projection matrix
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, CameraPos)		  // The camera's current position
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, CameraHeight)		  // The camera's current height
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(float, CameraHeight2)		  // fCameraHeight^2
END_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromSpaceVertexVariableParameters)

//==========================================
// Pixel stage shader uniforms
//==========================================
/**
 * This buffer is for variables that change very often (each frame for example)
 */
BEGIN_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromSpacePixelVariableParameters, )
DECLARE_UNIFORM_BUFFER_STRUCT_MEMBER(FVector, LightPos)			  // The direction vector to the light source
END_UNIFORM_BUFFER_STRUCT(FBumpedGroundFromSpacePixelVariableParameters)

//==========================================
// Typedefs & Constants
//==========================================
typedef TUniformBufferRef<FBumpedGroundFromSpaceVertexConstantParameters> FBumpedGroundFromSpaceVertexConstantParametersRef;
typedef TUniformBufferRef<FBumpedGroundFromSpaceVertexVariableParameters> FBumpedGroundFromSpaceVertexVariableParametersRef;
typedef TUniformBufferRef<FBumpedGroundFromSpacePixelVariableParameters>  FBumpedGroundFromSpacePixelVariableParametersRef;

//==========================================
// Class definition
//==========================================
/**
 * Bumped ground from space rendering vertex shader class
 */
class BumpedGroundFromSpaceVertexShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(BumpedGroundFromSpaceVertexShader, Global);

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	BumpedGroundFromSpaceVertexShader();
	explicit BumpedGroundFromSpaceVertexShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set constant uniforms to the shader (that never change)
	 */
	void SetConstantUniformParameters(FRHICommandList& pRHICmdList,
									  const FBumpedGroundFromSpaceVertexConstantParameters& pVertexParameters);

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FBumpedGroundFromSpaceVertexVariableParameters& pVertexParameters);
};

/**
 * Bumped ground from space rendering pixel shader class
 */
class BumpedGroundFromSpacePixelShader : public FGlobalShader
{
	DECLARE_SHADER_TYPE(BumpedGroundFromSpacePixelShader, Global);

private:

	/**
	 * The Bumpmap texture resource used in the shader pixel stage
	 */
	FShaderResourceParameter mBumpTextureParameter;

	/**
	 * The Ground texture resource used in the shader pixel stage
	 */
	FShaderResourceParameter mGroundTextureParameter;

	/**
	 * The Texture sampler used in the shader pixel stage
	 */
	FShaderResourceParameter mTextureParameterSampler;

public:

	//==========================================
	// Constructor(s) & Destructor
	//==========================================
	BumpedGroundFromSpacePixelShader();
	explicit BumpedGroundFromSpacePixelShader(const ShaderMetaType::CompiledShaderInitializerType& pInitializer);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Informs whether that shader should be kept in cache or not
	 */
	static bool ShouldCache(EShaderPlatform pPlatform);

	/**
	 * Save relevant elements of that shader (if any)
	 */
	virtual bool Serialize(FArchive& pArchive) override;

	/**
	 * This function set per frame uniforms to the shader
	 */
	void SetVariableUniformParameters(FRHICommandList& pRHICmdList,
									  const FBumpedGroundFromSpacePixelVariableParameters& pPixelParameters,
									  const UTexture2D* pBumpTexture,
									  const UTexture2D* pGroundTexture);
};
