
#include "UPlanet.h"
#include "SkydomeVertex.h"
#include "SkydomeShaderVertexLayout.h"

/*******************************************************************************************/
void FSkydomeShaderVertexLayout::InitRHI()
{
	FVertexDeclarationElementList lDefinition;
	uint32 lStride = sizeof(FSkydomeVertex);
	lDefinition.Add( FVertexElement( 0, STRUCT_OFFSET( FSkydomeVertex, Position ), VET_Float3, 0, lStride ) );
	this->mVertexDeclarationRHI = RHICreateVertexDeclaration( lDefinition );
}

/*******************************************************************************************/
void FSkydomeShaderVertexLayout::ReleaseRHI()
{
	this->mVertexDeclarationRHI.SafeRelease();
}
