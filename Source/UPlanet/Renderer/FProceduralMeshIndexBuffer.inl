
/**********************************************************************************************/
FORCEINLINE TArray<int32>& FProceduralMeshIndexBuffer::GetIBO()
{
	return this->mIndices;
}

/**********************************************************************************************/
FORCEINLINE const TArray<int32>& FProceduralMeshIndexBuffer::GetIBO() const
{
	return this->mIndices;
}
