// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "DynamicMeshBuilder.h"
#include "PlanetaryNodeMeshComponent.h"
#include "PlanetaryNodeMeshSceneProxy.h"

/***********************************************************************************************/
UPlanetaryNodeMeshComponent::UPlanetaryNodeMeshComponent(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
	this->PrimaryComponentTick.bCanEverTick = false;

	this->SetCollisionProfileName( UCollisionProfile::BlockAllDynamic_ProfileName );
}

/***********************************************************************************************/
UPlanetaryNodeMeshComponent::~UPlanetaryNodeMeshComponent()
{

}

/***********************************************************************************************/
FBoxSphereBounds UPlanetaryNodeMeshComponent::CalcBounds(const FTransform& pLocalToWorld) const
{
	// Only if have enough triangles
	if 
		( this->mNodeTriangles.Num() > 0 )
	{
		// Minimum Vector: It's set to the first vertex's position initially (NULL == FVector::ZeroVector might be required and a known vertex vector has intrinsically valid values)
		FVector lVecMin = this->mNodeTriangles[ 0 ].Vertex0.Position;

		// Maximum Vector: It's set to the first vertex's position initially (NULL == FVector::ZeroVector might be required and a known vertex vector has intrinsically valid values)
		FVector lVecMax = this->mNodeTriangles[ 0 ].Vertex0.Position;

		// Get maximum and minimum X, Y and Z positions of vectors
		for 
			( int32 lCurrTriangle = 0; lCurrTriangle < this->mNodeTriangles.Num(); lCurrTriangle++ )
		{
			lVecMin.X = (lVecMin.X > this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.X) ? this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.X : lVecMin.X;
			lVecMin.X = (lVecMin.X > this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.X) ? this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.X : lVecMin.X;
			lVecMin.X = (lVecMin.X > this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.X) ? this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.X : lVecMin.X;

			lVecMin.Y = (lVecMin.Y > this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Y) ? this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Y : lVecMin.Y;
			lVecMin.Y = (lVecMin.Y > this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Y) ? this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Y : lVecMin.Y;
			lVecMin.Y = (lVecMin.Y > this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Y) ? this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Y : lVecMin.Y;

			lVecMin.Z = (lVecMin.Z > this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Z) ? this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Z : lVecMin.Z;
			lVecMin.Z = (lVecMin.Z > this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Z) ? this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Z : lVecMin.Z;
			lVecMin.Z = (lVecMin.Z > this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Z) ? this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Z : lVecMin.Z;

			lVecMax.X = (lVecMax.X < this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.X) ? this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.X : lVecMax.X;
			lVecMax.X = (lVecMax.X < this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.X) ? this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.X : lVecMax.X;
			lVecMax.X = (lVecMax.X < this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.X) ? this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.X : lVecMax.X;

			lVecMax.Y = (lVecMax.Y < this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Y) ? this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Y : lVecMax.Y;
			lVecMax.Y = (lVecMax.Y < this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Y) ? this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Y : lVecMax.Y;
			lVecMax.Y = (lVecMax.Y < this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Y) ? this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Y : lVecMax.Y;

			lVecMax.Z = (lVecMax.Z < this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Z) ? this->mNodeTriangles[ lCurrTriangle ].Vertex0.Position.Z : lVecMax.Z;
			lVecMax.Z = (lVecMax.Z < this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Z) ? this->mNodeTriangles[ lCurrTriangle ].Vertex1.Position.Z : lVecMax.Z;
			lVecMax.Z = (lVecMax.Z < this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Z) ? this->mNodeTriangles[ lCurrTriangle ].Vertex2.Position.Z : lVecMax.Z;
		}

		FVector lVecOrigin = ((lVecMax - lVecMin) / 2) + lVecMin;	// Origin = ((Max Vertex's Vector - Min Vertex's Vector) / 2 ) + Min Vertex's Vector
		FVector lBoxPoint  = lVecMax - lVecMin;	// The difference between the "Maximum Vertex" and the "Minimum Vertex" is our actual Bounds Box
		return FBoxSphereBounds( lVecOrigin, 
								 lBoxPoint, 
								 lBoxPoint.Size() ).TransformBy( pLocalToWorld );
	}
	else
	{
		return FBoxSphereBounds();
	}
}

/***********************************************************************************************/
bool UPlanetaryNodeMeshComponent::SetMeshTriangles(const TArray<FPlanetaryMeshTriangle>& pOverrideMeshTriangles)
{
	this->mNodeTriangles = pOverrideMeshTriangles;

	// Update collision data
	this->UpdateCollision();

	// Need to recreate scene proxy to send it over
	this->MarkRenderStateDirty();

	return true;
}

/***********************************************************************************************/
void UPlanetaryNodeMeshComponent::RefreshBodySetup()
{
	if 
		( this->ModelBodySetup == NULL )
	{
		FString lBodyName = this->GetName();
		lBodyName.Append("_ModelBodySetup");
		this->ModelBodySetup = NewObject<UBodySetup>( this, UBodySetup::StaticClass(), *lBodyName );
		this->ModelBodySetup->CollisionTraceFlag = CTF_UseComplexAsSimple;
		this->ModelBodySetup->bMeshCollideAll = true;
	}
}

/***********************************************************************************************/
void UPlanetaryNodeMeshComponent::UpdateCollision()
{
	if 
		( this->bPhysicsStateCreated )
	{
		this->DestroyPhysicsState();
		this->RefreshBodySetup();
		this->CreatePhysicsState();

		// Works in Packaged build only since UE4.5:
		this->ModelBodySetup->InvalidatePhysicsData();
		this->ModelBodySetup->CreatePhysicsMeshes();
	}
}

/***********************************************************************************************/
void UPlanetaryNodeMeshComponent::ReleasePhysics()
{
	if
		( this->bPhysicsStateCreated )
	{
		this->DestroyPhysicsState();

		this->ModelBodySetup->InvalidatePhysicsData();

		this->bPhysicsStateCreated = false;
	}
}

/***********************************************************************************************/
bool UPlanetaryNodeMeshComponent::GetPhysicsTriMeshData(struct FTriMeshCollisionData* pCollisionData, bool pInUseAllTriData)
{
	for 
		( int32 lCurrTriangle = 0; lCurrTriangle < this->mNodeTriangles.Num(); lCurrTriangle++ )
	{
		const FPlanetaryMeshTriangle& lPlanetaryTriangle = this->mNodeTriangles[lCurrTriangle];

		FTriIndices lTriangle;
		lTriangle.v0 = pCollisionData->Vertices.Add( lPlanetaryTriangle.Vertex0.Position );
		lTriangle.v1 = pCollisionData->Vertices.Add( lPlanetaryTriangle.Vertex1.Position );
		lTriangle.v2 = pCollisionData->Vertices.Add( lPlanetaryTriangle.Vertex2.Position );

		pCollisionData->Indices.Add( lTriangle );
		pCollisionData->MaterialIndices.Add( lCurrTriangle );
	}

	pCollisionData->bFlipNormals = true;

	return pCollisionData->Vertices.Num() > 0 && pCollisionData->Indices.Num() > 0;
}

/***********************************************************************************************/
bool UPlanetaryNodeMeshComponent::ContainsPhysicsTriMeshData(bool pInUseAllTriData) const
{
	return false;// (this->mNodeTriangles.Num() > 0);
}

/***********************************************************************************************/
bool UPlanetaryNodeMeshComponent::WantsNegXTriMesh()
{
	return false; 
}

/***********************************************************************************************/
FPrimitiveSceneProxy* UPlanetaryNodeMeshComponent::CreateSceneProxy()
{
	FPrimitiveSceneProxy* lProxy = NULL;

	// Insure you have at least a triangle
	if 
		( this->mNodeTriangles.Num() > 0 )
	{
		UE_LOG( LogClass, Log, TEXT( "UPlanetaryNodeMeshComponent::CreateSceneProxy" ) );
		return new FPlanetaryNodeMeshSceneProxy( this );
	}

	return lProxy;
}

/***********************************************************************************************/
UBodySetup* UPlanetaryNodeMeshComponent::GetBodySetup()
{
	// Refresh definition
	this->RefreshBodySetup();

	return this->ModelBodySetup;
}

/***********************************************************************************************/
int32 UPlanetaryNodeMeshComponent::GetNumMaterials() const
{
	return 1; // For Now
}
