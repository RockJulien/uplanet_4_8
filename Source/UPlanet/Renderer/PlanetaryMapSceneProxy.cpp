// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "../PlanetaryMap.h"
#include "DynamicMeshBuilder.h"
#include "PlanetaryMapSceneProxy.h"

/*************************************************************************************************/
FPlanetaryMapSceneProxy::FPlanetaryMapSceneProxy(const UPlanetaryMap* pMeshComponent) :
FPrimitiveSceneProxy( pMeshComponent ), mSkydomeRenderer( this->GetScene().GetFeatureLevel() )
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
, mMaterialRelevance( pMeshComponent->GetMaterialRelevance( this->GetScene().GetFeatureLevel() ) )
#else
, mMaterialRelevance( pMeshComponent->GetMaterialRelevance() )
#endif
{
	this->mAllowRendering = pMeshComponent->AllowRendering();
	this->mVertices = pMeshComponent->GetVertices();
	this->mIndices  = pMeshComponent->GetIndices();
	this->mPrimitiveCount = this->mIndices.Num() / 3;

	this->IsInAtmosphere	= pMeshComponent->IsInAtmosphere();
	this->HasAtmosphere		= pMeshComponent->HasAtmosphere();
	this->PlanetaryRadius	= pMeshComponent->PlanetaryRadius;
	this->AtmosphericRadius = pMeshComponent->AtmosphericRadius;
	this->CameraLocation	= pMeshComponent->CameraLocation();
	this->SunLightLocation	= pMeshComponent->SunLightPosition();
	this->Wavelength		= pMeshComponent->Wavelength;
	this->Esun				= pMeshComponent->Esun;
	this->Kr				= pMeshComponent->Kr;
	this->Km				= pMeshComponent->Km;
	this->G					= pMeshComponent->G;

}

/*************************************************************************************************/
FPlanetaryMapSceneProxy::~FPlanetaryMapSceneProxy()
{

}

#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
/*************************************************************************************************/
void FPlanetaryMapSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& pViews, const FSceneViewFamily& pViewFamily, uint32 pVisibilityMap, FMeshElementCollector& pCollector) const
{
	QUICK_SCOPE_CYCLE_COUNTER( STAT_PlanetaryMapSceneProxy_GetDynamicMeshElements );

	if 
		( this->mAllowRendering == false )
	{
		return;
	}

	for 
		( int32 lViewIndex = 0; lViewIndex < pViews.Num(); lViewIndex++ )
	{
		if 
			( pVisibilityMap & (1 << lViewIndex) )
		{
			const FSceneView* lView = pViews[ lViewIndex ];

			FMatrix lWorldViewProj = this->GetLocalToWorld() * lView->ViewMatrices.GetViewProjMatrix();

			ENQUEUE_UNIQUE_RENDER_COMMAND_THREEPARAMETER
			( 
				FAtmosphericSkyDomeRendererRunner, // Command typename
				FAtmosphericSkyDomeRenderer*, lSkydomeRenderer, const_cast<FAtmosphericSkyDomeRenderer*>( &this->mSkydomeRenderer ), // First command param
				const FPlanetaryMapSceneProxy*, lPlanetaryMap, this, // Second command param
				FMatrix, lWorldViewProj, lWorldViewProj, // Third command param
				{
					lSkydomeRenderer->ExecuteShader( lPlanetaryMap, &lWorldViewProj ); // Delegate to run
				}
			);
		}
	}
}
#else
/*************************************************************************************************/
void FPlanetaryMapSceneProxy::DrawDynamicElements(FPrimitiveDrawInterface* pPDI, const FSceneView* pView)
{
	QUICK_SCOPE_CYCLE_COUNTER( STAT_PlanetaryMapSceneProxy_DrawDynamicElements );

	// Provide the worl view projection matrix.
	this->WorldViewProj = this->GetLocalToWorld() * pView->ViewMatrices.GetViewProjMatrix();

	// Draw the primitive using the cutom shader (from the rendering thread)
	this->mSkydomeRenderer.ExecuteShader( this );
}
#endif

/*************************************************************************************************/
FPrimitiveViewRelevance FPlanetaryMapSceneProxy::GetViewRelevance(const FSceneView* pView)
{
	FPrimitiveViewRelevance lPrimitiveRelevance;
	lPrimitiveRelevance.bDrawRelevance    = this->IsShown( pView );
	lPrimitiveRelevance.bShadowRelevance  = this->IsShadowCast( pView );
	lPrimitiveRelevance.bDynamicRelevance = true;
	this->mMaterialRelevance.SetPrimitiveViewRelevance( lPrimitiveRelevance );

	return lPrimitiveRelevance;
}

/*************************************************************************************************/
bool FPlanetaryMapSceneProxy::CanBeOccluded() const
{
	return this->mMaterialRelevance.bDisableDepthTest == false;
}

/*************************************************************************************************/
uint32 FPlanetaryMapSceneProxy::GetMemoryFootprint() const
{
	return (sizeof(*this) + this->GetAllocatedSize());
}

/*************************************************************************************************/
uint32 FPlanetaryMapSceneProxy::GetAllocatedSize() const
{
	return FPrimitiveSceneProxy::GetAllocatedSize();
}
