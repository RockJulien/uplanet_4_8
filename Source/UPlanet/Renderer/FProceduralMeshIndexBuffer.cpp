// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "FProceduralMeshIndexBuffer.h"
#include "Runtime/Launch/Resources/Version.h"

/**************************************************************************************************/
FProceduralMeshIndexBuffer::FProceduralMeshIndexBuffer()
{

}

/**************************************************************************************************/
FProceduralMeshIndexBuffer::~FProceduralMeshIndexBuffer()
{

}

/**************************************************************************************************/
void  FProceduralMeshIndexBuffer::InitRHI()
{
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 3
	FRHIResourceCreateInfo lCreateInfo;
	this->IndexBufferRHI = RHICreateIndexBuffer( sizeof( int32 ), this->mIndices.Num() * sizeof( int32 ), BUF_Static, lCreateInfo );
#else
	this->IndexBufferRHI = RHICreateIndexBuffer( sizeof( int32 ), this->mIndices.Num() * sizeof( int32 ), NULL, BUF_Static );
#endif
	// Write the indices to the index buffer.
	void* lBuffer = this->Lock();
	FMemory::Memcpy( lBuffer, this->mIndices.GetData(), this->mIndices.Num() * sizeof( int32 ) );
	this->Unlock();
}

/**************************************************************************************************/
void* FProceduralMeshIndexBuffer::Lock()
{
	return RHILockIndexBuffer( this->IndexBufferRHI, 0, this->mIndices.Num() * sizeof( int32 ), RLM_WriteOnly );
}

/**************************************************************************************************/
void  FProceduralMeshIndexBuffer::Unlock()
{
	RHIUnlockIndexBuffer( this->IndexBufferRHI );
}

/**************************************************************************************************/
FString FProceduralMeshIndexBuffer::GetFriendlyName() const
{ 
	return TEXT( "FProceduralMeshIndexBuffer" ); 
}
