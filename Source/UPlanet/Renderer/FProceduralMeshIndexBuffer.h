// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "DynamicMeshBuilder.h"

//==========================================
// Define and Constants
//==========================================

//==========================================
// Class definition
//==========================================
/**
 * Procedural mesh index buffer class
 */
class UPLANET_API FProceduralMeshIndexBuffer : public FIndexBuffer
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The indices internal buffer
	 */
	TArray<int32> mIndices;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FProceduralMeshIndexBuffer();
	~FProceduralMeshIndexBuffer();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Initialize the indices buffer GPU memory
	 */
	virtual void InitRHI() override;

	/**
	 * Lock the indices buffer and returns its data (for modification is needed)
	 */
	void* Lock();

	/**
	 * Unlock the indices buffer
	 */
	void  Unlock();

	/**
	 * Get the class human-readable name
	 */
	virtual FString GetFriendlyName() const override;// { return TEXT("FProceduralMeshIndexBuffer"); }

	/**
	 * Get a reference to the indices buffer object
	 */
	FORCEINLINE TArray<int32>& GetIBO();

	/**
	 * Get a reference to the indices buffer object (Const Version)
	 */
	FORCEINLINE const TArray<int32>& GetIBO() const;
};

//==========================================
// Inline
//==========================================
#include "FProceduralMeshIndexBuffer.inl"
