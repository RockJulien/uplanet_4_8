// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "../PlanetaryMap.h"
#include "DynamicMeshBuilder.h"
#include "PlanetaryNodeMeshComponent.h"
#include "PlanetaryNodeMeshSceneProxy.h"

/*************************************************************************************************/
FPlanetaryNodeMeshSceneProxy::FPlanetaryNodeMeshSceneProxy(const UPlanetaryNodeMeshComponent* pMeshComponent) :
FPrimitiveSceneProxy( pMeshComponent), mNodeRenderer( this->GetScene().GetFeatureLevel() )
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
, mMaterialRelevance( pMeshComponent->GetMaterialRelevance( this->GetScene().GetFeatureLevel() ) )
#else
, mMaterialRelevance( pMeshComponent->GetMaterialRelevance() )
#endif
{
	this->mPrimitiveCount = pMeshComponent->GetTriangles().Num();
	this->BumpTexture     = pMeshComponent->BumpTexture;
	this->GroundTexture   = pMeshComponent->GroundTexture;

	UPlanetaryMap* lPlanetMap = Cast<UPlanetaryMap>( pMeshComponent->AttachParent );
	this->DefaultTexture    = lPlanetMap->DefaultTexture;
	this->mAllowRendering   = lPlanetMap->AllowRendering();
	this->IsInAtmosphere    = lPlanetMap->IsInAtmosphere();
	this->HasAtmosphere		= lPlanetMap->HasAtmosphere();
	this->PlanetaryRadius   = lPlanetMap->PlanetaryRadius;
	this->AtmosphericRadius = lPlanetMap->AtmosphericRadius;
	this->CameraLocation    = lPlanetMap->CameraLocation();
	this->SunLightLocation  = lPlanetMap->SunLightPosition();
	this->Wavelength	    = lPlanetMap->Wavelength;
	this->Esun			    = lPlanetMap->Esun;
	this->Kr			    = lPlanetMap->Kr;
	this->Km			    = lPlanetMap->Km;
	this->G				    = lPlanetMap->G;

	// Add each triangle to the vertex/index buffers
	for 
		( uint32 lCurrTriangle = 0; lCurrTriangle < this->mPrimitiveCount; lCurrTriangle++ )
	{
		const FPlanetaryMeshTriangle& lTriangle = pMeshComponent->GetTriangle( lCurrTriangle );

		this->mVertices.Add( lTriangle.Vertex0 );
		this->mVertices.Add( lTriangle.Vertex1 );
		this->mVertices.Add( lTriangle.Vertex2 );
	}

}

/*************************************************************************************************/
FPlanetaryNodeMeshSceneProxy::~FPlanetaryNodeMeshSceneProxy()
{
	
}

#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
/*************************************************************************************************/
void FPlanetaryNodeMeshSceneProxy::GetDynamicMeshElements(const TArray<const FSceneView*>& pViews, const FSceneViewFamily& pViewFamily, uint32 pVisibilityMap, FMeshElementCollector& pCollector) const
{
	QUICK_SCOPE_CYCLE_COUNTER( STAT_PlanetaryNodeMeshSceneProxy_GetDynamicMeshElements );

	if
		( this->mAllowRendering == false )
	{
		return;
	}

	for
		( int32 lViewIndex = 0; lViewIndex < pViews.Num(); lViewIndex++ )
	{
		if
			( pVisibilityMap & (1 << lViewIndex) )
		{
			const FSceneView* lView = pViews[ lViewIndex ];

			FMatrix lWorldViewProj = this->GetLocalToWorld() * lView->ViewMatrices.GetViewProjMatrix();

			ENQUEUE_UNIQUE_RENDER_COMMAND_THREEPARAMETER
			(
				FPlanetaryNodeRendererRunner, // Command typename
				FPlanetaryNodeRenderer*, lNodeRenderer, const_cast<FPlanetaryNodeRenderer*>(&this->mNodeRenderer), // First command param
				const FPlanetaryNodeMeshSceneProxy*, lPlanetaryNode, this, // Second command param
				FMatrix, lWorldViewProj, lWorldViewProj, // Third command param
				{
					lNodeRenderer->ExecuteShader( lPlanetaryNode, &lWorldViewProj ); // Delegate to run
				}
			);
		}
	}
}
#else
/*************************************************************************************************/
void FPlanetaryNodeMeshSceneProxy::DrawDynamicElements(FPrimitiveDrawInterface* pPDI, const FSceneView* pView)
{
	QUICK_SCOPE_CYCLE_COUNTER( STAT_PlanetaryNodeMeshSceneProxy_DrawDynamicElements );

	this->WorldViewProj = this->GetLocalToWorld() * pView->ViewMatrices.GetViewProjMatrix();

	// Draw the primitive using the cutom shader (from the rendering thread)
	this->mNodeRenderer.ExecuteShader( this );
}
#endif

/*************************************************************************************************/
FPrimitiveViewRelevance FPlanetaryNodeMeshSceneProxy::GetViewRelevance(const FSceneView* pView)
{
	FPrimitiveViewRelevance lPrimitiveRelevance;
	lPrimitiveRelevance.bDrawRelevance    = this->IsShown( pView );
	lPrimitiveRelevance.bShadowRelevance  = this->IsShadowCast( pView );
	lPrimitiveRelevance.bDynamicRelevance = true;
	this->mMaterialRelevance.SetPrimitiveViewRelevance( lPrimitiveRelevance );

	return lPrimitiveRelevance;
}

/*************************************************************************************************/
bool FPlanetaryNodeMeshSceneProxy::CanBeOccluded() const
{
	return this->mMaterialRelevance.bDisableDepthTest == false;
}

/*************************************************************************************************/
uint32 FPlanetaryNodeMeshSceneProxy::GetMemoryFootprint() const
{
	return ( sizeof(*this) + this->GetAllocatedSize() );
}

/*************************************************************************************************/
uint32 FPlanetaryNodeMeshSceneProxy::GetAllocatedSize() const
{
	return FPrimitiveSceneProxy::GetAllocatedSize();
}
