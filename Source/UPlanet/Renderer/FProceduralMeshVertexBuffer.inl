
/***************************************************************************************************/
FORCEINLINE TArray<FDynamicMeshVertex>& FProceduralMeshVertexBuffer::GetVBO()
{
	return this->mVertices;
}

/***************************************************************************************************/
FORCEINLINE const TArray<FDynamicMeshVertex>& FProceduralMeshVertexBuffer::GetVBO() const
{
	return this->mVertices;
}
