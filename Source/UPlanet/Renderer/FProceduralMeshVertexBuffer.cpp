// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "FProceduralMeshVertexBuffer.h"
#include "Runtime/Launch/Resources/Version.h"

/**************************************************************************************************/
FProceduralMeshVertexBuffer::FProceduralMeshVertexBuffer()
{

}

/**************************************************************************************************/
FProceduralMeshVertexBuffer::~FProceduralMeshVertexBuffer()
{

}

/**************************************************************************************************/
void    FProceduralMeshVertexBuffer::InitRHI()
{
#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 3
	FRHIResourceCreateInfo lCreateInfo;
	this->VertexBufferRHI = RHICreateVertexBuffer( this->mVertices.Num() * sizeof( FDynamicMeshVertex ), BUF_Static, lCreateInfo );
#else
	this->VertexBufferRHI = RHICreateVertexBuffer( this->mVertices.Num() * sizeof( FDynamicMeshVertex ), NULL, BUF_Static );
#endif
	// Copy the vertex data into the vertex buffer.
	void* lVertexBufferData = this->Lock();
	FMemory::Memcpy( lVertexBufferData, this->mVertices.GetData(), this->mVertices.Num() * sizeof( FDynamicMeshVertex ) );
	this->Unlock();
}

/**************************************************************************************************/
void*   FProceduralMeshVertexBuffer::Lock()
{
	return RHILockVertexBuffer( this->VertexBufferRHI, 0, this->mVertices.Num() * sizeof( FDynamicMeshVertex ), RLM_WriteOnly );
}

/**************************************************************************************************/
void    FProceduralMeshVertexBuffer::Unlock()
{
	RHIUnlockVertexBuffer( this->VertexBufferRHI );
}

/**************************************************************************************************/
FString FProceduralMeshVertexBuffer::GetFriendlyName() const
{ 
	return TEXT( "FProceduralMeshVertexBuffer" ); 
}
