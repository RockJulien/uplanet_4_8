// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Components/MeshComponent.h"
#include "../PlanetaryQuadrant.h"
#include "../Shaders/FPlanetaryMeshVertex.h"
#include "PlanetaryNodeMeshComponent.generated.h"

//==========================================
// Define and Constants
//==========================================
/**
 *  The surface map will have 16 squares per node (64 triangles, and (8+1)x(8+1) set of vertices)
 */
#undef  NODE_WIDTH // Just for avoiding conflict when more than one define
#define NODE_WIDTH			8
#define HEIGHT_MAP_SCALE	2

#undef  SURFACE_MAP_WIDTH
#define SURFACE_MAP_WIDTH	(NODE_WIDTH + 1)
#undef  SURFACE_MAP_COUNT
#define SURFACE_MAP_COUNT	(SURFACE_MAP_WIDTH * SURFACE_MAP_WIDTH)

/**
 *  The height map resolution will be twice the resolution of the surface map (16+1)x(16+1)
 */
#define HEIGHT_MAP_WIDTH	(NODE_WIDTH * HEIGHT_MAP_SCALE + 1)
#define HEIGHT_MAP_COUNT	(HEIGHT_MAP_WIDTH * HEIGHT_MAP_WIDTH)

/**
 *  The height map resolution plus a border surrounding it for normal calculations
 */
#define BORDER_MAP_WIDTH	(HEIGHT_MAP_WIDTH + 2)
#define BORDER_MAP_COUNT	(BORDER_MAP_WIDTH * BORDER_MAP_WIDTH)

/**
 * Planetary mesh triangle structure
 */
struct UPLANET_API FPlanetaryMeshTriangle
{

	/**
	 * Planetary triangle vertex 1
	 */
	FPlanetaryMeshVertex Vertex0;

	/**
	 * Planetary triangle vertex 2
	 */
	FPlanetaryMeshVertex Vertex1;

	/**
	 * Planetary triangle vertex 3
	 */
	FPlanetaryMeshVertex Vertex2;
};

//==========================================
// Class definition
//==========================================
/**
 * Planetary node mesh that allows to specify custom triangle mesh geometry
 */
UCLASS(editinlinenew, ClassGroup = Rendering)
class UPLANET_API UPlanetaryNodeMeshComponent : public UMeshComponent, public IInterface_CollisionDataProvider
{
	GENERATED_BODY()
	
private:

	//==========================================
	// Attributes
	//==========================================

	/** 
	 * The set of triangles that compose this planetary node
	 */
	TArray<FPlanetaryMeshTriangle>      mNodeTriangles;

	//==========================================
	// Private Methods
	//==========================================

	// Region USceneComponent interface.

	/**
	 * Compute the planetary node mesh bounding box
	 */
	virtual FBoxSphereBounds CalcBounds(const FTransform& pLocalToWorld) const override;

	// Endregion USceneComponent interface.

public:

	//==========================================
	// Properties
	//==========================================
	/**
	 * Collision description
	 */
	UPROPERTY(BlueprintReadOnly, Category = "Collision")
	class UBodySetup* ModelBodySetup;

	/** 
	 * Heightmap texture reference 
	 */
	UPROPERTY(TextExportTransient)
	class UTexture2D* Heightmap;

	/**
	 * Bumpmap texture reference
	 */
	UPROPERTY(TextExportTransient)
	class UTexture2D* BumpTexture;

	/**
	 * Ground texture reference
	 */
	UPROPERTY(TextExportTransient)
	class UTexture2D* GroundTexture;

	//==========================================
	// Constructors & Destructor
	//==========================================
	UPlanetaryNodeMeshComponent(const FObjectInitializer& pInitializer);
	~UPlanetaryNodeMeshComponent();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Get the planetary node mesh triangle at the specified index
	 * 
	 * @param pIndex The triangle index to get.
	 */
	FORCEINLINE const FPlanetaryMeshTriangle& GetTriangle(int32 pIndex) const
	{
		return this->mNodeTriangles[ pIndex ];
	}

	/**
	 * Get the planetary node mesh triangles
	 */
	FORCEINLINE const TArray<FPlanetaryMeshTriangle>& GetTriangles() const
	{
		return this->mNodeTriangles;
	}

	/** 
	 * Set the geometry to use on this mesh 
	 */
	bool SetMeshTriangles(const TArray<FPlanetaryMeshTriangle>& pOverrideMeshTriangles);

	/**
	 * Refresh the collision description
	 */
	void RefreshBodySetup();

	/**
	 * Update the collision state(s)
	 */
	void UpdateCollision();

	/**
	 * Release the physic state and body
	 */
	void ReleasePhysics();

	// Region Interface_CollisionDataProvider Interface

	/**
	 * Get the triangle mesh data for physics computation
	 */
	virtual bool GetPhysicsTriMeshData(struct FTriMeshCollisionData* pCollisionData, bool pInUseAllTriData) override;

	/**
	 * Flag indicating whether triangle mesh data are created for physics computation
	 */
	virtual bool ContainsPhysicsTriMeshData(bool pInUseAllTriData) const override;

	/**
	 * Flag indicating whether triangle mesh data must have their X component negated.
	 */
	virtual bool WantsNegXTriMesh() override;

	// Endregion Interface_CollisionDataProvider Interface

	// Region UPrimitiveComponent.

	/**
	 * Create the scene proxy instance in charge of rendering the primitive data in parallel to the game thread
	 */
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	/**
	 * Get the collision description object ( TO DO : see if relevant as there is the public property )
	 */
	virtual class UBodySetup* GetBodySetup() override;

	// Endregion UPrimitiveComponent.

	// Region UMeshComponent.

	/**
	 * Get the number of materials for decorating this mesh
	 */
	virtual int32 GetNumMaterials() const override;

	// Endregion UMeshComponent.

};
