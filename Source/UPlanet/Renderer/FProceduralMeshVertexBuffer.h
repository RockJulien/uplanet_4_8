// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "DynamicMeshBuilder.h"

//==========================================
// Define and Constants
//==========================================

//==========================================
// Class definition
//==========================================
/**
 * Procedural mesh vertex buffer class
 */
class UPLANET_API FProceduralMeshVertexBuffer : public FVertexBuffer
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The vertices internal buffer
	 */
	TArray<FDynamicMeshVertex> mVertices;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FProceduralMeshVertexBuffer();
	~FProceduralMeshVertexBuffer();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Initialize the vertex buffer GPU memory
	 */
	virtual void InitRHI() override;

	/**
	 * Lock the vertex buffer and returns its data (for modification is needed)
	 */
	void* Lock();

	/**
	 * Unlock the vertex buffer
	 */
	void  Unlock();

	/**
	 * Get the class human-readable name
	 */
	virtual FString GetFriendlyName() const override;

	/**
	 * Get a reference to the vertex buffer object
	 */
	FORCEINLINE TArray<FDynamicMeshVertex>& GetVBO();

	/**
	 * Get a reference to the vertex buffer object (Const Version)
	 */
	FORCEINLINE const TArray<FDynamicMeshVertex>& GetVBO() const;
};

//==========================================
// Inline
//==========================================
#include "FProceduralMeshVertexBuffer.inl"
