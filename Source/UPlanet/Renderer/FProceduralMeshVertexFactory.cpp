// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "FProceduralMeshVertexFactory.h"

/**************************************************************************************************/
FProceduralMeshVertexFactory::FProceduralMeshVertexFactory()
{

}

/**************************************************************************************************/
FProceduralMeshVertexFactory::~FProceduralMeshVertexFactory()
{

}

/**************************************************************************************************/
void FProceduralMeshVertexFactory::InitializeFactory(const FProceduralMeshVertexBuffer* pVBO)
{
	// Comment out to enable building of light of a level 
	// (but no backing is done for the procedural mesh itself)
	//check( IsInRenderingThread() == false );

	ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER( InitProceduralMeshVertexFactory, // TypeName 
												FProceduralMeshVertexFactory*, pVertexFactory, this, // Param 1
												const FProceduralMeshVertexBuffer*, pVertexBuffer, pVBO, // Param 2
	{
		// Code => Initialize the vertex factory's stream components.
		DataType lNewData;
		lNewData.PositionComponent = STRUCTMEMBER_VERTEXSTREAMCOMPONENT( pVertexBuffer, FDynamicMeshVertex, Position, VET_Float3 );
		lNewData.TextureCoordinates.Add( FVertexStreamComponent( pVertexBuffer, STRUCT_OFFSET( FDynamicMeshVertex, TextureCoordinate ), sizeof( FDynamicMeshVertex ), VET_Float2 ) );
		lNewData.TangentBasisComponents[0] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT( pVertexBuffer, FDynamicMeshVertex, TangentX, VET_PackedNormal );
		lNewData.TangentBasisComponents[1] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT( pVertexBuffer, FDynamicMeshVertex, TangentZ, VET_PackedNormal );
		lNewData.ColorComponent = STRUCTMEMBER_VERTEXSTREAMCOMPONENT( pVertexBuffer, FDynamicMeshVertex, Color, VET_Color );
		pVertexFactory->SetData( lNewData );
	});
}
