// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "LocalVertexFactory.h"
#include "FProceduralMeshVertexBuffer.h"

//==========================================
// Define and Constants
//==========================================

//==========================================
// Class definition
//==========================================
/**
 * Procedural mesh vertex factory class
 */
class UPLANET_API FProceduralMeshVertexFactory : public FLocalVertexFactory
{
public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FProceduralMeshVertexFactory();
	~FProceduralMeshVertexFactory();

	//==========================================
	// Methods
	//==========================================
	/**
	 * Initialize the vertex factory for defining the shader vertex input to stream
	 */
	void InitializeFactory(const FProceduralMeshVertexBuffer* pVBO);
};