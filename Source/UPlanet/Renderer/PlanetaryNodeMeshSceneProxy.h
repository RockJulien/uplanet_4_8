// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "PrimitiveSceneProxy.h"
#include "FProceduralMeshVertexBuffer.h"
#include "FProceduralMeshIndexBuffer.h"
#include "FProceduralMeshVertexFactory.h"
#include "../Shaders/FPlanetaryMeshVertex.h"
#include "../Shaders/PlanetaryNodeRenderer.h"
#include "Runtime/Launch/Resources/Version.h"

//==========================================
// Define and Constants
//==========================================

//==========================================
// Forward declaration
//==========================================
struct FMaterialRelevance;
class UPlanetaryNodeMeshComponent;

//==========================================
// Class definition
//==========================================
/**
 * Planetary scene proxy class
 */
class UPLANET_API FPlanetaryNodeMeshSceneProxy : public FPrimitiveSceneProxy
{
private:

	//==========================================
	// Attributes
	//==========================================
	
	/**
	 * Stores the set of vertices stored in the draw triangle order
	 */
	TArray<FPlanetaryMeshVertex> mVertices;

	/**
	 * Stores the number of triangles to draw.
	 */
	uint32						 mPrimitiveCount;

	/** 
	 * Inform about the primitive relevance regarding to shadow, rendering processes and so on
	 */
	FMaterialRelevance 			 mMaterialRelevance;

	/**
	 * Stores the renderer
	 */
	FPlanetaryNodeRenderer       mNodeRenderer;

	/**
	 * Flag indicating whether the planetary map can be rendered or not.
	 */
	bool						 mAllowRendering;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FPlanetaryNodeMeshSceneProxy(const UPlanetaryNodeMeshComponent* pMeshComponent);
	virtual ~FPlanetaryNodeMeshSceneProxy();

	//==========================================
	// Properties
	//==========================================

	/**
	 * The planetary node mesh world view projection
	 */
	FMatrix WorldViewProj;

	/**
	 * The texture to use to get Bump normals
	 */
	UTexture2D* BumpTexture;

	/**
	 * The texture to use to fill pixels
	 */
	UTexture2D* GroundTexture;

	/**
	 * The texture to use by default
	 */
	TAssetPtr<UTexture2D> DefaultTexture;

	/**
	 * Flag indicating whether the planet node must be rendered as inside the atmosphere (if any atmosphere)
	 */
	bool IsInAtmosphere;

	/**
	 * Flag indicating whether the planet node must be rendered taking into account an atmosphere
	 */
	bool HasAtmosphere;

	/**
	 * The radius of the planetary map
	 */
	float PlanetaryRadius;

	/**
	 * The atmospheric radius if any atmosphere.
	 */
	float AtmosphericRadius;

	/**
	 * The camera location relative to the planet.
	 */
	FVector CameraLocation;

	/**
	 * The sunlight world position
	 */
	FVector SunLightLocation;

	/**
	 * The Wave length property in the atmosphere
	 */
	FVector Wavelength;

	/**
	 * The ESun property of the atmosphere
	 */
	float	Esun;

	/**
	 * The Kr property of the atmosphere
	 */
	float	Kr;

	/**
	 * The Km property of the atmosphere
	 */
	float	Km;

	/**
	 * The G property of the atmosphere
	 */
	float	G;

	//==========================================
	// Methods
	//==========================================

#if ENGINE_MAJOR_VERSION >= 4 && ENGINE_MINOR_VERSION >= 5
	/**
	 * New drawing function for Versions greater or equal than 4.5.
	 * Draw dynamic elements for the supplied scene views by adding the mesh
	 * into the collector or drawing your stuff yourself (should be in a render thread)
	 */
	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& pViews, const FSceneViewFamily& pViewFamily, uint32 pVisibilityMap, FMeshElementCollector& pCollector) const override;
#else
	/**
	 * Old drawing function for Versions smaller than 4.5.
	 * Draw dynamic elements for the supplied scene view using
	 * the primitive drawing device interface handle
	 */
	virtual void DrawDynamicElements(FPrimitiveDrawInterface* pPDI, const FSceneView* pView);
#endif

	/**
	 * Returns the primitive view relevance in the scene informing whether it has to be drawn,
	 * casts shadows and so on.
	 */
	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* pView);

	/**
	 * Informs whether the primitive can be occluded or not.
	 */
	virtual bool CanBeOccluded() const override;

	/**
	 * Returns the primitive memory footprint
	 */
	virtual uint32 GetMemoryFootprint() const;

	/**
	 * Returns the primitive allocated size in bytes
	 */
	uint32 GetAllocatedSize() const;

	/**
	 * Returns the primitive count (that is the number of triangles to draw)
	 */
	FORCEINLINE uint32 PrimitiveCount() const
	{
		return this->mPrimitiveCount;
	}

	/**
	 * Returns the set of vertices stored in the draw triangle order (that is, no need to draw with indices)
	 */
	FORCEINLINE const TArray<FPlanetaryMeshVertex>& Vertices() const
	{
		return this->mVertices;
	}
};