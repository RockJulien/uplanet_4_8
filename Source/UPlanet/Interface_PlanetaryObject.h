// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Interface_PlanetaryObject.generated.h"

//==========================================
// Interface definition
//==========================================
/**
 * Base interface for all planetary object involved in the planet generation (UObject version castable)
 */
UINTERFACE(meta=(CannotImplementInterfaceInBlueprint))
class UPLANET_API UInterface_PlanetaryObject : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

//==========================================
// Interface definition
//==========================================
/**
 * Base interface for all planetary object involved in the planet generation
 */
class UPLANET_API IInterface_PlanetaryObject
{
	GENERATED_IINTERFACE_BODY()

public:

	//==========================================
	// Methods
	//==========================================

	/**
	 * Update the planetary object
	 */
	virtual void Update() = 0;

	/**
	 * Draw the planetary object
	 */
	virtual void Draw() = 0;

};
