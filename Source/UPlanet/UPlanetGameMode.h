// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "GameFramework/GameMode.h"
#include "UPlanetGameMode.generated.h"

//==========================================
// Class definition
//==========================================
/**
 * Planet game mode
 */
UCLASS()
class UPLANET_API AUPlanetGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:

	//==========================================
	// Constructor & Destructor
	//==========================================
	AUPlanetGameMode(const FObjectInitializer& pInitializer);

	//==========================================
	// Methods
	//==========================================
};
