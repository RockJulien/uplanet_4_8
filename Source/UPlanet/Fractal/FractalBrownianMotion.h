// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Noise/Noise.h"

#undef MAX_OCTAVES
#define MAX_OCTAVES 128

/**
 * 
 */
class UPLANET_API FractalBrownianMotion : public Noise
{
private:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * The fractal roughness
	 */
	float mRoughness;

	/**
	 * The fractal lacunarity
	 */
	float mLacunarity;

	/**
	 * The set of exponents (up to Max Octaves
	 */
	float mExponents[ MAX_OCTAVES ];

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	FractalBrownianMotion();
	FractalBrownianMotion(uint32 pDimensions, uint32 pSeed, float pRoughness, float pLacunarity);

	//==========================================
	// Methods
	//==========================================

	/**
	* Modify the fractal generator with the supplied parameters.
	*/
	void ModifyFractal(uint32 pDimensions, uint32 pSeed, float pRoughness, float pLacunarity);

	/**
	 * Get the brownian motion fractal sample at the position.
	 */
	float GetBrownianMotion(const FVector& pPosition, float pOctaves);

	/**
	 * Get a turbulence fractal sample at the position.
	 */
	float GetTurbulence(const FVector& pPosition, float pOctaves);

	/**
	 * Get a multi fractal sample at the position.
	 */
	float GetMultifractal(const FVector& pPosition, float pOctaves, float pOffset);

	/**
	 * Get a Heterogeneous fractal sample at the position.
	 */
	float GetHeterofractal(const FVector& pPosition, float pOctaves, float pOffset);

	/**
	 * Get a hybrid multi fractal sample at the position.
	 */
	float GetHybridMultifractal(const FVector& pPosition, float pOctaves, float pOffset, float pGain);

	/**
	 * Get a ridged multi fractal sample at the position.
	 */
	float GetRidgedMultifractal(const FVector& pPosition, float pOctaves, float pOffset, float pThreshold);

	/**
	 * Get the fractal generator lacunarity
	 */
	FORCEINLINE float Lacunarity() const
	{
		return this->mLacunarity;
	}

	/**
	 * Get the exponent at the supplied index
	 */
	FORCEINLINE float Exponents(uint8 pIndex) const
	{
		if 
			( pIndex >= MAX_OCTAVES )
		{
			return 0.0f;
		}

		return this->mExponents[ pIndex ];
	}
};
