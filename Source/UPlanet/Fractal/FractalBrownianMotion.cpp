// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "FractalBrownianMotion.h"

/********************************************************************************************************/
FractalBrownianMotion::FractalBrownianMotion()
{
	this->ModifyFractal( 3, -1, 2.0f, 5.0f );
}

/********************************************************************************************************/
FractalBrownianMotion::FractalBrownianMotion(uint32 pDimensions, uint32 pSeed, float pRoughness, float pLacunarity)
{
	this->ModifyFractal( pDimensions, pSeed, pRoughness, pLacunarity );
}

/********************************************************************************************************/
void  FractalBrownianMotion::ModifyFractal(uint32 pDimensions, uint32 pSeed, float pRoughness, float pLacunarity)
{
	this->ModifyNoise( pDimensions, pSeed );
	this->mRoughness  = pRoughness;
	this->mLacunarity = pLacunarity;
	float lFactor = 1.0f;
	for 
		( uint16 lCurrOctave = 0; lCurrOctave < MAX_OCTAVES; lCurrOctave++ )
	{
		this->mExponents[ lCurrOctave ] = FMath::Pow( lFactor, -this->mRoughness );
		lFactor *= this->mLacunarity;
	}
}

/********************************************************************************************************/
float FractalBrownianMotion::GetBrownianMotion(const FVector& pPosition, float pOctaves)
{
	float lSample = 0;
	FVector lTempPosition = pPosition;

	// Inner loop of spectral construction, where the fractal is built
	uint16 lCurrOctave;
	for 
		( lCurrOctave = 0; lCurrOctave < pOctaves; lCurrOctave++ )
	{
		lSample += this->GetNoise( lTempPosition ) * this->mExponents[ lCurrOctave ];
		for 
			( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
		{
			lTempPosition[ lCurrDimension ] *= this->mLacunarity;
		}
	}

	// Take care of remainder in Octaves
	pOctaves -= StaticCast<int32>( pOctaves );
	if 
		( pOctaves > DELTA )
	{
		lSample += pOctaves * this->GetNoise( lTempPosition ) * this->mExponents[ lCurrOctave ];
	}

	return FMath::Clamp( lSample, -0.99999f, 0.99999f );
}

/********************************************************************************************************/
float FractalBrownianMotion::GetTurbulence(const FVector& pPosition, float pOctaves)
{
	float lSample = 0;
	FVector lTempPosition = pPosition;
	
	// Inner loop of spectral construction, where the fractal is built
	uint16 lCurrOctave;
	for 
		( lCurrOctave = 0; lCurrOctave < pOctaves; lCurrOctave++ )
	{
		lSample += FMath::Abs( this->GetNoise( lTempPosition ) ) * this->mExponents[ lCurrOctave ];
		for 
			( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
		{
			lTempPosition[ lCurrDimension ] *= this->mLacunarity;
		}
	}

	// Take care of remainder in Octaves
	pOctaves -= StaticCast<int32>( pOctaves );
	if 
		( pOctaves > DELTA)
	{
		lSample += pOctaves * FMath::Abs( this->GetNoise( lTempPosition ) * this->mExponents[ lCurrOctave ] );
	}

	return FMath::Clamp( lSample, -0.99999f, 0.99999f );
}

/********************************************************************************************************/
float FractalBrownianMotion::GetMultifractal(const FVector& pPosition, float pOctaves, float pOffset)
{
	float lSample = 0;
	FVector4 lTempPosition = pPosition;

	// Inner loop of spectral construction, where the fractal is built
	uint16 lCurrOctave;
	for
		( lCurrOctave = 0; lCurrOctave < pOctaves; lCurrOctave++ )
	{
		lSample *= this->GetNoise( lTempPosition ) * this->mExponents[ lCurrOctave ] + pOffset;
		for
			( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
		{
			lTempPosition[ lCurrDimension ] *= this->mLacunarity;
		}
	}

	// Take care of remainder in Octaves
	pOctaves -= StaticCast<int32>( pOctaves );
	if
		( pOctaves > DELTA )
	{
		lSample *= pOctaves * ( this->GetNoise( lTempPosition ) * this->mExponents[ lCurrOctave ] + pOffset );
	}

	return FMath::Clamp( lSample, -0.99999f, 0.99999f );
}

/********************************************************************************************************/
float FractalBrownianMotion::GetHeterofractal(const FVector& pPosition, float pOctaves, float pOffset)
{
	float lSample = this->GetNoise( pPosition ) + pOffset;
	FVector lTempPosition = pPosition;

	// Inner loop of spectral construction, where the fractal is built
	uint16 lCurrOctave;
	for
		( lCurrOctave = 1; lCurrOctave < pOctaves; lCurrOctave++ )
	{
		lSample += ( this->GetNoise( lTempPosition ) + pOffset ) * this->mExponents[ lCurrOctave ] * lSample;
		for
			( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
		{
			lTempPosition[ lCurrDimension ] *= this->mLacunarity;
		}
	}

	// Take care of remainder in Octaves
	pOctaves -= StaticCast<int32>( pOctaves );
	if
		( pOctaves > DELTA )
	{
		lSample += pOctaves * ( this->GetNoise( lTempPosition ) + pOffset ) * this->mExponents[ lCurrOctave ] * lSample;
	}

	return FMath::Clamp( lSample, -0.99999f, 0.99999f );
}

/********************************************************************************************************/
float FractalBrownianMotion::GetHybridMultifractal(const FVector& pPosition, float pOctaves, float pOffset, float pGain)
{
	float lSample = ( this->GetNoise( pPosition ) + pOffset ) * this->mExponents[ 0 ];
	float lWeight = lSample;
	FVector ltempPosition = pPosition;

	// Inner loop of spectral construction, where the fractal is built
	uint16 lCurrOctave;
	for 
		( lCurrOctave = 1; lCurrOctave < pOctaves; lCurrOctave++ )
	{
		if 
			( lWeight > 1.0f )
		{
			lWeight = 1.0f;
		}

		float lSignal = ( this->GetNoise( ltempPosition ) + pOffset ) * this->mExponents[ lCurrOctave ];
		lSample += lWeight * lSignal;
		lWeight *= pGain * lSignal;
		for 
			( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
		{
			ltempPosition[ lCurrDimension ] *= this->mLacunarity;
		}
	}

	// Take care of remainder in fOctaves
	pOctaves -= StaticCast<int32>( pOctaves );
	if 
		( pOctaves > DELTA )
	{
		if
			( lWeight > 1.0f )
		{
			lWeight = 1.0f;
		}

		float lSignal = ( this->GetNoise( ltempPosition ) + pOffset ) * this->mExponents[ lCurrOctave ];
		lSample += pOctaves * lWeight * lSignal;
	}

	return FMath::Clamp( lSample, -0.99999f, 0.99999f );
}

/********************************************************************************************************/
float FractalBrownianMotion::GetRidgedMultifractal(const FVector& pPosition, float pOctaves, float pOffset, float pThreshold)
{
	FVector lTempPosition = pPosition;

	// Inner loop of spectral construction, where the fractal is built
	float lTempSample = this->GetNoise( lTempPosition );
	float lSign   = FMath::Sign( lTempSample );
	float lSample = lSign * FMath::Square( lTempSample );
	for 
		( uint8 lCurrOctave = 1; lCurrOctave < 12; lCurrOctave++ )
	{
		for 
			( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
		{
			lTempPosition[ lCurrDimension ] *= this->mLacunarity;
		}

		lSample += this->GetNoise( lTempPosition ) * this->mExponents[ lCurrOctave ];
	}

	while 
		( FMath::Abs( lSample ) > 1.0f )
	{
		if 
			( lSample > 0.0f )
		{
			lSample = 2.0f - lSample;
		}
		else
		{
			lSample = -2.0f - lSample;
		}
	}

	if 
		( lSample <= 0.0f )
	{
		lSample = -FMath::Pow( -lSample, 0.7f );
	}
	else
	{
		lSample = FMath::Pow( lSample, 1 + this->GetNoise( lTempPosition ) * lSample );
	}

	return lSample;
}

