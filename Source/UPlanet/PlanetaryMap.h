// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Components/SceneComponent.h"
#include "DataStructures/PlanetaryQuadTree.h"
#include "PlanetaryFace.h"
#include "Shaders/SkydomeVertex.h"
#include "PlanetaryTreeCoordinates.h"
#include "PlanetaryMap.generated.h"

/**
 * Planetary styles
 */
UENUM()
enum PlanetaryStyle
{
	/**
	 * Planetary earth mode
	 */
	eEarth = 0,

	/**
	* Planetary moon mode
	*/
	eMoon = 1,

	/**
	 * Planetary mars mode
	 */
	eMars = 2
};

//==========================================
// Forward declaration
//==========================================
class FPlanetFactoryService;

//==========================================
// Class definition
//==========================================
/**
 * Planetary map owning the overall planet mesh made of 6 Quad tree
 * that is one per face (like cube faces) but is an Actor of the scene
 * that is, spawnable into the scene.
 */
UCLASS(editinlinenew, meta=(BlueprintSpawnableComponent), ClassGroup=Rendering)
class UPLANET_API UPlanetaryMap : public UMeshComponent
{
	GENERATED_BODY()
	
private:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * The planet factory service
	 */
	FPlanetFactoryService*  mFactoryService;

	/**
	 * The current number of splits so far
	 */
	uint16					mCurrentSplitCount;

	/**
	 * The planetary skydome vertices
	 */
	TArray<FSkydomeVertex>	mSkydomeVertices;

	/**
	 * The planetary skydome indices
	 */
	TArray<int32>			mSkydomeIndices;

	/**
	 * The planetary trees for each face
	 */
	FPlanetaryQuadTree*		mFaceTrees[ 6 ];

	/**
	 * The cache for face ordering regarding to the distance to camera.
	 */
	PlanetaryFace::PlanetaryFace mOrderedFaces[ 6 ];

	/**
	 * The corresponding planetary coordinate for the camera position
	 */
	PlanetaryTreeCoordinates mCameraCoordinates;

	/**
	 * The relative camera position regarding to the planet
	 */
	FVector					mRelativeCameraPosition;

	/**
	 * The relative camera orientation regarding to the planet
	 */
	FQuat					mRelativeCameraOrientation;

	/**
	 * The current distance to the horizon
	 */
	float					mHorizonDistance;

	/**
	 * The planetary map maximal depth
	 */
	int16					mPlanetaryMaxDepth;

	/**
	 * The planetary node count
	 */
	int32					mPlanetaryNodeCount;

	/**
	 * Flag indicating whether the planetary map can be rendered or not.
	 */
	bool					mAllowRendering;

	//==========================================
	// Private Methods
	//==========================================
	/**
	 * Initialize the planet map
	 */
	void Initialize();

	/**
	 * Generates the skydome geometry returning the vertices and indices
	 */
	void GenerateSkydomeGeometry(TArray<FSkydomeVertex>& pVertices, TArray<int32>& pIndices, float pRadius, uint16 pSliceCount, uint16 pStackCount);

	// Region USceneComponent interface.

	/**
	 * Compute the planetary map bounding box
	 */
	virtual FBoxSphereBounds CalcBounds(const FTransform& pLocalToWorld) const override;

	// Endregion USceneComponent interface.

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	UPlanetaryMap(const FObjectInitializer& pInitializer);
	~UPlanetaryMap();

	//==========================================
	// Properties
	//==========================================

	/**
	 * The texture to use by default
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	UTexture2D* DefaultTexture;

	/**
	 * The planetary style mode
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	TEnumAsByte<PlanetaryStyle> PlanetaryMode;

	/**
	 * The maximum number of splits allowed
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	uint16 MaxSplitCount;

	/**
	 * The split factor to adjust for affecting split priority (Linear effect)
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	float SplitFactor;

	/**
	 * The split power to adjust for affecting split priority (Exponetial effect)
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	float SplitPower;

	/**
	 * The radius of the planetary map
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	float PlanetaryRadius;

	/**
	 * The maximal height above terrain allowed for the planetary map
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	float PlanetaryMaxHeight;

	/**
	 * The atmospheric radius if any atmosphere.
	 */
	UPROPERTY(EditAnywhere, Category = Planet)
	float AtmosphericRadius;

	/**
	 * The Wave length property in the atmosphere
	 */
	UPROPERTY(EditAnywhere, Category = Atmosphere)
	FVector Wavelength;

	/**
	 * The ESun property of the atmosphere
	 */
	UPROPERTY(EditAnywhere, Category = Atmosphere)
	float	Esun;

	/**
	 * The Kr property of the atmosphere
	 */
	UPROPERTY(EditAnywhere, Category = Atmosphere)
	float	Kr;

	/**
	 * The Km property of the atmosphere
	 */
	UPROPERTY(EditAnywhere, Category = Atmosphere)
	float	Km;

	/**
	 * The G property of the atmosphere
	 */
	UPROPERTY(EditAnywhere, Category = Atmosphere)
	float	G;

	//==========================================
	// Methods
	//==========================================

	/**
	 * Update the planet surface
	 */
	void Update(const class UCameraComponent* pCamera);

	/**
	 * Flag indicating whether the hierarchy can be splitted any further
	 */
	bool CanSplit();

	/**
	 * Releases the planetary map resources
	 */
	void Release();

	/**
	 * Allow to render the planet map or not (to use in Begin/EndingPlay events of the actor)
	 */
	void AllowRendering(bool pCanRender);

	// Region UPrimitiveComponent.

	/**
	 * Begin destroy event
	 */
	virtual void BeginDestroy() override;

	/**
	 * Create the scene proxy instance in charge of rendering the primitive data in parallel to the game thread
	 */
	virtual FPrimitiveSceneProxy* CreateSceneProxy() override;

	// Endregion UPrimitiveComponent.

	// Region UActorComponent.

	/**
	 * Called when a component is registered, after Scene is set, but before CreateRenderState_Concurrent or CreatePhysicsState are called.
	 */
	virtual void OnRegister() override;

	/**
	 * Delegate called on Planetary map property changes
	 */
	virtual void PostEditChangeProperty(struct FPropertyChangedEvent& pEventArgs) override;

	// Endregion UActorComponent.

	// Region UMeshComponent

	/**
	 * Get the number of materials for decorating this mesh
	 */
	virtual int32 GetNumMaterials() const override;

	// Endregion UMeshComponent

	/**
	 * Get the height above the ground at the supplied position
	 */
	float GetHeightAboveGround(const FVector& pPosition);

	/**
	 * Get the planet factory service
	 */
	FORCEINLINE const FPlanetFactoryService* FactoryService() const
	{
		return this->mFactoryService;
	}

	/**
	 * Get the current split count in the overall planet hierarchy
	 */
	FORCEINLINE uint16 CurrentSplitCount() const
	{
		return this->mCurrentSplitCount;
	}

	/**
	 * Increment the node count by one
	 */
	FORCEINLINE void IncrementNodeCount()
	{
		this->mPlanetaryNodeCount++;
	}

	/**
	 * Flag indicating whether the planetary map can be rendered or not.
	 */
	FORCEINLINE bool AllowRendering() const
	{
		return this->mAllowRendering;
	}

	/**
	 * Get the camera view Frustum
	 */
	FORCEINLINE FConvexVolume Frustum() const
	{
		FSceneViewProjectionData lProjectionData;
		ULocalPlayer* lPlayer = GetWorld()->GetFirstLocalPlayerFromController();
		lPlayer->GetProjectionData( lPlayer->ViewportClient->Viewport, eSSP_FULL, lProjectionData );
		const FMatrix lViewProjectionMatrix = lProjectionData.ViewRotationMatrix * lProjectionData.ProjectionMatrix;

		// Use the view projection for computing the Frustum
		FConvexVolume lFrustum;
		GetViewFrustumBounds( lFrustum, lViewProjectionMatrix, false );

		return lFrustum;
	}

	/**
	 * Get the relative camera location regarding to the planet
	 */
	FORCEINLINE FVector CameraLocation() const
	{
		return this->mRelativeCameraPosition;
	}

	/**
	 * Get the relative camera orientation regarding to the planet
	 */
	FORCEINLINE FQuat   CameraOrientation() const
	{
		return this->mRelativeCameraOrientation;
	}

	/**
	 * Get the planetary coordinates version of the camera location
	 */
	FORCEINLINE PlanetaryTreeCoordinates CameraCoordinates() const
	{
		return this->mCameraCoordinates;
	}

	/**
	 * Get the sunlight position
	 */
	FORCEINLINE FVector SunLightPosition() const
	{
		TArray<const ULightComponent*> lPlanetRelevantLights;
		this->GetScene()->GetRelevantLights( const_cast<UPlanetaryMap*>( this ), &lPlanetRelevantLights );
		for
			( int32 lCurrLight = 0; lCurrLight < lPlanetRelevantLights.Num(); lCurrLight++ )
		{
			const UDirectionalLightComponent* lCandidate = Cast<UDirectionalLightComponent>( lPlanetRelevantLights[ lCurrLight ] );
			if 
				( lCandidate != NULL )
			{
				// Must have one sun light, so return straight if matches
				return lCandidate->GetComponentLocation();
			}
		}

		return FVector( -6000.0f, 0.0f, 0.0f ); // Far away on X
	}

	/**
	 * Get the flag indicating whether the planet has an atmosphere or not
	 */
	FORCEINLINE bool    HasAtmosphere() const
	{
		return false;// this->AtmosphericRadius > 0.0f;
	}

	/**
	 * Get the flag indicating whether the planet has an atmosphere or not
	 */
	FORCEINLINE bool    IsInAtmosphere() const
	{
		// If the camera coordinates are in the atmosphere
		return this->mCameraCoordinates.GetSurfaceHeight() < this->AtmosphericRadius;
	}
	
	/**
	 * Get the planetary horizon distance if any horizon
	 */
	FORCEINLINE float   HorizonDistance() const
	{
		return this->mHorizonDistance;
	}

	/**
	 * Get the planetary map maximal depth
	 */
	FORCEINLINE int16   MaxDepth() const
	{
		return this->mPlanetaryMaxDepth;
	}

	/**
	 * Get the planetary node count
	 */
	FORCEINLINE int32   NodeCount() const
	{
		return this->mPlanetaryNodeCount;
	}

	/**
	 * Get the skydome vertices
	 */
	FORCEINLINE const TArray<FSkydomeVertex>& GetVertices() const
	{
		return this->mSkydomeVertices;
	}

	/**
	 * Get the skydome indices
	 */
	FORCEINLINE const TArray<int32>& GetIndices() const
	{
		return this->mSkydomeIndices;
	}

	/**
	 * Set the new planetary map maximal depth
	 */
	FORCEINLINE void    SetMaxDepth(int16 pNewDepth)
	{
		this->mPlanetaryMaxDepth = FMath::Max( this->mPlanetaryMaxDepth, pNewDepth );
	}

};
