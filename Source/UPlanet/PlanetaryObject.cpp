// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryObject.h"

/**************************************************************************************/
UPlanetaryObject::UPlanetaryObject() :
Super()
{
	this->SetFlags( RF_RootSet );
}

/**************************************************************************************/
UPlanetaryObject::UPlanetaryObject(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
	this->SetFlags( RF_RootSet );
}

/**************************************************************************************/
void UPlanetaryObject::Update() 
{
	// Nothing to do
}

/**************************************************************************************/
void UPlanetaryObject::Draw()
{
	// Nothing to do
}


