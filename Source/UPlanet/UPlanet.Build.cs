// Fill out your copyright notice in the Description page of Project Settings.

//==========================================
// Includes
//==========================================
using UnrealBuildTool;

//==========================================
// Class definition
//==========================================
public class UPlanet : ModuleRules
{
    //==========================================
    // Constructors
    //==========================================
	public UPlanet(TargetInfo Target)
	{
        /**
         * Public modules to link with
         */
		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });

        /**
         * Private modules to link with
         */
        PrivateDependencyModuleNames.AddRange(new string[] { "RHI", "RenderCore", "ShaderCore" });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");
		// if ((Target.Platform == UnrealTargetPlatform.Win32) || (Target.Platform == UnrealTargetPlatform.Win64))
		// {
		//		if (UEBuildConfiguration.bCompileSteamOSS == true)
		//		{
		//			DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
		//		}
		// }
	}
}
