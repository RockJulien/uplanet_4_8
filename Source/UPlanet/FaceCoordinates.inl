
/***************************************************************************************/
FORCEINLINE FVector FaceCoordinates::GetPosition(PlanetaryFace::PlanetaryFace pFace, const float& pSize) const
{
	return PlanetaryHelper::GetPlanetaryPositionFromCoordinates( pFace, this->mXCoordinate, this->mYCoordinate, pSize );
}

/***************************************************************************************/
FORCEINLINE FVector2D FaceCoordinates::AsVector2D() const
{
	return FVector2D( this->mXCoordinate, this->mYCoordinate );
}

/***************************************************************************************/
FORCEINLINE float   FaceCoordinates::GetXCoordinate() const
{
	return this->mXCoordinate;
}

/***************************************************************************************/
FORCEINLINE float   FaceCoordinates::GetYCoordinate() const
{
	return this->mYCoordinate;
}
