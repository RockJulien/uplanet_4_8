// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace PlanetaryCoordinateState
{

	/**
	 * Enumeration offering the available planetary coordinates states
	 */
	enum PlanetaryCoordinateState
	{
		/**
		 * The Planetary coordinates have no state
		 */
		eNone = 0,

		/**
		 * The Planetary coordinates are borrowed
		 */
		 eBorrowed
	};

}
