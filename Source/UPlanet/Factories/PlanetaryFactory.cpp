// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryFactory.h"

/********************************************************************************************/
UPlanetaryFactory::UPlanetaryFactory(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
	this->mName		   = this->GetClass()->GetName();
	this->mDescription = "Factory service";
}

/********************************************************************************************/
UPlanetaryFactory::~UPlanetaryFactory()
{
	
}

/********************************************************************************************/
void UPlanetaryFactory::Activate()
{
	this->mIsActive = true;
}

/********************************************************************************************/
void UPlanetaryFactory::Deactivate()
{
	this->mIsActive = false;
}

/********************************************************************************************/
const FString& UPlanetaryFactory::Name() const
{
	return this->mName;
}

/********************************************************************************************/
const FString& UPlanetaryFactory::Description() const
{
	return this->mDescription;
}

/********************************************************************************************/
bool UPlanetaryFactory::IsActive() const
{
	return this->mIsActive;
}

/********************************************************************************************/
void UPlanetaryFactory::Release()
{
	this->Deactivate();
}

/********************************************************************************************/
bool UPlanetaryFactory::AffectsNode(const FPlanetaryQuadTreeNode* pNode) const
{ 
	return true; 
}

/********************************************************************************************/
void UPlanetaryFactory::BuildNode(FPlanetaryQuadTreeNode* pNode)
{
	// Nothing to do
}

/********************************************************************************************/
void UPlanetaryFactory::DestroyNode(const FPlanetaryQuadTreeNode* pNode)
{
	// Nothing to do
}



