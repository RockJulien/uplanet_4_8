// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "../Services/Interface_PluginService.h"
#include "../PlanetaryObject.h"
#include "PlanetaryFactory.generated.h"

/**
 * Forward declarations
 */
class FPlanetaryQuadTreeNode;

/**
 * Base Planetary Factory class
 */
UCLASS()
class UPLANET_API UPlanetaryFactory : public UPlanetaryObject, public IInterface_PluginService
{
	GENERATED_BODY()

protected:

	//==========================================
	// Attributes
	//==========================================
	
	/**
	 * Stores the factory priority
	 */
	int32 mFactoryPriority;

	/**
	 * Stores the factory service name
	 */
	FString mName;

	/**
	 * Stores the factory service description
	 */
	FString mDescription;

	/**
	 * Stores a flag indicating whether the service is active or not
	 */
	bool mIsActive;

	//==========================================
	// Private Methods
	//==========================================

	/**
	 * Set the service as activated
	 */
	void Activate();

	/**
	 * Set the service as deactivated
	 */
	void Deactivate();

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	UPlanetaryFactory(const FObjectInitializer& pInitializer);
	virtual ~UPlanetaryFactory();

	//==========================================
	// Methods
	//==========================================

	// #region IPluginService

	/**
	 * Gets the service name
	 */
	const FString& Name() const;

	/**
	 * Gets the service description
	 */
	const FString& Description() const;

	/**
	 * Gets the flag indicating whether the service is active or not.
	 */
	bool IsActive() const;

	/**
	 * Releases resources
	 */
	virtual void Release();

	// #endregion IPluginService

	/**
	 * Checks wether the factory affets the node supplied
	 */
	virtual bool AffectsNode(const FPlanetaryQuadTreeNode* pNode) const;

	/**
	 * Builds the node supplied
	 */
	virtual void BuildNode(FPlanetaryQuadTreeNode* pNode);

	/**
	 * Releases resources involved in the factory process for that node
	 */
	virtual void DestroyNode(const FPlanetaryQuadTreeNode* pNode);

	/**
	 * Gets the factory priority
	 */
	FORCEINLINE int32 Priority() const
	{
		return this->mFactoryPriority;
	}

	/**
	 * Sets the factory priority
	 */
	FORCEINLINE void SetPriority(int32 pPriority)
	{
		this->mFactoryPriority = pPriority;
	}
};
