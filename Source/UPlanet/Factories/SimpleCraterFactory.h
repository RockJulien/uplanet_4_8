// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "PlanetaryFactory.h"
#include "SimpleCraterFactory.generated.h"

//==========================================
// Class definition
//==========================================
/**
 * Crater height map factory for the terrain geometry
 */
UCLASS()
class UPLANET_API USimpleCraterFactory : public UPlanetaryFactory
{
	GENERATED_BODY()
	
	//==========================================
	// Properties
	//==========================================
	/**
	 * The fractal seed used by this object
	 */
	UPROPERTY()
	int32 FractalSeed;

	/**
	 * The fractal seed used by this object
	 */
	UPROPERTY()
	int32 TopLevel;

	/**
	 * The fractal seed used by this object
	 */
	UPROPERTY()
	int32 BottomLevel;

	/**
	 * The fractal seed used by this object
	 */
	UPROPERTY()
	int32 CraterCountPerLevel;

private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * The fractal seed used by this object
	 */
	float mCraterScale[50];

public:

	//==========================================
	// Constructor & Destructor
	//==========================================
	USimpleCraterFactory(const FObjectInitializer& pInitializer);
	virtual ~USimpleCraterFactory();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Build the planetary node
	 */
	virtual void BuildNode(FPlanetaryQuadTreeNode* pNode) override;

};
