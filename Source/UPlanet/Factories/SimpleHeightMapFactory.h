// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "PlanetaryFactory.h"
#include "../Fractal/FractalBrownianMotion.h"
#include "SimpleHeightMapFactory.generated.h"

//==========================================
// Class definition
//==========================================
/**
 * Simple height map factory for the terrain geometry
 */
UCLASS()
class UPLANET_API USimpleHeightMapFactory : public UPlanetaryFactory
{
	GENERATED_BODY()
	
public:

	//==========================================
	// Properties
	//==========================================
	/**
	 * The fractal seed used by this object
	 */
	UPROPERTY()
	int32 FractalSeed;

	/**
	 * The octaves used by the fractal process
	 */
	UPROPERTY()
	float Octaves;

	/**
	 * The roughness used by the fractal process
	 */
	UPROPERTY()
	float Roughness;

protected:

	//==========================================
	// Attributes
	//==========================================
	
	/**
	 * The fractal generator
	 */
	FractalBrownianMotion* mFractalGenerator;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	USimpleHeightMapFactory(const FObjectInitializer& pInitializer);
	virtual ~USimpleHeightMapFactory();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Build the planetary node
	 */
	virtual void BuildNode(FPlanetaryQuadTreeNode* pNode) override;

	/**
	 * Releases resources
	 */
	virtual void Release() override;
};
