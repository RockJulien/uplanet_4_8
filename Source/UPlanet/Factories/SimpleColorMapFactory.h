// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "PlanetaryFactory.h"
#include "SimpleColorMapFactory.generated.h"

/**
 * Planetary simple color modes
 */
enum ColorStyle
{
	/**
	 * Planetary earth color mode
	 */
	eEarthColor = 0,

	/**
	 * Planetary lunar color mode
	 */
	eLunarColor = 1,

	/**
	 * Planetary mars color mode
	 */
	eMarsColor  = 2
};

//==========================================
// Class definition
//==========================================
/**
 * Planetary simple color texture filler factory for the terrain texturing/coloring
 */
UCLASS()
class UPLANET_API USimpleColorMapFactory : public UPlanetaryFactory
{
	GENERATED_BODY()
	
private:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * The panel of ground colors
	 */
	TArray<FColor> mColors;

	/**
	 * Clamp a float value as a byte value
	 */
	FORCEINLINE uint8 ColorClamp(float pToClamp) 
	{ 
		return StaticCast<uint8>( pToClamp < 0 ? 0 : pToClamp > 255 ? 255 : pToClamp );
	}

	/**
	 * Scale a color using the supplied factor
	 */
	FORCEINLINE FColor ScaleColor(FColor pColor, float pFactor)
	{
		return FColor( this->ColorClamp( pColor.R * pFactor ), 
					   this->ColorClamp( pColor.G * pFactor ), 
					   this->ColorClamp( pColor.B * pFactor ), 
					   this->ColorClamp( pColor.A * pFactor ) );
	}

public:

	//==========================================
	// Constructor & Destructor
	//==========================================
	USimpleColorMapFactory(const FObjectInitializer& pInitializer);
	virtual ~USimpleColorMapFactory();

	//==========================================
	// Methods
	//==========================================
	
	/**
	 * Build the planetary node
	 */
	virtual void BuildNode(FPlanetaryQuadTreeNode* pNode) override;

	/**
	 * Initialize the color map used to fill the texture afterward
	 */
	void InitializeColorMap(ColorStyle pMode);

	/**
	 * Releases resources
	 */
	virtual void Release() override;
};
