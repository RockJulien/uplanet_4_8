// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "../PlanetaryMap.h"
#include "../DataStructures/PlanetaryQuadTreeNode.h"
#include "MixedHeightMapFactory.h"

/*********************************************************************************************/
UMixedHeightMapFactory::UMixedHeightMapFactory(const FObjectInitializer& pInitializer) :
Super(pInitializer), mTextureSize(MAX_OCTAVES), mIsDisposed(false)
{
	this->FractalSeed = 865366;
	this->Octaves	  = 8.5;
	this->Roughness   = 0.5;
	this->mFractalGenerator->ModifyFractal( 3, this->FractalSeed, 1.0f - this->Roughness, 2.0f );

	for 
		( int8 lFace = 0; lFace < 6; lFace++ )
	{
		this->mBuffers[ lFace ] = UTexture2D::CreateTransient( this->mTextureSize, this->mTextureSize, EPixelFormat::PF_FloatRGBA );
		this->mBuffers[lFace]->CompressionSettings = TextureCompressionSettings::TC_VectorDisplacementmap;
		this->mBuffers[lFace]->SRGB = 0;
		this->mBuffers[lFace]->AddToRoot();
		this->mBuffers[lFace]->UpdateResource();
		float* pIndex = StaticCast<float*>( this->mBuffers[ lFace ]->PlatformData->Mips[ 0 ].BulkData.Lock( LOCK_READ_WRITE ) );
		for 
			( int32 y = 0; y < this->mTextureSize; y++ )
		{
			for 
				( int32 x = 0; x < this->mTextureSize; x++ )
			{
				PlanetaryTreeCoordinates lCoordinate( (PlanetaryFace::PlanetaryFace)lFace, x / (this->mTextureSize - 1), y / (this->mTextureSize - 1));
				FVector lDirection = lCoordinate.GetDirection();

				lDirection *= 2;
				float lValue = this->mFractalGenerator->GetNoise( lDirection ) + -0.1f;
				for 
					( int8 i = 1; i < 5; i++ )
				{
					lDirection *= this->mFractalGenerator->Lacunarity();
					lValue += (this->mFractalGenerator->GetNoise( lDirection ) + -0.1f) * this->mFractalGenerator->Exponents( i );
				}

				while 
					( FMath::Abs( lValue ) > 1.0f )
				{
					if 
						( lValue > 0.0f )
					{
						lValue = 2 - lValue;
					}
					else
					{
						lValue = -2 - lValue;
					}
				}

				*pIndex = lValue;
				pIndex++;
			}
		}

		// Unlock the texture
		this->mBuffers[ lFace ]->PlatformData->Mips[ 0 ].BulkData.Unlock();
	}
}

/*********************************************************************************************/
UMixedHeightMapFactory::~UMixedHeightMapFactory()
{
	
}

/*********************************************************************************************/
void UMixedHeightMapFactory::BuildNode(FPlanetaryQuadTreeNode* pNode)
{
	if 
		( pNode == NULL )
	{
		return;
	}

	UE_LOG( LogClass, Log, TEXT( "UMixedHeightMapFactory::BuildNode" ) );

	float lPlanetaryRadius    = pNode->GetOwnerTree()->OwnerMap()->PlanetaryRadius;
	float lPlanetaryMaxHeight = pNode->GetOwnerTree()->OwnerMap()->PlanetaryMaxHeight;
	// Initialize the height map
	for 
		( int32 lIndex = 0; lIndex < BORDER_MAP_COUNT; lIndex++ )
	{
		float lHeight = this->GetHeight( pNode->GetCookedCoordinate( lIndex ) );
		lHeight = lPlanetaryRadius + lHeight * lPlanetaryMaxHeight;
		pNode->EditCookedCoordinate( lIndex ).SetSurfaceHeight( lHeight );
	}
}

/*********************************************************************************************/
float UMixedHeightMapFactory::GetHeight(const PlanetaryTreeCoordinates& pCoordinates)
{
	// Get the initial height from the pre-generated maps
	float lBase;
	float lExtra;
	PlanetaryFace::PlanetaryFace lFace = pCoordinates.GetFace();
	
	int8 lElementSize = 4;
	float lX = pCoordinates.GetXCoordinate() * (this->mTextureSize - 1);
	float lY = pCoordinates.GetYCoordinate() * (this->mTextureSize - 1);
	int32 nX = FMath::Min(this->mTextureSize - 2, FMath::Max(0, (int32)lX));
	int32 nY = FMath::Min(this->mTextureSize - 2, FMath::Max(0, (int32)lY));
	float lRatioX = lX - nX;
	float lRatioY = lY - nY;

	float* lValue = StaticCast<float*>( this->mBuffers[ lFace ]->PlatformData->Mips[ 0 ].BulkData.Lock( LOCK_READ_ONLY ) ) + lElementSize * ( this->mTextureSize * nY + nX );
	lBase = lValue[ 0 ] * (1 - lRatioX) * (1 - lRatioY) +
			lValue[ 1 ] * lRatioX * (1 - lRatioY) +
			lValue[ this->mTextureSize ] * (1 - lRatioX) * lRatioY +
			lValue[ this->mTextureSize + 1 ] * lRatioX * lRatioY;
	this->mBuffers[ lFace ]->PlatformData->Mips[ 0 ].BulkData.Unlock();

	// Compute partial fractal to add detail
	if 
		( lBase > 0.0f )
	{
		float lLacunarity  = this->mFractalGenerator->Lacunarity();
		FVector lDirection = pCoordinates.GetDirection();
		lDirection *= FMath::Pow( lLacunarity, 6 );
		lExtra = FMath::Abs( this->mFractalGenerator->GetNoise( lDirection ) ) * this->mFractalGenerator->Exponents( 6 );
		for 
			( int8 i = 6; i < 12; i++ )
		{
			lDirection *= lLacunarity;
			lExtra += FMath::Abs( this->mFractalGenerator->GetNoise( lDirection ) ) * this->mFractalGenerator->Exponents( i );
		}

		// For numbers from 0..1, 1-Square(1-n) is similar to sqrt(n)
		lBase = lBase - ( 1 - FMath::Square( 1 - lExtra ) ) * FMath::Sqrt( FMath::Abs( lBase ) );
	}

	// Return the final adjusted height
	return lBase;
}

/**********************************************************************************************************/
void  UMixedHeightMapFactory::Release()
{
	Super::Release();

	if 
		( this->mIsDisposed == false )
	{
		for
			( int8 lFace = 0; lFace < 6; lFace++ )
		{
			this->mBuffers[ lFace ]->ReleaseResource();
		}

		this->mIsDisposed = true;
	}
}
