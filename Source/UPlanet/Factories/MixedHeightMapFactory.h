// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "PlanetaryFactory.h"
#include "SimpleHeightMapFactory.h"
#include "../PlanetaryTreeCoordinates.h"
#include "MixedHeightMapFactory.generated.h"

//==========================================
// Forward declarations
//==========================================
class UTexture2D;

//==========================================
// Class definition
//==========================================
/**
 * Mixed height map factory class
 */
UCLASS()
class UPLANET_API UMixedHeightMapFactory : public USimpleHeightMapFactory
{
	GENERATED_BODY()
	
private:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * Stores the texture per face (6)
	 */
	UTexture2D* mBuffers[6];

	/**
	 * Stores the texture size
	 */
	int32 mTextureSize;

	/**
	 * Stores the flag indicating whether the factory reources have been disposed or not
	 */
	bool mIsDisposed;

	//==========================================
	// Private Methods
	//==========================================

	/**
	 * Gets the height at the planetary coordinates
	 */
	float GetHeight(const PlanetaryTreeCoordinates& pCoordinates);

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	UMixedHeightMapFactory(const FObjectInitializer& pInitializer);
	virtual ~UMixedHeightMapFactory();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Build the planetary node
	 */
	virtual void BuildNode(FPlanetaryQuadTreeNode* pNode) override;

	/**
	 * Release factory resources
	 */
	virtual void Release() override;
};
