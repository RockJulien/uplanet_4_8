// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryMap.h"
#include "DataStructures/PlanetaryQuadTree.h"
#include "DataStructures/PlanetaryQuadTreeNode.h"
#include "SimpleHeightMapFactory.h"

/**********************************************************************************************************/
USimpleHeightMapFactory::USimpleHeightMapFactory(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
	this->mFractalGenerator = new FractalBrownianMotion( 3, this->FractalSeed, 1.0f - this->Roughness, 2.0f );
}

/**********************************************************************************************************/
USimpleHeightMapFactory::~USimpleHeightMapFactory()
{
	
}

/**********************************************************************************************************/
void USimpleHeightMapFactory::BuildNode(FPlanetaryQuadTreeNode* pNode)
{
	if 
		( pNode == NULL )
	{
		return;
	}

	UE_LOG( LogClass, Log, TEXT("USimpleHeightMapFactory::BuildNode") );

	float lPlanetaryMaxHeight = pNode->GetOwnerTree()->OwnerMap()->PlanetaryMaxHeight;

	for 
		( int32 lIndex = 0; lIndex < BORDER_MAP_COUNT; lIndex++ )
	{
		PlanetaryTreeCoordinates lCoordinates = pNode->GetCookedCoordinate( lIndex );
		FVector lNormalized  = lCoordinates.GetDirection();
		float lSurfaceHeight = lCoordinates.GetSurfaceHeight();
		if 
			( this->FractalSeed )
		{
			FVector lTempPosition = lNormalized;
			lSurfaceHeight += this->mFractalGenerator->GetBrownianMotion( lTempPosition, this->Octaves ) * lPlanetaryMaxHeight;
		}

		pNode->EditCookedCoordinate( lIndex ).SetSurfaceHeight( lSurfaceHeight );
	}
}

/**********************************************************************************************************/
void USimpleHeightMapFactory::Release()
{
	Super::Release();

	if 
		( this->mFractalGenerator != NULL )
	{
		delete this->mFractalGenerator;
		this->mFractalGenerator = NULL;
	}
}
