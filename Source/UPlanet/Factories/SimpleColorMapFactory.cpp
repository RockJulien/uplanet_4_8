// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryMap.h"
#include "SimpleColorMapFactory.h"
#include "DataStructures/PlanetaryQuadTree.h"
#include "DataStructures/PlanetaryQuadTreeNode.h"

/******************************************************************************************/
USimpleColorMapFactory::USimpleColorMapFactory(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
	
}

/******************************************************************************************/
USimpleColorMapFactory::~USimpleColorMapFactory()
{
	
}

/******************************************************************************************/
void USimpleColorMapFactory::BuildNode(FPlanetaryQuadTreeNode* pNode)
{
	if
		( pNode == NULL )
	{
		return;
	}

	UE_LOG( LogClass, Log, TEXT( "USimpleColorMapFactory::BuildNode" ) );

	// Provide a color regarding to the height at each coordinate (e.g : from water to dirt/grass and then snow for the earth)
	int32 lColorCount		  = this->mColors.Num();
	float lPlanetaryRadius    = pNode->GetOwnerTree()->OwnerMap()->PlanetaryRadius;
	float lPlanetaryMaxHeight = pNode->GetOwnerTree()->OwnerMap()->PlanetaryMaxHeight;
	uint8* lData = StaticCast<uint8*>( pNode->EditGroundTexture()->PlatformData->Mips[ 0 ].BulkData.Lock( LOCK_READ_WRITE ) );
	for 
		( int32 y = 0; y < HEIGHT_MAP_WIDTH; y++ )
	{
		int32 lCoord = (y + 1) * BORDER_MAP_WIDTH + 1;
		for 
			( int32 x = 0; x < HEIGHT_MAP_WIDTH; x++ )
		{
			float lAltitude = pNode->GetCookedCoordinate( lCoord++ ).GetSurfaceHeight() - lPlanetaryRadius;
			float lHeight   = lAltitude / lPlanetaryMaxHeight;
			lHeight = (lColorCount - 1) * FMath::Clamp( (lHeight + 1.0f) * 0.5f, 0.001f, 0.999f );
			int32 nHeight = StaticCast<int32>( lHeight );
			float lRatio  = lHeight - nHeight;
			FColor lColor = this->ScaleColor( this->mColors[nHeight], (1 - lRatio) );
			lColor += this->ScaleColor( this->mColors[nHeight + 1], lRatio );

			*lData++ = lColor.R;
			*lData++ = lColor.G;
			*lData++ = lColor.B;
			*lData++ = lColor.A;
		}
	}
	pNode->EditGroundTexture()->PlatformData->Mips[ 0 ].BulkData.Unlock();
}

/******************************************************************************************/
void USimpleColorMapFactory::InitializeColorMap(ColorStyle pMode)
{
	this->mColors.Empty();
	switch 
		( pMode )
	{
	case ColorStyle::eEarthColor:
		{
			this->mColors.Add(FColor(0, 0, 74, 255));
			this->mColors.Add(FColor(0, 0, 74, 255));
			this->mColors.Add(FColor(0, 5, 89, 255));
			this->mColors.Add(FColor(0, 5, 89, 255));
			this->mColors.Add(FColor(0, 13, 99, 255));
			this->mColors.Add(FColor(0, 13, 99, 255));
			this->mColors.Add(FColor(0, 18, 112, 255));
			this->mColors.Add(FColor(0, 18, 112, 255));
			this->mColors.Add(FColor(0, 18, 112, 255));
			this->mColors.Add(FColor(0, 18, 112, 255));
			this->mColors.Add(FColor(3, 23, 125, 255));
			this->mColors.Add(FColor(3, 23, 125, 255));
			this->mColors.Add(FColor(3, 36, 140, 255));
			this->mColors.Add(FColor(3, 36, 140, 255));
			this->mColors.Add(FColor(5, 64, 166, 255));
			this->mColors.Add(FColor(5, 64, 166, 255));
			this->mColors.Add(FColor(196, 155, 102, 255));
			this->mColors.Add(FColor(196, 155, 102, 255));
			this->mColors.Add(FColor(136, 112, 56, 255));
			this->mColors.Add(FColor(136, 112, 56, 255));
			this->mColors.Add(FColor(63, 70, 34, 255));
			this->mColors.Add(FColor(63, 70, 34, 255));
			this->mColors.Add(FColor(20, 47, 25, 255));
			this->mColors.Add(FColor(20, 47, 25, 255));
			this->mColors.Add(FColor(43, 60, 47, 255));
			this->mColors.Add(FColor(43, 60, 47, 255));
			this->mColors.Add(FColor(101, 107, 100, 255));
			this->mColors.Add(FColor(101, 107, 100, 255));
			this->mColors.Add(FColor(255, 255, 255, 255));
			this->mColors.Add(FColor(255, 255, 255, 255));
			this->mColors.Add(FColor(255, 255, 255, 255));
			this->mColors.Add(FColor(255, 255, 255, 255));
		}
		break;
	case ColorStyle::eLunarColor:
		{
			this->mColors.Add(FColor(120, 120, 120, 255));
			this->mColors.Add(FColor(121, 121, 121, 255));
			this->mColors.Add(FColor(122, 122, 122, 255));
			this->mColors.Add(FColor(123, 123, 123, 255));
			this->mColors.Add(FColor(125, 125, 125, 255));
			this->mColors.Add(FColor(126, 126, 126, 255));
			this->mColors.Add(FColor(127, 127, 127, 255));
			this->mColors.Add(FColor(128, 128, 128, 255));
			this->mColors.Add(FColor(130, 130, 130, 255));
			this->mColors.Add(FColor(131, 131, 131, 255));
			this->mColors.Add(FColor(132, 132, 132, 255));
			this->mColors.Add(FColor(133, 133, 133, 255));
			this->mColors.Add(FColor(135, 135, 135, 255));
			this->mColors.Add(FColor(136, 136, 136, 255));
			this->mColors.Add(FColor(137, 137, 137, 255));
			this->mColors.Add(FColor(138, 138, 138, 255));
			this->mColors.Add(FColor(140, 140, 140, 255));
			this->mColors.Add(FColor(141, 141, 141, 255));
			this->mColors.Add(FColor(142, 142, 142, 255));
			this->mColors.Add(FColor(143, 143, 143, 255));
			this->mColors.Add(FColor(145, 145, 145, 255));
			this->mColors.Add(FColor(146, 146, 146, 255));
			this->mColors.Add(FColor(147, 147, 147, 255));
			this->mColors.Add(FColor(148, 148, 148, 255));
			this->mColors.Add(FColor(150, 150, 150, 255));
			this->mColors.Add(FColor(151, 151, 151, 255));
			this->mColors.Add(FColor(152, 152, 152, 255));
			this->mColors.Add(FColor(153, 153, 153, 255));
			this->mColors.Add(FColor(155, 155, 155, 255));
			this->mColors.Add(FColor(156, 156, 156, 255));
			this->mColors.Add(FColor(157, 157, 157, 255));
			this->mColors.Add(FColor(158, 158, 158, 255));
			this->mColors.Add(FColor(165, 165, 165, 255));
			this->mColors.Add(FColor(166, 166, 166, 255));
			this->mColors.Add(FColor(167, 167, 167, 255));
			this->mColors.Add(FColor(168, 168, 168, 255));
			this->mColors.Add(FColor(170, 170, 170, 255));
			this->mColors.Add(FColor(171, 171, 171, 255));
			this->mColors.Add(FColor(172, 172, 172, 255));
			this->mColors.Add(FColor(173, 173, 173, 255));
			this->mColors.Add(FColor(175, 175, 175, 255));
			this->mColors.Add(FColor(176, 176, 176, 255));
			this->mColors.Add(FColor(177, 177, 177, 255));
			this->mColors.Add(FColor(178, 178, 178, 255));
			this->mColors.Add(FColor(180, 180, 180, 255));
			this->mColors.Add(FColor(181, 181, 181, 255));
			this->mColors.Add(FColor(182, 182, 182, 255));
			this->mColors.Add(FColor(183, 183, 183, 255));
			this->mColors.Add(FColor(185, 185, 185, 255));
			this->mColors.Add(FColor(186, 186, 186, 255));
			this->mColors.Add(FColor(187, 187, 187, 255));
			this->mColors.Add(FColor(188, 188, 188, 255));
			this->mColors.Add(FColor(190, 190, 190, 255));
			this->mColors.Add(FColor(191, 191, 191, 255));
			this->mColors.Add(FColor(192, 192, 192, 255));
			this->mColors.Add(FColor(193, 193, 193, 255));
			this->mColors.Add(FColor(195, 195, 195, 255));
			this->mColors.Add(FColor(196, 196, 196, 255));
			this->mColors.Add(FColor(197, 197, 197, 255));
			this->mColors.Add(FColor(198, 198, 198, 255));
			this->mColors.Add(FColor(200, 200, 200, 255));
			this->mColors.Add(FColor(201, 201, 201, 255));
			this->mColors.Add(FColor(202, 202, 202, 255));
			this->mColors.Add(FColor(203, 203, 203, 255));
		}
		break;
	case ColorStyle::eMarsColor:
		{
			// TO DO
		}
		break;
	}
}

/**********************************************************************************************************/
void USimpleColorMapFactory::Release()
{
	Super::Release();

	// Add your stuff
}
