// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryMap.h"
#include "Randomizers/Craterizer.h"
#include "SimpleCraterFactory.h"
#include "DataStructures/PlanetaryQuadTree.h"
#include "DataStructures/PlanetaryQuadTreeNode.h"

/**********************************************************************************************************/
USimpleCraterFactory::USimpleCraterFactory(const FObjectInitializer& pInitializer) :
Super( pInitializer )
{
	this->mCraterScale[ 0 ] = 0.0f;

	this->FractalSeed = 865366;
	this->TopLevel    = 5;
	this->BottomLevel = 7;
	this->CraterCountPerLevel = 1;
}

/**********************************************************************************************************/
USimpleCraterFactory::~USimpleCraterFactory()
{
	
}

/**********************************************************************************************************/
void USimpleCraterFactory::BuildNode(FPlanetaryQuadTreeNode* pNode)
{
	if
		( pNode == NULL )
	{
		return;
	}

	UE_LOG( LogClass, Log, TEXT( "USimpleCraterFactory::BuildNode" ) );

	if 
		( this->mCraterScale[0] == 0.0f )
	{
		float lScale = pNode->GetOwnerTree()->OwnerMap()->PlanetaryRadius * 0.5f;
		for 
			( int32 lCounter = 0; lCounter < 50; lCounter++ )
		{
			this->mCraterScale[ lCounter ] = lScale / ( 2.0f * FMath::Pow( lScale, 0.25f ) );
			lScale *= 0.5f;
		}
	}

	for 
		( int32 lIndex = 0; lIndex < BORDER_MAP_COUNT; lIndex++ )
	{
		PlanetaryTreeCoordinates lCoordinates = pNode->GetCookedCoordinate( lIndex );
		float lHeight = lCoordinates.GetSurfaceHeight();
		float lX = lCoordinates.GetXCoordinate();
		float lY = lCoordinates.GetYCoordinate();

		// Skip over the top levels of the crater tree, generating the appropriate seeds on the way
		int32 lCounter;
		int32 lSeed = this->FractalSeed + (((int16)pNode->GetFace()) << 4);
		for 
			( lCounter = 0; lCounter < this->TopLevel; lCounter++ )
		{
			lSeed = Craterizer::GetChildSeed( lSeed, lX, lY );
		}

		// Now add all the applicable crater offsets down to the bottom of the tree
		Craterizer lNode( lSeed, this->CraterCountPerLevel );
		while 
			( lCounter <= this->BottomLevel )
		{
			lNode.ModifySeed( lSeed );
			lHeight += lNode.GetOffset( lX, lY, this->mCraterScale[ lCounter ] );
			lSeed    = Craterizer::GetChildSeed( lSeed, lX, lY );
			lCounter++;
		}

		pNode->EditCookedCoordinate( lIndex ).SetSurfaceHeight( lHeight );
	}
}
