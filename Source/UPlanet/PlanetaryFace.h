// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

namespace PlanetaryFace
{

	/**
	 * Enumeration offering the available planetary faces (like cube faces)
	 */
	enum PlanetaryFace
	{
		/**
		 * No face
		 */
		eNone = -1,

		/**
		 * The right face
		 */
		 eRightFace = 0,

		 /**
		  * The left face
		  */
		  eLeftFace,

		  /**
		   * The top face
		   */
		   eTopFace,

		   /**
			* The Bottom face
			*/
			eBottomFace,

			/**
			 * The front face
			 */
			 eFrontFace,

			 /**
			  * The back face
			  */
			  eBackFace
	};

}
