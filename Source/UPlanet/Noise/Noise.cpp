// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "Noise.h"
#include "GenericPlatformMath.h"
#include "../Randomizers/BaseRandomizer.h"

/*****************************************************************************************************/
float Noise::GetLattice(int32 pXInt, float pXFloat, int32 pYInt, float pYFloat, int32 pZInt, float pZFloat, int32 pWInt, float pWFloat)
{
	int32 n[4] = { pXInt, pYInt, pZInt, pWInt };
	float f[4] = { pXFloat, pYFloat, pZFloat, pWFloat };
	uint32 lIndex = 0;
	for 
		( uint32 lCurrentDimension = 0; lCurrentDimension < this->mDimensionCount; lCurrentDimension++ )
	{
		lIndex = this->mMap[ (lIndex + n[ lCurrentDimension ]) & 0xFF ];
	}

	float lToReturn = 0;
	for 
		( uint32 lCurrentDimension = 0; lCurrentDimension < this->mDimensionCount; lCurrentDimension++ )
	{
		lToReturn += this->mBuffer[ lIndex ][ lCurrentDimension ] * f[ lCurrentDimension ];
	}

	return lToReturn;
}

/*****************************************************************************************************/
void  Noise::Normalize(float* pValues, uint8 pDimensionCount)
{
	if 
		( pValues == NULL )
	{
		return;
	}

	float lMagnitude = 0;
	for 
		( uint8 lCurrDimension = 0; lCurrDimension < pDimensionCount; lCurrDimension++ )
	{
		lMagnitude += pValues[ lCurrDimension ] * pValues[ lCurrDimension ];
	}

	lMagnitude = 1 / FGenericPlatformMath::Sqrt( lMagnitude );

	for 
		( uint8 lCurrDimension = 0; lCurrDimension < pDimensionCount; lCurrDimension++ )
	{
		pValues[ lCurrDimension ] *= lMagnitude;
	}
}

/*****************************************************************************************************/
float Noise::Cubic(float pValue)
{
	return pValue * pValue * (3 - 2 * pValue);
}

/*****************************************************************************************************/
Noise::Noise()
{
	this->ModifyNoise( 3, -1 );
}

/*****************************************************************************************************/
Noise::Noise(uint8 pDimensionCount, uint32 pSeed)
{
	this->ModifyNoise( pDimensionCount, pSeed );
}

/*****************************************************************************************************/
void  Noise::ModifyNoise(uint8 pDimensionCount, uint32 pSeed)
{
	uint8 lMaxDimension = 4;
	this->mDimensionCount = FGenericPlatformMath::Min( pDimensionCount, lMaxDimension );
	BaseRandomizer lRandomizer( pSeed );

	uint16 lCounter;
	for 
		( lCounter = 0; lCounter < 256; lCounter++ )
	{
		this->mMap[ lCounter ] = lCounter;
		for 
			( uint8 lCurrentDimension = 0; lCurrentDimension < this->mDimensionCount; lCurrentDimension++ )
		{
			this->mBuffer[ lCounter ][ lCurrentDimension ] = StaticCast<float>( lRandomizer.GetRandomBetweenD( -0.5, 0.5 ) );
		}

		this->Normalize( this->mBuffer[ lCounter ], this->mDimensionCount );
	}

	for 
		( uint16 lCurr = lCounter; lCurr > 0; lCurr-- )
	{
		uint32 lRandom = lRandomizer.GetRandomBetweenUI( 0, 255 );
		Swap( this->mMap[ lCurr ], this->mMap[ lRandom ] );
	}
}

/*****************************************************************************************************/
float Noise::GetNoise(const FVector& pDirection)
{
	const uint8 lMaxDimension = 4;
	int32 lIndices[ lMaxDimension ];	// Indexes to pass to lattice function
	float lRemainders[ lMaxDimension ];	// Remainders to pass to lattice function
	float lCubics[ lMaxDimension ];		// Cubic values to pass to interpolation function

	for 
		( uint8 lCurrDimension = 0; lCurrDimension < this->mDimensionCount; lCurrDimension++ )
	{
		lIndices[ lCurrDimension ] = FGenericPlatformMath::FloorToInt( pDirection[ lCurrDimension ] );
		lRemainders[ lCurrDimension ] = pDirection[ lCurrDimension ] - lIndices[ lCurrDimension ];
		lCubics[ lCurrDimension ]  = this->Cubic( lRemainders[ lCurrDimension ] );
	}
	
	float lToReturn = 0.0f;
	switch 
		( this->mDimensionCount )
	{
	case 1:
		lToReturn = FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1 ),
								 lCubics[ 0 ] );
		break;
	case 2:
		lToReturn = FMath::Lerp( FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ] ),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1 ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] );
		break;
	case 3:
		lToReturn = FMath::Lerp( FMath::Lerp( FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ], lRemainders[ 2 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ], lRemainders[ 2 ] ),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ], lRemainders[ 2 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ], lRemainders[ 2 ] ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] ),
								 FMath::Lerp( FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1 ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] ),
								 lCubics[ 2 ] );
		break;
	case 4:
		lToReturn = FMath::Lerp( FMath::Lerp( FMath::Lerp( FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ], lRemainders[ 3 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ], lRemainders[ 3 ] ),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ], lRemainders[ 3 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ], lRemainders[ 3 ] ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] ),
								 FMath::Lerp( FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1, lIndices[ 3 ], lRemainders[ 3 ] ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1, lIndices[ 3 ], lRemainders[ 3 ] ),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1, lIndices[ 3 ], lRemainders[ 3 ] ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] ),
								 lCubics[ 2 ] ),
								 FMath::Lerp( FMath::Lerp( FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[0], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ], lRemainders[ 2 ], lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] ),
								 FMath::Lerp(FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1, lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ], lRemainders[ 1 ], lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1, lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 lCubics[ 0 ] ),
								 FMath::Lerp( this->GetLattice( lIndices[ 0 ], lRemainders[ 0 ], lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1 ),
								 this->GetLattice( lIndices[ 0 ] + 1, lRemainders[ 0 ] - 1, lIndices[ 1 ] + 1, lRemainders[ 1 ] - 1, lIndices[ 2 ] + 1, lRemainders[ 2 ] - 1, lIndices[ 3 ] + 1, lRemainders[ 3 ] - 1 ),
								 lCubics[ 0 ] ),
								 lCubics[ 1 ] ),
								 lCubics[ 2 ] ),
								 lCubics[ 3 ] );
		break;
	}
	
	return FMath::Clamp( lToReturn * 2.0f, - 0.99999f, 0.99999f);
}
