// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================

//==========================================
// Define and Constants
//==========================================
#undef MAX_DIMENSIONS
#define MAX_DIMENSIONS 3

//==========================================
// Class definition
//==========================================
/**
 * Perlin noise generator class
 */
class UPLANET_API Noise
{
protected:

	//==========================================
	// Attributes
	//==========================================

	/**
	 * Number of dimensions used by this object
	 */
	uint32 mDimensionCount;

	/**
	 * Randomized map of indexes into buffer
	 */
	UTF8CHAR mMap[ 256 ];

	/**
	 * Random n-dimensional buffer
	 */
	float mBuffer[ 256 ][ MAX_DIMENSIONS ];

	//==========================================
	// Private Methods
	//==========================================

	/**
	 * Get the lattice value
	 */
	float GetLattice(int32 pXInt, float pXFloat, int32 pYInt = 0, float pYFloat = 0.0f, int32 pZInt = 0, float pZFloat = 0.0f, int32 pWInt = 0, float pWFloat = 0.0f);
	
	/**
	 * Normalize the set of floating values like a vector
	 */
	void  Normalize(float* pValues, uint8 pDimensionCount);

	/**
	 * Computes the cubic value of a value
	 */
	float Cubic(float pValue);

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	Noise();
	Noise(uint8 pDimensionCount, uint32 pSeed);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Modify the noise with the supplied dimension count and seed.
	 */
	void ModifyNoise(uint8 pDimensionCount, uint32 pSeed);

	/**
	 * Method offering a noise sample giving a direction.
	 */
	float GetNoise(const FVector& pDirection);
};