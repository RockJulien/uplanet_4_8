
/***************************************************************************************************/
FORCEINLINE FVector PlanetaryTreeCoordinates::GetDirection() const
{
	return FaceCoordinates::GetPosition( this->mFace );
}

/***************************************************************************************************/
FORCEINLINE FVector PlanetaryTreeCoordinates::GetPosition() const
{
	return this->GetPosition( this->mSurfaceHeight );
}

/***************************************************************************************************/
FORCEINLINE FVector PlanetaryTreeCoordinates::GetPosition(const float& pHeight) const
{
	return FaceCoordinates::GetPosition( this->mFace, pHeight );
}

/***************************************************************************************************/
FORCEINLINE PlanetaryFace::PlanetaryFace PlanetaryTreeCoordinates::GetFace() const
{
	return this->mFace;
}

/***************************************************************************************************/
FORCEINLINE PlanetaryCoordinateState::PlanetaryCoordinateState PlanetaryTreeCoordinates::GetState() const
{
	return this->mState;
}

/***************************************************************************************************/
FORCEINLINE float PlanetaryTreeCoordinates::GetSurfaceHeight() const
{
	return this->mSurfaceHeight;
}

/***************************************************************************************************/
FORCEINLINE void PlanetaryTreeCoordinates::SetState(PlanetaryCoordinateState::PlanetaryCoordinateState pState)
{
	this->mState = pState;
}

/***************************************************************************************************/
FORCEINLINE void PlanetaryTreeCoordinates::SetSurfaceHeight(const float& pHeight)
{
	this->mSurfaceHeight = pHeight;
}
