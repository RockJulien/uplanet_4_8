// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlatformMath.h"
#include "BaseRandomizer.h"

/****************************************************************************************************/
BaseRandomizer::BaseRandomizer()
{
	// Init the seed
	this->ModifySeed( -1 );
}

/****************************************************************************************************/
BaseRandomizer::BaseRandomizer(int32 pSeed)
{
	// Init the seed
	this->ModifySeed( pSeed );
}

/****************************************************************************************************/
void BaseRandomizer::ModifySeed(int32 pSeed)
{
	// Apply another seed
	FGenericPlatformMath::SRandInit( pSeed );
}

/****************************************************************************************************/
double BaseRandomizer::GetRandom()
{
	return (double)FGenericPlatformMath::SRand();
}

/****************************************************************************************************/
double BaseRandomizer::GetRandomBetweenD(double pMin, double pMax)
{
	double lInterval = pMax - pMin;
	double lRandom = lInterval * this->GetRandom();
	return pMin + FGenericPlatformMath::Min( lRandom, lInterval );
}

/****************************************************************************************************/
uint32 BaseRandomizer::GetRandomBetweenUI(uint32 pMin, uint32 pMax)
{
	uint32 lInterval = pMax - pMin;
	uint32 lRandom = StaticCast<uint32>( (lInterval + 1.0) * this->GetRandom() );
	return pMin + FGenericPlatformMath::Min( lRandom, lInterval );
}



