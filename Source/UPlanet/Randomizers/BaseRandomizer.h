// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================

//==========================================
// Class definition
//==========================================
/**
 * Base randomizer class wrapping the unreal random generator
 */
class UPLANET_API BaseRandomizer
{
public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	BaseRandomizer();
	BaseRandomizer(int32 pSeed);

	//==========================================
	// Methods
	//==========================================
	
	/**
	 * Modify the randomizer with the supplied seed.
	 */
	void ModifySeed(int32 pSeed);

	/**
	 * This method offers a random value as a floating 
	 * point double precision value
	 */
	double GetRandom();

	/**
	 * This method offers a random value as a floating
	 * point double precision value between two boudning
	 * values
	 */
	double GetRandomBetweenD(double pMin, double pMax);

	/**
	 * This method offers a random value as an unsigned
	 * integer value between two boudning values
	 */
	uint32 GetRandomBetweenUI(uint32 pMin, uint32 pMax);
};
