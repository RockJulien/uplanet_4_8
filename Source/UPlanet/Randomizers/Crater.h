// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================


//==========================================
// Class definition
//==========================================
/**
 * Creater base class
 */
class UPLANET_API Crater
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * Stores the crater X coordinate position
	 */
	float mX;

	/**
	 * Stores the crater Y coordinate position
	 */
	float mY;

	/**
	 * Stores the crater radius
	 */
	float mRadius;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	Crater();

	//==========================================
	// Methods
	//==========================================

	/**
	 * Gets the crater X coordinate position
	 */
	float CraterXCoordinate() const;

	/**
	 * Gets the crater Y coordinate position
	 */
	float CraterYCoordinate() const;

	/**
	 * Gets the crater radius
	 */
	float CraterRadius() const;
};
