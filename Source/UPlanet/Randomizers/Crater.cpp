// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "Crater.h"
#include "BaseRandomizer.h"

/********************************************************************************************************/
Crater::Crater()
{
	BaseRandomizer lRandomizer;
	this->mX = lRandomizer.GetRandom() * 0.5f + 0.25f;
	this->mY = lRandomizer.GetRandom() * 0.5f + 0.25f;
	this->mRadius = lRandomizer.GetRandom() * 0.125f + 0.125f;
}

/********************************************************************************************************/
float Crater::CraterXCoordinate() const
{
	return this->mX;
}

/********************************************************************************************************/
float Crater::CraterYCoordinate() const
{
	return this->mY;
}

/********************************************************************************************************/
float Crater::CraterRadius() const
{
	return this->mRadius;
}
