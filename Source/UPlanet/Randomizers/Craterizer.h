// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Crater.h"

//==========================================
// Class definition
//==========================================
/**
 * Creater-rizer class offering values used to create a creater based shape.
 */
class UPLANET_API Craterizer
{
private:

	//==========================================
	// Attributes
	//==========================================
	/**
	 * Stores the creater seed.
 	 */
	int32 mSeed;

	/**
	 * Stores the crater count
	 */
	int32 mCraterCount;

	/**
	 * Stores the creaters data
	 */
	Crater mCraters[20];

public:

	//==========================================
	// Inner types
	//==========================================
	/**
	 * Crater corner enumeration used to 
	 * minimize the chances of the same seed
	 */
	enum CraterCorner
	{
		/**
		 * The top left crater corner
		 */
		eTopLeft  = 11,

		/**
		 * The top right crater corner
		 */
		eTopRight = 101,

		/**
		 * The bottom left crater corner
		 */
		eBottomLeft = 1009,

		/**
		 * The bottom right crater corner
		 */
		eBottomRight = 10007
	};

	//==========================================
	// Constructors & Destructor
	//==========================================
	Craterizer();
	Craterizer(int32 pSeed, int32 pCount);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Modify the craterizer with the supplied seed.
	 */
	void ModifySeed(int32 pSeed);

	/**
	 * Gets the distance
	 */
	float GetDistance(int32 pIndex, float pX, float pY);

	/**
	 * Gets the offset
	 */
	float GetOffset(float pX, float pY, float pScale);

	/**
	 * Gets the child seed
	 */
	static int32 GetChildSeed(int32 pSeed, float& pX, float& pY);
};
