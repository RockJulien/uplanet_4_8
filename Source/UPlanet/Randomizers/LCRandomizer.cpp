// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "LCRandomizer.h"

/********************************************************************************************************/
LCRandomizer::LCRandomizer()
{
	this->ModifySeed( -1 );
}

LCRandomizer::LCRandomizer(uint32 pSeed)
{
	this->ModifySeed( pSeed );
}

void LCRandomizer::ModifySeed(uint32 pSeed)
{
	this->mSeed      = pSeed;

	// TO DO : Involves seed in the two value below.
	this->mQuotient  = LONG_MAX / 16807L;
	this->mRemainder = LONG_MAX % 16807L;
}

uint32 LCRandomizer::GetRandom()
{
	if 
		( this->mSeed <= this->mQuotient )
	{
		this->mSeed = (this->mSeed * 16807L) % LONG_MAX;
	}
	else
	{
		int32 lHigh = this->mSeed / this->mQuotient;
		int32 lLow  = this->mSeed % this->mQuotient;

		int32 lChecker = 16807L * lLow - this->mRemainder * lHigh;

		if 
			( lChecker > 0 )
		{
			this->mSeed = StaticCast<uint32>( lChecker );
		}
		else
		{
			this->mSeed = lChecker + LONG_MAX;
		}
	}

	return this->mSeed;
}



