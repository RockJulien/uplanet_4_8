// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

/**
 * Kirkpatrick and stoll R250 random generator using a shift register sequence.
 */
class UPLANET_API R250Randomizer
{
protected:

	//==========================================
	// Attributes
	//==========================================
	
	/**
	 * The set of random values pre generated
	 */
	uint32 mR250Buffer[250];

	/**
	 * The Buffer index
	 */
	int32  mR250Index;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	R250Randomizer();
	R250Randomizer(uint32 pSeed);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Modify the randomizer with the supplied seed.
	 */
	void ModifySeed(uint32 pSeed);

	/**
	 * This method offers a random value as an unsigned
	 * interger value
	 */
	uint32 GetRandom();

	/**
	 * This method offers a random value as a floating point
	 * double precision value
	 */
	double GetRandomD();
};
