// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "R250Randomizer.h"
#include "LCRandomizer.h"
#include "PlatformMath.h"

#define ALL_BITS   0xffffffffL
#define HALF_RANGE 0x40000000L
#define MSB	 0x80000000L
#define STEP 7

/********************************************************************************************/
R250Randomizer::R250Randomizer()
{
	this->ModifySeed( -1 );
}

/********************************************************************************************/
R250Randomizer::R250Randomizer(uint32 pSeed)
{
	this->ModifySeed( pSeed );
}

/********************************************************************************************/
void R250Randomizer::ModifySeed(uint32 pSeed)
{
	FGenericPlatformMath::SRandInit( pSeed );
	LCRandomizer lLCRandomizer( pSeed );

	this->mR250Index = 0;
	for 
		( uint32 lCurrentRandom = 0; lCurrentRandom < 250; lCurrentRandom++ )
	{
		this->mR250Buffer[ lCurrentRandom ] = StaticCast<uint32>( FGenericPlatformMath::SRand() );

		// Set some MSBs to 1
		if
			( lLCRandomizer.GetRandom() > HALF_RANGE )
		{
			this->mR250Buffer[ lCurrentRandom ] |= MSB;
		}
	}

	uint32 lMostSignificantBit = MSB; // turn on diagonal bit
	uint32 lMask = ALL_BITS;	      // turn off the leftmost bits

	int32 lWord;
	uint8 lBits = 32;
	for 
		( uint32 lCurrentBit = 0; lCurrentBit < lBits; lCurrentBit++ )
	{
		lWord = STEP * lCurrentBit + 3;	// select a word to operate on
		this->mR250Buffer[ lWord ] &= lMask; // turn off bits left of the diagonal
		this->mR250Buffer[ lWord ] |= lMostSignificantBit;  // turn on the diagonal bit
		lMask >>= 1;
		lMostSignificantBit >>= 1;
	}
}

/********************************************************************************************/
uint32 R250Randomizer::GetRandom()
{
	register int32 lRegisterIndex;
	if 
		( this->mR250Index >= 147 )
	{
		lRegisterIndex = this->mR250Index - 147;
	}
	else
	{
		lRegisterIndex = this->mR250Index + 103;
	}

	register uint32 lNewRand = this->mR250Buffer[ this->mR250Index ] ^ this->mR250Buffer[ lRegisterIndex ];
	this->mR250Buffer[ this->mR250Index ] = lNewRand;

	// Wrap index if needed.
	if 
		( this->mR250Index >= 249 )
	{
		this->mR250Index = 0;
	}
	else
	{
		this->mR250Index++;
	}

	return lNewRand;
}

/********************************************************************************************/
double R250Randomizer::GetRandomD()
{
	return StaticCast<double>( this->GetRandom() / ALL_BITS );
}

#undef ALL_BITS
#undef HALF_RANGE
#undef MSB
#undef STEP


