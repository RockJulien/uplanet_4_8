// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "Craterizer.h"
#include "BaseRandomizer.h"

/********************************************************************************************************/
Craterizer::Craterizer()
{
	this->mCraterCount = 5;
	this->ModifySeed( -1 );
}

/********************************************************************************************************/
Craterizer::Craterizer(int32 pSeed, int32 pCount)
{
	this->mCraterCount = pCount;
	this->ModifySeed( pSeed );
}

/********************************************************************************************************/
void Craterizer::ModifySeed(int32 pSeed)
{
	this->mSeed = pSeed;
	BaseRandomizer lRandomizer( this->mSeed );
	for
		( int32 lCurrCrater = 0; lCurrCrater < this->mCraterCount; lCurrCrater++ )
	{
		if
			( lRandomizer.GetRandom() < 0.5f )
		{
			this->mCraterCount--;
		}
	}

	for
		( int32 lCurrCrater = 0; lCurrCrater < this->mCraterCount; lCurrCrater++ )
	{
		this->mCraters[ lCurrCrater ] = Crater();
	}
}

/********************************************************************************************************/
float Craterizer::GetDistance(int32 pIndex, float pX, float pY)
{
	float lDx = pX - this->mCraters[ pIndex ].CraterXCoordinate();
	float lDy = pY - this->mCraters[ pIndex ].CraterYCoordinate();
	lDx *= lDx;
	lDy *= lDy;

	return lDx + lDy;
}

/********************************************************************************************************/
float Craterizer::GetOffset(float pX, float pY, float pScale)
{
	float lOffset = 0;
	for 
		( int32 lCurrCrater = 0; lCurrCrater < this->mCraterCount; lCurrCrater++ )
	{
		float lDistance = this->GetDistance( lCurrCrater, pX, pY );
		float lRadius   = this->mCraters[ lCurrCrater ].CraterRadius();
		float lDiameter = lRadius * lRadius;
		if 
			( lDistance < lDiameter )
		{
			lDistance /= lDiameter;
			float lDistanceSquare = lDistance * lDistance;
			lOffset += ((lDistanceSquare - 0.25f) * 2.0f * lRadius) * (1 - lDistance);
		}
	}

	return lOffset * pScale;
}

/********************************************************************************************************/
int32 Craterizer::GetChildSeed(int32 pSeed, float& pX, float& pY)
{
	pSeed = pSeed * 100;

	if 
		( pX < 0.5f )
	{
		if 
			( pY < 0.5f )
		{
			pSeed += Craterizer::eTopLeft;
		}
		else
		{
			pY -= 0.5f;
			pSeed += Craterizer::eBottomLeft;
		}
	}
	else
	{
		if 
			( pY < 0.5f )
		{
			pX -= 0.5f;
			pSeed += Craterizer::eTopRight;
		}
		else
		{
			pX -= 0.5f;
			pY -= 0.5f;
			pSeed += Craterizer::eBottomRight;
		}
	}

	pX *= 2.0f;
	pY *= 2.0f;

	return pSeed;
}
