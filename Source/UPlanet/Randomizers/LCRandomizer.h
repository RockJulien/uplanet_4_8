// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Class definition
//==========================================
/**
 * Linear congruential generator class (pseudo random generator)
 */
class UPLANET_API LCRandomizer
{
protected:

	//==========================================
	// Attributes
	//==========================================
	
	/**
	 * The seed to use for the random generation
	 */
	uint32 mSeed;

	/**
	 * The quotient to apply to the LC random generator
	 */
	uint32 mQuotient;

	/**
	 * The remainder value
	 */
	uint32 mRemainder;

public:

	//==========================================
	// Constructors & Destructor
	//==========================================
	LCRandomizer();
	LCRandomizer(uint32 pSeed);

	//==========================================
	// Methods
	//==========================================

	/**
	 * Modify the randomizer with the supplied seed.
	 */
	void ModifySeed(uint32 pSeed);

	/**
	 * This method offers a random value as an unsigned
	 * interger value
	 */
	uint32 GetRandom();
};
