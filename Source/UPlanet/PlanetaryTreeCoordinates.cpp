// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryTreeCoordinates.h"

/***************************************************************************************************/
PlanetaryTreeCoordinates::PlanetaryTreeCoordinates() :
FaceCoordinates(), mSurfaceHeight(0.0f), mFace(PlanetaryFace::eFrontFace), mState(PlanetaryCoordinateState::eNone)
{

}

/***************************************************************************************************/
PlanetaryTreeCoordinates::PlanetaryTreeCoordinates(const PlanetaryTreeCoordinates& pToCopy) :
FaceCoordinates(pToCopy), mSurfaceHeight(pToCopy.mSurfaceHeight), mFace(pToCopy.mFace), mState(pToCopy.mState)
{

}

/***************************************************************************************************/
PlanetaryTreeCoordinates::PlanetaryTreeCoordinates(PlanetaryFace::PlanetaryFace pFace, const FaceCoordinates& pCoordinate, const float& pHeight) :
FaceCoordinates(pCoordinate), mSurfaceHeight(pHeight), mFace(pFace), mState(PlanetaryCoordinateState::eNone)
{

}

/***************************************************************************************************/
PlanetaryTreeCoordinates::PlanetaryTreeCoordinates(const FVector& pPosition, const float& pHeight) :
FaceCoordinates(), mSurfaceHeight(pHeight), mState(PlanetaryCoordinateState::eNone)
{
	this->mFace = PlanetaryHelper::GetFace( pPosition );
	PlanetaryHelper::GetFaceCoordinatesFromPosition( this->mFace, pPosition, this->mXCoordinate, this->mYCoordinate );
}

/***************************************************************************************************/
PlanetaryTreeCoordinates::PlanetaryTreeCoordinates(PlanetaryFace::PlanetaryFace pFace, const FVector& pPosition, const float& pHeight) :
FaceCoordinates(), mSurfaceHeight(pHeight), mFace(pFace), mState(PlanetaryCoordinateState::eNone)
{
	PlanetaryHelper::GetFaceCoordinatesFromPosition( this->mFace, pPosition, this->mXCoordinate, this->mYCoordinate );
}

/***************************************************************************************************/
PlanetaryTreeCoordinates::PlanetaryTreeCoordinates(PlanetaryFace::PlanetaryFace pFace, const float& pXCoordinate, const float& pYCoordinate, const float& pHeight) :
FaceCoordinates(pXCoordinate, pYCoordinate), mSurfaceHeight(pHeight), mFace(pFace), mState(PlanetaryCoordinateState::eNone)
{

}

/***************************************************************************************************/
PlanetaryTreeCoordinates::~PlanetaryTreeCoordinates()
{

}

/***************************************************************************************************/
const PlanetaryTreeCoordinates& PlanetaryTreeCoordinates::operator = (const PlanetaryTreeCoordinates& pCoordinate)
{
	this->mFace  = pCoordinate.mFace;
	this->mState = pCoordinate.mState;
	this->mSurfaceHeight = pCoordinate.mSurfaceHeight;
	this->mXCoordinate   = pCoordinate.mXCoordinate;
	this->mYCoordinate   = pCoordinate.mYCoordinate;

	return *this;
}
