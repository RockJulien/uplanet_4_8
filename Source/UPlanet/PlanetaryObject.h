// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//==========================================
// Includes
//==========================================
#include "Interface_PlanetaryObject.h"
#include "PlanetaryObject.generated.h"

//==========================================
// Class definition
//==========================================
/**
 * Base class for all planetary object involved in the planet generation
 */
UCLASS(abstract)
class UPLANET_API UPlanetaryObject : public UObject, public IInterface_PlanetaryObject
{
	GENERATED_BODY()

protected:

	//==========================================
	// Constructors
	//==========================================
	UPlanetaryObject();
	UPlanetaryObject(const FObjectInitializer& pInitializer);

public:

	//==========================================
	// Methods
	//==========================================

	/**
	 * Update the planetary object
	 */
	virtual void Update() override;

	/**
	 * Draw the planetary object
	 */
	virtual void Draw() override;

};
