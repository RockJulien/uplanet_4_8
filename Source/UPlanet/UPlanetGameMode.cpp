// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "UPlanetGameMode.h"
#include "Player/PlayerSpaceship.h"

AUPlanetGameMode::AUPlanetGameMode(const FObjectInitializer& pInitializer) : 
Super( pInitializer )
{
	// set default pawn class to our flying pawn
	this->DefaultPawnClass = APlayerSpaceship::StaticClass();
}

