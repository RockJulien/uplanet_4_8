// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetActor.h"
#include "Player/PlayerSpaceship.h"

/*****************************************************************************************/
APlanetActor::APlanetActor(const FObjectInitializer& pInitializer) : 
Super( pInitializer )
{
	// Inform that actor must be updated every frames
	this->PrimaryActorTick.bCanEverTick = true;

	// Create the planet component
	this->Planet = pInitializer.CreateDefaultSubobject<UPlanetaryMap>( this, TEXT( "Planet" ) );

	// Set it as root component
	this->RootComponent = this->Planet;
}

/*****************************************************************************************/
void APlanetActor::Tick(float pDeltaSeconds)
{
	Super::Tick( pDeltaSeconds );

	// Find the first camera
	/*UCameraComponent* lCameraComponent = nullptr;
	for 
		( TObjectIterator<ACameraActor> CameraItr; CameraItr; ++CameraItr )
	{
		UCameraComponent* lCurrentCamera = CameraItr->GetCameraComponent();
		if 
			( lCurrentCamera != nullptr &&
			  lCurrentCamera->bIsActive )
		{
			lCameraComponent = lCurrentCamera;
			break;
		}
	}*/

	/*UClass* lClass   = nullptr;
	UScriptViewportClient* lObject = nullptr;
	for
		(TObjectIterator<UScriptViewportClient> Itr; Itr; ++Itr)
	{
		lObject = *Itr;
		lClass = Itr->GetClass();
	}*/

	/*if
		(lCameraComponent != nullptr)
	{
		this->Planet->Update( lCameraComponent );
	}*/

	ULocalPlayer* lPlayer = this->GetWorld()->GetFirstLocalPlayerFromController();
	bool lAnyPlayer = lPlayer != NULL;
	if 
		( lAnyPlayer )
	{
		APlayerSpaceship* lPlayerActor = Cast<APlayerSpaceship>( lPlayer->PlayerController->GetPawn() );
		bool lIsPlayerSpaceShip = lPlayerActor != NULL;
		if 
			( lIsPlayerSpaceShip )
		{
			// Look for the player camera component and use it to update the planet
			TInlineComponentArray<UCameraComponent*> lCameras;
			lPlayerActor->GetComponents<UCameraComponent>( lCameras );

			for
				( UCameraComponent* lCameraComponent : lCameras )
			{
				if
					( lCameraComponent->bIsActive )
				{
					this->Planet->Update( lCameraComponent );
					return;
				}
			}
		}
	}
}

/*****************************************************************************************/
void APlanetActor::BeginPlay()
{
	// Only allow planet map rendering once it really starts
	this->Planet->AllowRendering( true );
}

/*****************************************************************************************/
void APlanetActor::EndPlay(const EEndPlayReason::Type pEndPlayReason)
{
	// Stop any rendering once it stops
	this->Planet->AllowRendering( false );
}

/*****************************************************************************************/
bool APlanetActor::ShouldTickIfViewportsOnly() const
{
	// Force to tick even in viewport editor design mode (that is when not really playing/simulating)
	return true;
}



