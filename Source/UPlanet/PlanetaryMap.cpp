// Fill out your copyright notice in the Description page of Project Settings.

#include "UPlanet.h"
#include "PlanetaryMap.h"
#include "Factories/PlanetaryFactory.h"
#include "Factories/SimpleCraterFactory.h"
#include "Factories/MixedHeightMapFactory.h"
#include "Factories/SimpleColorMapFactory.h"
#include "DynamicMeshBuilder.h"
#include "Services/PlanetFactoryService.h"
#include "Renderer/PlanetaryMapSceneProxy.h"

/***********************************************************************************************/
void UPlanetaryMap::GenerateSkydomeGeometry(TArray<FSkydomeVertex>& pVertices, TArray<int32>& pIndices, float pRadius, uint16 pSliceCount, uint16 pStackCount)
{
	// Start fresh
	pVertices.Empty();
	pIndices.Empty();

	//
	// Compute the vertices starting at the top pole and moving down the stacks.
	//

	// Poles: note that there will be texture coordinate distortion as there is
	// not a unique point on the texture map to assign to the pole when mapping
	// a rectangular texture onto a sphere.
	FSkydomeVertex lTopVertex( 0.0f, +pRadius, 0.0f );
	FSkydomeVertex lBottomVertex( 0.0f, -pRadius, 0.0f );

	pVertices.Add( lTopVertex );

	float lPhiStep   = PI / pStackCount;
	float lThetaStep = 2.0f * PI / pSliceCount;

	// Compute vertices for each stack ring (do not count the poles as rings).
	for 
		( uint16 lCurrStack = 1; lCurrStack <= pStackCount - 1; lCurrStack++ )
	{
		float lPhi = lCurrStack * lPhiStep;

		// Vertices of ring.
		for 
			( uint16 lCurrSlice = 0; lCurrSlice <= pSliceCount; lCurrSlice++ )
		{
			float lTheta = lCurrSlice * lThetaStep;

			FSkydomeVertex lCurrVertex;

			// spherical to cartesian
			lCurrVertex.Position.X = pRadius * sinf( lPhi ) * cosf( lTheta );
			lCurrVertex.Position.Y = pRadius * cosf( lPhi );
			lCurrVertex.Position.Z = pRadius * sinf( lPhi ) * sinf( lTheta );

			pVertices.Add( lCurrVertex );
		}
	}

	pVertices.Add( lBottomVertex );

	//
	// Compute indices for top stack.
	// The top stack was written first to the lCurrVertex buffer
	// and connects the top pole to the first ring.
	//
	for 
		( uint16 lCurrStack = 1; lCurrStack <= pSliceCount; lCurrStack++ )
	{
		pIndices.Add( 0 );
		pIndices.Add( lCurrStack + 1 );
		pIndices.Add( lCurrStack );
	}

	//
	// Compute indices for inner stacks (not connected to poles).
	//

	// Offset the indices to the index of the first lCurrVertex in the first ring.
	// This is just skipping the top pole lCurrVertex.
	int32 lBaseIndex = 1;
	uint16 lRingVertexCount = pSliceCount + 1;
	for 
		( uint16 lCurrStack = 0; lCurrStack < pStackCount - 2; lCurrStack++ )
	{
		for 
			( uint16 lCurrSlice = 0; lCurrSlice < pSliceCount; lCurrSlice++ )
		{
			pIndices.Add( lBaseIndex + lCurrStack * lRingVertexCount + lCurrSlice );
			pIndices.Add( lBaseIndex + lCurrStack * lRingVertexCount + lCurrSlice + 1 );
			pIndices.Add( lBaseIndex + (lCurrStack + 1) * lRingVertexCount + lCurrSlice );

			pIndices.Add( lBaseIndex + (lCurrStack + 1) * lRingVertexCount + lCurrSlice );
			pIndices.Add( lBaseIndex + lCurrStack * lRingVertexCount + lCurrSlice + 1 );
			pIndices.Add( lBaseIndex + (lCurrStack + 1) * lRingVertexCount + lCurrSlice + 1 );
		}
	}

	//
	// Compute indices for bottom stack.  The bottom stack was written last to the lCurrVertex buffer
	// and connects the bottom pole to the bottom ring.
	//

	// South pole lCurrVertex was added last.
	int32 lSouthPoleIndex = pVertices.Num() - 1;

	// Offset the indices to the index of the first lCurrVertex in the last ring.
	lBaseIndex = lSouthPoleIndex - lRingVertexCount;
	for 
		( uint16 lCurrStack = 0; lCurrStack < pSliceCount; lCurrStack++ )
	{
		pIndices.Add( lSouthPoleIndex );
		pIndices.Add( lBaseIndex + lCurrStack );
		pIndices.Add( lBaseIndex + lCurrStack + 1 );
	}
}

/***********************************************************************************************/
FBoxSphereBounds UPlanetaryMap::CalcBounds(const FTransform& pLocalToWorld) const
{
	float lRadius = this->PlanetaryRadius;
	if
		( this->HasAtmosphere() )
	{
		lRadius = this->AtmosphericRadius;
	}

	return FBoxSphereBounds( FVector( 0.0f ),
							 FVector( lRadius ),
							 lRadius ).TransformBy( pLocalToWorld );
}

/***********************************************************************************************/
UPlanetaryMap::UPlanetaryMap(const FObjectInitializer& pInitializer) :
Super(pInitializer), mCurrentSplitCount(0), mAllowRendering(false)
{
	this->PrimaryComponentTick.bCanEverTick = false;
}

/***********************************************************************************************/
UPlanetaryMap::~UPlanetaryMap()
{
	
}

/***********************************************************************************************/
void UPlanetaryMap::Initialize()
{
	this->PlanetaryMode = PlanetaryStyle::eEarth;
	this->MaxSplitCount = 50;
	this->SplitFactor   = 16.0f;
	this->SplitPower    = 1.25f;

	this->PlanetaryRadius    = 63.780f;   // Planet radius
	this->AtmosphericRadius  = 65.3745f; // Slightly bigger
	this->PlanetaryMaxHeight = 20.0f;

	this->Wavelength = FVector(0.650f, 0.570f, 0.475f);
	this->Esun = 15.0f;
	this->Kr   = 0.0025;
	this->Km   = 0.0015;
	this->G    = -0.95;

	// Create skydome geometry
	this->GenerateSkydomeGeometry( this->mSkydomeVertices, this->mSkydomeIndices, this->AtmosphericRadius, 100, 100 );

	this->mFactoryService = new FPlanetFactoryService();

	// Create factories in the service (earth mode by default so provide color, 
	USimpleColorMapFactory* lColorFactory = NewObject<USimpleColorMapFactory>( this, USimpleColorMapFactory::StaticClass(), TEXT("SimpleColorMapFactory") );
	lColorFactory->InitializeColorMap( ColorStyle::eEarthColor );
	lColorFactory->SetPriority( 10 ); // Higher priority means latest one to build nodes

	USimpleCraterFactory* lSimpleCraterFactory = NewObject<USimpleCraterFactory>( this, USimpleCraterFactory::StaticClass(), TEXT("SimpleCraterFactory") );
	lSimpleCraterFactory->SetPriority( 1 ); // Smaller priority means the factory will build nodes in priority (first)

	//UMixedHeightMapFactory* lMixedHeightFactory = NewObject<UMixedHeightMapFactory>( this, UMixedHeightMapFactory::StaticClass(), TEXT("MixedHeightMapFactory") );

	this->mFactoryService->RegisterService( lSimpleCraterFactory );
	//this->mFactoryService->RegisterService( lMixedHeightFactory );
	this->mFactoryService->RegisterService( lColorFactory );

	for
		( int8 lCurrFace = 0; lCurrFace < 6; lCurrFace++ )
	{
		this->mFaceTrees[lCurrFace] = new FPlanetaryQuadTree( this, (PlanetaryFace::PlanetaryFace)lCurrFace );
	}

	this->mFaceTrees[PlanetaryFace::eRightFace]->SetNeighbors(this->mFaceTrees[PlanetaryFace::eTopFace], this->mFaceTrees[PlanetaryFace::eBackFace], this->mFaceTrees[PlanetaryFace::eBottomFace], this->mFaceTrees[PlanetaryFace::eFrontFace]);
	this->mFaceTrees[PlanetaryFace::eLeftFace]->SetNeighbors(this->mFaceTrees[PlanetaryFace::eTopFace], this->mFaceTrees[PlanetaryFace::eFrontFace], this->mFaceTrees[PlanetaryFace::eBottomFace], this->mFaceTrees[PlanetaryFace::eBackFace]);
	this->mFaceTrees[PlanetaryFace::eTopFace]->SetNeighbors(this->mFaceTrees[PlanetaryFace::eBackFace], this->mFaceTrees[PlanetaryFace::eRightFace], this->mFaceTrees[PlanetaryFace::eFrontFace], this->mFaceTrees[PlanetaryFace::eLeftFace]);
	this->mFaceTrees[PlanetaryFace::eBottomFace]->SetNeighbors(this->mFaceTrees[PlanetaryFace::eFrontFace], this->mFaceTrees[PlanetaryFace::eRightFace], this->mFaceTrees[PlanetaryFace::eBackFace], this->mFaceTrees[PlanetaryFace::eLeftFace]);
	this->mFaceTrees[PlanetaryFace::eFrontFace]->SetNeighbors(this->mFaceTrees[PlanetaryFace::eTopFace], this->mFaceTrees[PlanetaryFace::eRightFace], this->mFaceTrees[PlanetaryFace::eBottomFace], this->mFaceTrees[PlanetaryFace::eLeftFace]);
	this->mFaceTrees[PlanetaryFace::eBackFace]->SetNeighbors(this->mFaceTrees[PlanetaryFace::eTopFace], this->mFaceTrees[PlanetaryFace::eLeftFace], this->mFaceTrees[PlanetaryFace::eBottomFace], this->mFaceTrees[PlanetaryFace::eRightFace]);
}

/***********************************************************************************************/
void UPlanetaryMap::Update(const UCameraComponent* pCamera)
{
	if 
		( pCamera == NULL )
	{
		return;
	}

	UE_LOG( LogClass, Log, TEXT( "UPlanetaryMap::Update" ) );

	// Reset the split count for the current update pass
	this->mCurrentSplitCount  = 0;

	FVector lCameraPosition   = pCamera->GetComponentLocation();
	FQuat lCameraRotationQuat = pCamera->GetComponentQuat();

	FVector lPlanetPosition   = this->GetComponentLocation();

	FVector lPlanetToCamera   = lCameraPosition - lPlanetPosition;

	FQuat lPlanetRotationQuat = this->GetComponentQuat();

	lPlanetRotationQuat.Normalize();

	FQuat lInvPlanetRotationQuat = lPlanetRotationQuat.Inverse();

	// Refresh the camera relative info in cache.
	this->mRelativeCameraPosition    = lPlanetRotationQuat * lPlanetToCamera;
	this->mRelativeCameraOrientation = lInvPlanetRotationQuat * lCameraRotationQuat;
	
	// Refresh the horizon distance
	float lAltitude = this->mRelativeCameraPosition.Size();
	float lHorizonAltitude = FMath::Max( lAltitude - this->PlanetaryRadius, this->PlanetaryMaxHeight );
	this->mHorizonDistance = FMath::Sqrt( FMath::Square( lHorizonAltitude ) + 2.0f * lHorizonAltitude * this->PlanetaryRadius );

	// Refresh the camera coordinates over the planet
	this->mCameraCoordinates = PlanetaryTreeCoordinates( this->mRelativeCameraPosition, lAltitude );

	// Refresh the face order by sorting them by distance
	float lDistances[ 6 ];
	for 
		( int8 lCurrFace = 0; lCurrFace < 6; lCurrFace++ )
	{
		bool lResult = this->mFaceTrees[ lCurrFace ]->GetRoot()->HitTest( PlanetaryTreeCoordinates( this->mRelativeCameraPosition ) );
		if 
			( lResult )
		{
			lDistances[ lCurrFace ] = 0.0f;
		}
		else
		{
			FVector lCenter = this->mFaceTrees[ lCurrFace ]->GetRoot()->GetCookedVertex( 40 ).Position;
			float lDistance = FVector::DistSquared( this->mRelativeCameraPosition, lCenter );
			lDistances[ lCurrFace ] = lDistance;
		}

		// Reset the ordered array as well
		this->mOrderedFaces[ lCurrFace ] = (PlanetaryFace::PlanetaryFace)lCurrFace;
	}

	for 
		( int8 i = 0; i < 5; i++ )
	{
		for 
			( int8 j = 0; j < 5 - i; j++ )
		{
			if 
				( lDistances[ this->mOrderedFaces[ j ] ] > lDistances[ this->mOrderedFaces[ j + 1 ] ] )
			{
				Swap( this->mOrderedFaces[ j ], this->mOrderedFaces[ j + 1 ] );
			}
		}
	}

	// Now update the quad-tree (in order from nearest to farthest)
	for
		( int8 lCurrFace = 0; lCurrFace < 6; lCurrFace++ )
	{
		this->mFaceTrees[ this->mOrderedFaces[ lCurrFace ] ]->UpdateTree();
	}

	// Finally, update the factories (which may be handling surface objects)
	TArray<IInterface_PluginService*> lServices = this->mFactoryService->GetActiveServices();
	for
		( int8 lCurrService = 0; lCurrService < lServices.Num(); lCurrService++ )
	{
		IInterface_PluginService* lService = lServices[ lCurrService ];
		UPlanetaryFactory* lFactoryService = StaticCast<UPlanetaryFactory*>( lService );
		if
			( lFactoryService != NULL )
		{
			lFactoryService->Update();
		}
	}

	// Need to recreate scene proxy to send it over
	this->MarkRenderStateDirty();

	UE_LOG( LogClass, Log, TEXT( "End of UPlanetaryMap::Update" ) );
}

/****************************************************************************************************************/
bool UPlanetaryMap::CanSplit()
{
	if
		( this->mCurrentSplitCount >= this->MaxSplitCount )
	{
		return false;
	}

	this->mCurrentSplitCount++;

	return true;
}

/***********************************************************************************************/
void UPlanetaryMap::Release()
{
	for 
		( int8 lCurrFace = 0; lCurrFace < 6; lCurrFace++ )
	{
		bool lIsFaceTreeValid = this->mFaceTrees[ lCurrFace ] != NULL;
		if 
			( lIsFaceTreeValid )
		{
			delete this->mFaceTrees[ lCurrFace ];
			this->mFaceTrees[ lCurrFace ] = NULL;
		}
	}

	bool lIsFactoryValid = this->mFactoryService != NULL;
	if 
		( lIsFactoryValid )
	{
		// Releases factory service resources
		this->mFactoryService->Release();
		delete this->mFactoryService;
		this->mFactoryService = NULL;
	}
}

/***********************************************************************************************/
void UPlanetaryMap::AllowRendering(bool pCanRender)
{
	this->mAllowRendering = pCanRender;
}

/***********************************************************************************************/
void UPlanetaryMap::BeginDestroy()
{
	this->Release();

	// Call ancestor method else crash in conditional method begin destroy
	Super::BeginDestroy();
}

/***********************************************************************************************/
FPrimitiveSceneProxy* UPlanetaryMap::CreateSceneProxy()
{
	FPrimitiveSceneProxy* lProxy = NULL;
	if 
		( this->HasAtmosphere() )
	{
		UE_LOG( LogClass, Log, TEXT( "UPlanetaryMap::CreateSceneProxy" ) );

		// The planetary map being in charge of rendering the atmosphere only
		// provide a proxy only if any atmosphere expected ;-).
		return new FPlanetaryMapSceneProxy( this );
	}

	return lProxy;
}

/***********************************************************************************************/
int32 UPlanetaryMap::GetNumMaterials() const
{
	return 1;
}

/***********************************************************************************************/
void UPlanetaryMap::OnRegister()
{
	Super::OnRegister();

	// Initialize the planetary map once the planet is registered into the scene
	// thus has a world reference
	this->Initialize();
}

/***********************************************************************************************/
void  UPlanetaryMap::PostEditChangeProperty(FPropertyChangedEvent& pEventArgs)
{
	FName lPropertyName = (pEventArgs.Property != NULL) ? pEventArgs.Property->GetFName() : NAME_None;
	if 
		( lPropertyName == GET_MEMBER_NAME_CHECKED( UPlanetaryMap, PlanetaryMode ) )
	{
		// If modifying the Mode, refresh the simple color factory service
		USimpleColorMapFactory* lColorFactory = Cast<USimpleColorMapFactory>( this->mFactoryService->GetActiveService( USimpleColorMapFactory::StaticClass() ) );
		if 
			( lColorFactory != NULL )
		{
			switch 
				( this->PlanetaryMode )
			{
			case PlanetaryStyle::eEarth:
				{
					lColorFactory->InitializeColorMap( ColorStyle::eEarthColor );
				}
				break;
			case PlanetaryStyle::eMoon:
				{
					lColorFactory->InitializeColorMap( ColorStyle::eLunarColor );
				}
				break;
			case PlanetaryStyle::eMars:
				{
					lColorFactory->InitializeColorMap( ColorStyle::eMarsColor );
				}
				break;
			}
		}
	}

	// Ancestor method
	Super::PostEditChangeProperty( pEventArgs );
}

/***********************************************************************************************/
float UPlanetaryMap::GetHeightAboveGround(const FVector& pPosition)
{
	FVector lPlanetPosition   = this->GetComponentLocation();
	FVector lToPosition       = pPosition - lPlanetPosition;
	FQuat lPlanetRotationQuat = this->GetComponentQuat();
	lPlanetRotationQuat.Normalize();

	FVector lRelativePosition = lPlanetRotationQuat * lToPosition;
	float lAltitude = lRelativePosition.Size();
	PlanetaryTreeCoordinates lCoordinates( lRelativePosition );
	for 
		( int8 lFace = 0; lFace < 6; lFace++ )
	{
		if 
			( this->mFaceTrees[ lFace ]->GetRoot()->HitTest( lCoordinates ) )
		{
			return lAltitude - this->mFaceTrees[ lFace ]->GetRoot()->GetHeightAtCoordinates( lCoordinates );
		}
	}

	return lAltitude - this->PlanetaryRadius;
}
